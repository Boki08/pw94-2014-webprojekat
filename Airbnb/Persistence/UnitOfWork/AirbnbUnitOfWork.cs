﻿using Airbnb.Persistence.Repository;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using Unity;

namespace Airbnb.Persistence.UnitOfWork
{
    public class AirbnbUnitOfWork : IUnitOfWork
    {
        private readonly DbContext _context;


        [Dependency]
        public IAppUserRepository AppUsers { get; set; }
        [Dependency]
        public IApartmentRepository Apartments { get; set; }
        [Dependency]
        public IAmenityRepository Amenities { get; set; }
        [Dependency]
        public IApartmentPictureRepository ApartmentPictures { get; set; }
        [Dependency]
        public ICommentRepository Comments { get; set; }
        [Dependency]
        public IApartmentTypeRepository ApartmentTypes { get; set; }
        [Dependency]
        public IReservationRepository Reservations { get; set; }
        [Dependency]
        public IStatusRepository Statuses { get; set; }
        [Dependency]
        public IFreeDateRepository FreeDates { get; set; }
        [Dependency]
        public IAddressRepository Addresses { get; set; }
        [Dependency]
        public ILocationRepository Locations { get; set; }
        [Dependency]
        public IRentingDateRepository RentingDates { get; set; }
        


    public AirbnbUnitOfWork(DbContext context)
        {
            _context = context;
        }

        public int Complete()
        {
            return _context.SaveChanges();
        }

        public void Dispose()
        {
            _context.Dispose();
        }
    }
}