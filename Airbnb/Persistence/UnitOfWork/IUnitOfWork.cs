﻿using Airbnb.Persistence.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Airbnb.Persistence.UnitOfWork
{
    public interface IUnitOfWork : IDisposable
    {
        IAppUserRepository AppUsers { get; set; }
        IApartmentRepository Apartments { get; set; }
        IAmenityRepository Amenities { get; set; }
        IApartmentPictureRepository ApartmentPictures { get; set; }
        ICommentRepository Comments { get; set; }
        IApartmentTypeRepository ApartmentTypes { get; set; }
        IReservationRepository Reservations { get; set; }
        IStatusRepository Statuses { get; set; }
        IFreeDateRepository FreeDates { get; set; }
        IAddressRepository Addresses { get; set; }
        ILocationRepository Locations { get; set; }
        IRentingDateRepository RentingDates { get; set; }
        int Complete();
    }
}