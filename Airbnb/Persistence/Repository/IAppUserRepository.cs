﻿using Airbnb.Models.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Airbnb.Persistence.Repository
{
    public interface IAppUserRepository : IRepository<AppUser, int>
    {
        IEnumerable<AppUser> SearchUsersGenderUsername(string gender,string username);
    }
}