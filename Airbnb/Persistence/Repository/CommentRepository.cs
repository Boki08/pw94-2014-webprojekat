﻿using Airbnb.Models.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace Airbnb.Persistence.Repository
{

    public class CommentRepository : Repository<Comment, int>, ICommentRepository
    {
        public CommentRepository(DbContext context) : base(context)
        {
        }
        protected AirbnbDBContext DBContext { get { return context as AirbnbDBContext; } }

        public IEnumerable<Comment> GetApartmenComments(int apartmentId)
        {
            return DBContext.Comments.Include(c=>c.AppUser).Where(r => r.ApartmentId == apartmentId && r.Approved==true);

        }
        public IEnumerable<Comment> GetAllApartmenComments(int apartmentId)
        {
            return DBContext.Comments.Include(c => c.AppUser).Where(r => r.ApartmentId == apartmentId);

        }
    }
}