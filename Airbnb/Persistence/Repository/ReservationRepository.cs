﻿using Airbnb.Models.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace Airbnb.Persistence.Repository
{

    public class ReservationRepository : Repository<Reservation, int>, IReservationRepository
    {
        public ReservationRepository(DbContext context) : base(context)
        {
        }
        protected AirbnbDBContext DBContext { get { return context as AirbnbDBContext; } }

        public IEnumerable<Reservation> GetAllUserReservationsSort(int userId,string sortType)
        {
            if (sortType == "Low")
            {
                return DBContext.Reservations.Include(x => x.Status).Where(s => s.AppUserId== userId).OrderBy(x => x.Price);
            }
            if (sortType == "High")
            {
                return DBContext.Reservations.Include(x => x.Status).Where(s => s.AppUserId == userId).OrderByDescending(x => x.Price);
            }
            else
            {
                return DBContext.Reservations.Include(x => x.Status).Where(s => s.AppUserId == userId);

            }
        }
        public Reservation GetUserReservation(int userId, int reservationId)
        {
            return DBContext.Reservations.Include(x => x.Status).Include(x=>x.Apartment.Location.Select(y => y.Address)).Where(x => x.Id == reservationId && x.AppUserId== userId).FirstOrDefault();
        }

        public IEnumerable<Reservation> GetAllHostReservationsSort(int userId, string sortType, string selectedUsername, string[] statusFilter)
        {
            IEnumerable<Reservation> reservations;
            if (selectedUsername== "notSet")
            {
                reservations = DBContext.Reservations.Include(x => x.Status).Include(x => x.Apartment).Where(s => s.Apartment.AppUserId == userId);
            }
            else
            {
                reservations = DBContext.Reservations.Include(x => x.Status).Include(x => x.Apartment).Where(s => s.Apartment.AppUserId == userId && s.AppUser.UserName.Contains(selectedUsername)); 
            }

            if (statusFilter.Length > 0)
            {
                 reservations = reservations.Where(x=> statusFilter.Contains( x.Status.Description));
                //var test = reservations.Where(x => statusFilter.Any(y => y == x.Status.Description));
            }

            if (sortType == "Low")
            {
                return reservations.OrderBy(x => x.Price);
            }
            if (sortType == "High")
            {
                return reservations.OrderByDescending(x => x.Price);
            }
            else
            {
                return reservations;

            }
        }
        public IEnumerable<Reservation> GetAllAdminReservationsSort(string sortType, string selectedUsername, string[] statusFilter)
        {
            IEnumerable<Reservation> reservations;
            if (selectedUsername == "notSet")
            {
                reservations = DBContext.Reservations.Include(x => x.Status).Include(x => x.Apartment);
            }
            else
            {
                reservations = DBContext.Reservations.Include(x => x.Status).Include(x => x.Apartment).Where(s=>s.AppUser.UserName.Contains(selectedUsername));
            }

            if (statusFilter.Length > 0)
            {
                reservations = reservations.Where(x => statusFilter.Contains(x.Status.Description));
                //var test = reservations.Where(x => statusFilter.Any(y => y == x.Status.Description));
            }

            if (sortType == "Low")
            {
                return reservations.OrderBy(x => x.Price);
            }
            if (sortType == "High")
            {
                return reservations.OrderByDescending(x => x.Price);
            }
            else
            {
                return reservations;

            }
        }
        public Reservation GetHostReservation(int userId, int reservationId)
        {
            return DBContext.Reservations.Include(x => x.Status).Include(x => x.AppUser).Include(x => x.Apartment).Include(x => x.Apartment.Location.Select(y => y.Address)).Where(x => x.Id == reservationId && x.Apartment.AppUserId == userId).FirstOrDefault();
        }
        public Reservation GetAdminReservation(int reservationId)
        {
            return DBContext.Reservations.Include(x => x.Status).Include(x => x.AppUser).Include(x => x.Apartment).Include(x => x.Apartment.Location.Select(y => y.Address)).Where(x => x.Id == reservationId).FirstOrDefault();
        }
        public IEnumerable<Reservation> ForApartmentWithStatus(int apartmentId)
        {
            return DBContext.Reservations.Include(x => x.Status).Include(x => x.AppUser).Include(x => x.Apartment).Where(x => x.ApartmentId == apartmentId && x.Status.Description== "Created" || x.Status.Description == "Accepted");
        }
    }
}