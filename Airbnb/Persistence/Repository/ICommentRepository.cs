﻿using Airbnb.Models.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Airbnb.Persistence.Repository
{
    public interface ICommentRepository : IRepository<Comment, int>
    {
        IEnumerable<Comment>  GetApartmenComments(int apartmentId);
        IEnumerable<Comment> GetAllApartmenComments(int apartmentId);
    }
}
