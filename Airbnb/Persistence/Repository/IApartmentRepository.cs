﻿using Airbnb.Models.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Airbnb.Persistence.Repository
{
    public interface IApartmentRepository : IRepository<Apartment, int>
    {
        Apartment GetApartmentWithHost(int apartmentId);
        Apartment GetEnabledApartment(int apartmentId);
        Apartment GetApartmentLocAmeHostPicType(int apartmentId);
        Apartment GetEnabledApartmentWithReservations(int apartmentId);
        IEnumerable<Apartment> GetAllApartments(string priceSort);

        IEnumerable<Apartment> GetAllApartmentsSearch(int[] amenityFilter, int[] typeFilter,bool searchChecked, string city, string priceSort, DateTime fromDate, DateTime toDate, int minPrice, int maxPrice, int numOfGuests, int numOfRooms);
        IEnumerable<Apartment> GetAllApartmentsHostSearch(int hostId, string statusFilter,int[]amenityFilter,int [] typeFilter, bool searchChecked, string city, string priceSort, DateTime fromDate, DateTime toDate, int minPrice, int maxPrice, int numOfGuests, int numOfRooms);
        
        IEnumerable<Apartment> GetAllApartmentsAdminSearch(string statusFilter,int[] amenityFilter, int[] typeFilter, bool searchChecked, string city, string priceSort, DateTime fromDate, DateTime toDate, int minPrice, int maxPrice, int numOfGuests, int numOfRooms);
        IEnumerable<Apartment> GetApartmentsHost(int hostId);
        //Apartment GetApartmentLocPicType(int apartmentId,int hostId);
        Apartment GetApartmentLocAmeHostPicTypeAdmin(int apartmentId);
        Apartment GetApartmentLocAmeHostPicTypeHost(int apartmentId, int hostId);
        Apartment GetApartmentForEdit(int apartmentId, int hostId);

        void DeleteApartment(int apartmentId);
    }
}