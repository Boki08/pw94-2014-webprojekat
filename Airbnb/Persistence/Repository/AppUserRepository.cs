﻿using Airbnb.Models.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace Airbnb.Persistence.Repository
{
    public class AppUserRepository : Repository<AppUser, int>, IAppUserRepository
    {
        public AppUserRepository(DbContext context) : base(context)
        {
        }
        protected AirbnbDBContext DBContext { get { return context as AirbnbDBContext; } }

        public IEnumerable<AppUser> SearchUsersGenderUsername(string gender, string username)
        {
            if (gender != "notSet" && username != "notSet")
            {
                return DBContext.AppUsers.Where(x => x.Gender == gender && x.UserName.Contains(username));
            }
            else if (gender != "notSet" && username == "notSet")
            {
                return DBContext.AppUsers.Where(x => x.Gender == gender).ToList();
            }
            else if (gender == "notSet" && username != "notSet")
            {
                return DBContext.AppUsers.Where(x => x.UserName.Contains(username));
            }
            else
            {
                return DBContext.AppUsers;
            }
        }
    }
}