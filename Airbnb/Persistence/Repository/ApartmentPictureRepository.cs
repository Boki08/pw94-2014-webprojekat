﻿using Airbnb.Models.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace Airbnb.Persistence.Repository
{
    public class ApartmentPictureRepository : Repository<ApartmentPicture, int>, IApartmentPictureRepository
    {
        public ApartmentPictureRepository(DbContext context) : base(context)
        {
        }
        protected AirbnbDBContext DBContext { get { return context as AirbnbDBContext; } }

        public ApartmentPicture GetApartmentPicture(int apartmentId)
        {
            return DBContext.ApartmentPictures.Where(r => r.Id == apartmentId).FirstOrDefault();

        }
    }
}