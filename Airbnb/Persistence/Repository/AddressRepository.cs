﻿using Airbnb.Models.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace Airbnb.Persistence.Repository
{

    public class AddressRepository : Repository<Address, int>, IAddressRepository
    {
        public AddressRepository(DbContext context) : base(context)
        {
        }
        protected AirbnbDBContext DBContext { get { return context as AirbnbDBContext; } }


    }
}