﻿using Airbnb.Models.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Airbnb.Persistence.Repository
{
    public interface IReservationRepository : IRepository<Reservation, int>
    {
         IEnumerable<Reservation> GetAllUserReservationsSort(int userId, string sortType);
        Reservation GetUserReservation(int userId, int reservationId);
        Reservation GetHostReservation(int userId, int reservationId);

        Reservation GetAdminReservation(int reservationId);


        IEnumerable<Reservation> GetAllHostReservationsSort(int userId, string sortType, string selectedUsername, string[] statusFilter);
        IEnumerable<Reservation>  GetAllAdminReservationsSort(string sortType,string selectedUsername, string[] statusFilter);

        IEnumerable<Reservation> ForApartmentWithStatus(int apartmentId);
    }
}
