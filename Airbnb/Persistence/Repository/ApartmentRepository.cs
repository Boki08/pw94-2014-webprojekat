﻿using Airbnb.Models.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace Airbnb.Persistence.Repository
{
    public class ApartmentRepository : Repository<Apartment, int>, IApartmentRepository
    {
        public ApartmentRepository(DbContext context) : base(context)
        {
        }
        protected AirbnbDBContext DBContext { get { return context as AirbnbDBContext; } }

        public Apartment GetApartmentWithHost(int apartmentId)
        {
            return DBContext.Apartments.Include(x => x.AppUser).Where(r => r.Id == apartmentId && r.Status == true).FirstOrDefault();

        }
        public Apartment GetEnabledApartment(int apartmentId)
        {
            return DBContext.Apartments.Where(r => r.Id == apartmentId && r.Status == true).FirstOrDefault();

        }
        public Apartment GetApartmentLocAmeHostPicType(int apartmentId)
        {

            return DBContext.Apartments.Include(x => x.Amenities).Include(x => x.RentingDates).Include(x => x.Location.Select(y => y.Address)).Include(x => x.ApartmentType).Include(x => x.Pictures).Include(x => x.AppUser).Where(s => s.Id == apartmentId && s.Status == true).FirstOrDefault();

        }
        public Apartment GetApartmentLocAmeHostPicTypeAdmin(int apartmentId)
        {

            return DBContext.Apartments.Include(x => x.Amenities).Include(x => x.RentingDates).Include(x => x.Location.Select(y => y.Address)).Include(x => x.ApartmentType).Include(x => x.Pictures).Include(x => x.AppUser).Where(s => s.Id == apartmentId).FirstOrDefault();

        }
        public Apartment GetApartmentLocAmeHostPicTypeHost(int apartmentId, int hostId)
        {

            return DBContext.Apartments.Include(x => x.Amenities).Include(x => x.RentingDates).Include(x => x.Location.Select(y => y.Address)).Include(x => x.ApartmentType).Include(x => x.Pictures).Include(x => x.AppUser).Where(s => s.Id == apartmentId && s.AppUserId == hostId).FirstOrDefault();

        }
        //public Apartment GetApartmentLocPicType(int apartmentId, int hostId)
        //{
        //    return DBContext.Apartments.Include(x => x.Location.Select(y => y.Address)).Include(x => x.ApartmentType).Include(x => x.Pictures).Where(s => s.Id == apartmentId && s.AppUserId == hostId).FirstOrDefault();
        //}
        public Apartment GetApartmentForEdit(int apartmentId, int hostId)
        {
            return DBContext.Apartments.Include(x => x.Location.Select(y => y.Address)).Include(x => x.ApartmentType).Include(x => x.Amenities).Include(x => x.FreeDates).Include(x => x.RentingDates).Include(x => x.Pictures).Where(s => s.Id == apartmentId && s.AppUserId == hostId).FirstOrDefault();
        }
        public Apartment GetEnabledApartmentWithReservations(int apartmentId)
        {
            return DBContext.Apartments.Include(x => x.Reservations.Select(y=>y.Status)).Where(s => s.Id == apartmentId && s.Status == true).FirstOrDefault();
        }

        public IEnumerable<Apartment> GetAllApartments(string priceSort)
        {
            //return DBContext.Apartments.Include(x => x.AppUser).Include(x => x.Pictures).Include(x => x.Location.Select(y=>y.Address)).Where(s => s.Status == true);
            if (priceSort == "Low")
            {
                return DBContext.Apartments.Include(x => x.AppUser).Include(x => x.Pictures).Include(x => x.Location.Select(y => y.Address)).Where(s => s.Status == true).OrderBy(x => x.Price);
            }
            if (priceSort == "High")
            {
                return DBContext.Apartments.Include(x => x.AppUser).Include(x => x.Pictures).Include(x => x.Location.Select(y => y.Address)).Where(s => s.Status == true).OrderByDescending(x => x.Price);
            }
            else
            {
                return DBContext.Apartments.Include(x => x.AppUser).Include(x => x.Pictures).Include(x => x.Location.Select(y => y.Address)).Where(s => s.Status == true);
            }
        }
        public IEnumerable<Apartment> GetAllApartmentsSearch(int[] amenityFilter, int[] typeFilter, bool searchChecked, string city, string priceSort, DateTime fromDate, DateTime toDate, int minPrice, int maxPrice, int numOfGuests, int numOfRooms)
        {


            IEnumerable<Apartment> apartments;
            if (searchChecked)
            {
                if (city == "noCity")
                {
                    apartments = DBContext.Apartments.Include(x => x.AppUser).Include(x => x.Pictures).Include(x => x.Amenities).Include(x => x.Location.Select(y => y.Address)).Include(x => x.FreeDates).Include(x => x.RentingDates).Where(y => y.FreeDates.Any(z => z.StartDate <= fromDate && z.EndDate >= toDate)).Where(p => p.Price >= minPrice && p.Price <= maxPrice).Where(g => g.NumOfGuests >= numOfGuests).Where(g => g.NumOfRooms >= numOfRooms).Where(s => s.Status == true);
                }
                else
                {
                    apartments = DBContext.Apartments.Include(x => x.AppUser).Include(x => x.Pictures).Include(x => x.Amenities).Include(x => x.Location.Select(y => y.Address)).Include(x => x.FreeDates).Include(x => x.RentingDates).Where(y => y.FreeDates.Any(z => z.StartDate <= fromDate && z.EndDate >= toDate)).Where(p => p.Price >= minPrice && p.Price <= maxPrice).Where(g => g.NumOfGuests >= numOfGuests).Where(g => g.NumOfRooms >= numOfRooms).Where(s => s.Status == true).Where(z => z.Location.Any(c => c.Address.Any(a => a.City == city)));
                }
            }
            else
            {
                apartments = DBContext.Apartments.Include(x => x.AppUser).Include(x => x.Pictures).Include(x => x.Amenities).Include(x => x.Location.Select(y => y.Address)).Include(x => x.FreeDates).Include(x => x.RentingDates).Where(s => s.Status == true);
            }
            if (amenityFilter != null && amenityFilter.Length > 0)
            {
                apartments = apartments.Where(x => amenityFilter.All(y => x.Amenities.Select(g => g.Id).Contains(y)));
            }
            if (typeFilter != null && typeFilter.Length > 0)
            {
                apartments = apartments.Where(x => typeFilter.Contains(x.ApartmentTypeId));
            }

            if (priceSort == "Low")
            {
                apartments = apartments.OrderBy(x => x.Price);
            }
            else if (priceSort == "High")
            {
                apartments = apartments.OrderByDescending(x => x.Price);
            }

            return apartments;


            //if (city == "noCity")
            //{
            //    if (priceSort == "Low")
            //    {
            //        return DBContext.Apartments.Include(x => x.AppUser).Include(x => x.Pictures).Include(x => x.Amenities).Include(x => x.Location.Select(y => y.Address)).Include(x => x.FreeDates).Include(x => x.RentingDates).Where(y => y.FreeDates.Any(z => z.StartDate <= fromDate && z.EndDate >= toDate)).Where(p => p.Price >= minPrice && p.Price <= maxPrice).Where(g => g.NumOfGuests >= numOfGuests).Where(g => g.NumOfRooms >= numOfRooms).Where(s => s.Status == true).OrderBy(x => x.Price);
            //    }
            //    if (priceSort == "High")
            //    {
            //        return DBContext.Apartments.Include(x => x.AppUser).Include(x => x.Pictures).Include(x => x.Amenities).Include(x => x.Location.Select(y => y.Address)).Include(x => x.FreeDates).Include(x => x.RentingDates).Where(y => y.FreeDates.Any(z => z.StartDate <= fromDate && z.EndDate >= toDate)).Where(p => p.Price >= minPrice && p.Price <= maxPrice).Where(g => g.NumOfGuests >= numOfGuests).Where(g => g.NumOfRooms >= numOfRooms).Where(s => s.Status == true).OrderByDescending(x => x.Price);
            //    }
            //    else
            //    {
            //        return DBContext.Apartments.Include(x => x.AppUser).Include(x => x.Pictures).Include(x => x.Amenities).Include(x => x.Location.Select(y => y.Address)).Include(x => x.FreeDates).Include(x => x.RentingDates).Where(y => y.FreeDates.Any(z => z.StartDate <= fromDate && z.EndDate >= toDate)).Where(p => p.Price >= minPrice && p.Price <= maxPrice).Where(g => g.NumOfGuests >= numOfGuests).Where(g => g.NumOfRooms >= numOfRooms).Where(s => s.Status == true);
            //    }
            //}
            //else
            //{
            //    if (priceSort == "Low")
            //    {
            //        return DBContext.Apartments.Include(x => x.AppUser).Include(x => x.Pictures).Include(x => x.Amenities).Include(x => x.Location.Select(y => y.Address)).Include(x => x.FreeDates).Include(x => x.RentingDates).Where(y => y.FreeDates.Any(z => z.StartDate <= fromDate && z.EndDate >= toDate)).Where(p => p.Price >= minPrice && p.Price <= maxPrice).Where(g => g.NumOfGuests >= numOfGuests).Where(g => g.NumOfRooms >= numOfRooms).Where(s => s.Status == true).Where(z => z.Location.Any(c => c.Address.Any(a => a.City == city))).OrderBy(x => x.Price);
            //    }
            //    if (priceSort == "High")
            //    {
            //        return DBContext.Apartments.Include(x => x.AppUser).Include(x => x.Pictures).Include(x => x.Amenities).Include(x => x.Location.Select(y => y.Address)).Include(x => x.FreeDates).Include(x => x.RentingDates).Where(y => y.FreeDates.Any(z => z.StartDate <= fromDate && z.EndDate >= toDate)).Where(p => p.Price >= minPrice && p.Price <= maxPrice).Where(g => g.NumOfGuests >= numOfGuests).Where(g => g.NumOfRooms >= numOfRooms).Where(s => s.Status == true).Where(z => z.Location.Any(c => c.Address.Any(a => a.City == city))).OrderByDescending(x => x.Price);
            //    }
            //    else
            //    {
            //        return DBContext.Apartments.Include(x => x.AppUser).Include(x => x.Pictures).Include(x => x.Amenities).Include(x => x.Location.Select(y => y.Address)).Include(x => x.FreeDates).Include(x => x.RentingDates).Where(y => y.FreeDates.Any(z => z.StartDate <= fromDate && z.EndDate >= toDate)).Where(p => p.Price >= minPrice && p.Price <= maxPrice).Where(g => g.NumOfGuests >= numOfGuests).Where(g => g.NumOfRooms >= numOfRooms).Where(s => s.Status == true).Where(z => z.Location.Any(c => c.Address.Any(a => a.City == city)));
            //    }
            //}



        }

        public IEnumerable<Apartment> GetAllApartmentsHostSearch(int hostId, string statusFilter, int[] amenityFilter, int[] typeFilter, bool searchChecked, string city, string priceSort, DateTime fromDate, DateTime toDate, int minPrice, int maxPrice, int numOfGuests, int numOfRooms)
        {

            IEnumerable<Apartment> apartments;
            if (searchChecked)
            {
                if (city == "noCity")
                {
                    apartments = DBContext.Apartments.Include(x => x.AppUser).Include(x => x.Pictures).Include(x => x.Amenities).Include(x => x.Location.Select(y => y.Address)).Include(x => x.FreeDates).Include(x => x.RentingDates).Where(y => y.FreeDates.Any(z => z.StartDate <= fromDate && z.EndDate >= toDate)).Where(p => p.Price >= minPrice && p.Price <= maxPrice).Where(g => g.NumOfGuests >= numOfGuests).Where(g => g.NumOfRooms >= numOfRooms).Where(u => u.AppUserId == hostId);
                }
                else
                {
                    apartments = DBContext.Apartments.Include(x => x.AppUser).Include(x => x.Pictures).Include(x => x.Amenities).Include(x => x.Location.Select(y => y.Address)).Include(x => x.FreeDates).Include(x => x.RentingDates).Where(y => y.FreeDates.Any(z => z.StartDate <= fromDate && z.EndDate >= toDate)).Where(p => p.Price >= minPrice && p.Price <= maxPrice).Where(g => g.NumOfGuests >= numOfGuests).Where(g => g.NumOfRooms >= numOfRooms).Where(z => z.Location.Any(c => c.Address.Any(a => a.City == city))).Where(u => u.AppUserId == hostId);
                }
            }
            else
            {
                apartments = DBContext.Apartments.Include(x => x.AppUser).Include(x => x.Pictures).Include(x => x.Amenities).Include(x => x.Location.Select(y => y.Address)).Include(x => x.FreeDates).Include(x => x.RentingDates).Where(u => u.AppUserId == hostId);
            }
            if (amenityFilter != null && amenityFilter.Length > 0)
            {
                //var b = apartments.Where(x => x.Amenities.Any(z => amenityFilter.All(y => y.Equals(z.Id))));
                //var c = apartments.Where(x => x.Amenities.Any(z => amenityFilter.Any(y => y.Equals(z.Id))));
                //var d = apartments.Where(x => x.Amenities.All(z => amenityFilter.Any(y => y.Equals(z.Id))));
                //var e = apartments.Where(x => x.Amenities.All(z => amenityFilter.All(y => y.Equals(z.Id))));
                //var g = apartments.Where(x => x.Amenities.Select(y => y.Id).Contains( amenityFilter.All(z => z.Equals(x.Amenities.Any(c=>c.Id==))));
                //var f = apartments.Where(x => x.Amenities.Where(y=> y.Id.amenityFilter.All(aa => aa.Equals(y.Id))));
                apartments = apartments.Where(x => amenityFilter.All(y => x.Amenities.Select(g => g.Id).Contains(y)));
                //apartments = apartments.Where(x => x.Amenities.Any(a=> amenityFilter.All(y => y.Equals(a.Id))));

                //var test = reservations.Where(x => statusFilter.Any(y => y == x.Status.Description));
            }
            if (typeFilter != null && typeFilter.Length > 0)
            {
                apartments = apartments.Where(x => typeFilter.Contains(x.ApartmentTypeId));

            }


            if (statusFilter == "Enabled")
            {
                apartments = apartments.Where(x => x.Status == true);
            }
            else if (statusFilter == "Disabled")
            {
                apartments = apartments.Where(x => x.Status == false);
            }


            if (priceSort == "Low")
            {
                apartments = apartments.OrderBy(x => x.Price);
            }
            else if (priceSort == "High")
            {
                apartments = apartments.OrderByDescending(x => x.Price);
            }
            //else
            //{
            //    return DBContext.Apartments.Include(x => x.AppUser).Include(x => x.Pictures).Include(x => x.Amenities).Include(x => x.Location.Select(y => y.Address)).Include(x => x.FreeDates).Include(x => x.RentingDates).Where(y => y.FreeDates.Any(z => z.StartDate <= fromDate && z.EndDate >= toDate)).Where(p => p.Price >= minPrice && p.Price <= maxPrice).Where(g => g.NumOfGuests >= numOfGuests).Where(g => g.NumOfRooms >= numOfRooms).Where(s => s.Status == true).Where(u => u.AppUserId == hostId);
            //}

            return apartments;
            //if (city == "noCity")
            //{
            //    if (priceSort == "Low")
            //    {
            //        return DBContext.Apartments.Include(x => x.AppUser).Include(x => x.Pictures).Include(x => x.Amenities).Include(x => x.Location.Select(y => y.Address)).Include(x => x.FreeDates).Include(x => x.RentingDates).Where(y => y.FreeDates.Any(z => z.StartDate <= fromDate && z.EndDate >= toDate)).Where(p => p.Price >= minPrice && p.Price <= maxPrice).Where(g => g.NumOfGuests >= numOfGuests).Where(g => g.NumOfRooms >= numOfRooms).Where(s => s.Status == true).Where(u => u.AppUserId == hostId).OrderBy(x => x.Price);
            //    }
            //    if (priceSort == "High")
            //    {
            //        return DBContext.Apartments.Include(x => x.AppUser).Include(x => x.Pictures).Include(x => x.Amenities).Include(x => x.Location.Select(y => y.Address)).Include(x => x.FreeDates).Include(x => x.RentingDates).Where(y => y.FreeDates.Any(z => z.StartDate <= fromDate && z.EndDate >= toDate)).Where(p => p.Price >= minPrice && p.Price <= maxPrice).Where(g => g.NumOfGuests >= numOfGuests).Where(g => g.NumOfRooms >= numOfRooms).Where(s => s.Status == true).Where(u => u.AppUserId == hostId).OrderByDescending(x => x.Price);
            //    }
            //    else
            //    {
            //        return DBContext.Apartments.Include(x => x.AppUser).Include(x => x.Pictures).Include(x => x.Amenities).Include(x => x.Location.Select(y => y.Address)).Include(x => x.FreeDates).Include(x => x.RentingDates).Where(y => y.FreeDates.Any(z => z.StartDate <= fromDate && z.EndDate >= toDate)).Where(p => p.Price >= minPrice && p.Price <= maxPrice).Where(g => g.NumOfGuests >= numOfGuests).Where(g => g.NumOfRooms >= numOfRooms).Where(s => s.Status == true).Where(u => u.AppUserId == hostId);
            //    }
            //}
            //else
            //{
            //    if (priceSort == "Low")
            //    {
            //        return DBContext.Apartments.Include(x => x.AppUser).Include(x => x.Pictures).Include(x => x.Amenities).Include(x => x.Location.Select(y => y.Address)).Include(x => x.FreeDates).Include(x => x.RentingDates).Where(y => y.FreeDates.Any(z => z.StartDate <= fromDate && z.EndDate >= toDate)).Where(p => p.Price >= minPrice && p.Price <= maxPrice).Where(g => g.NumOfGuests >= numOfGuests).Where(g => g.NumOfRooms >= numOfRooms).Where(s => s.Status == true).Where(z => z.Location.Any(c => c.Address.Any(a => a.City == city))).Where(u => u.AppUserId == hostId).OrderBy(x => x.Price);
            //    }
            //    if (priceSort == "High")
            //    {
            //        return DBContext.Apartments.Include(x => x.AppUser).Include(x => x.Pictures).Include(x => x.Amenities).Include(x => x.Location.Select(y => y.Address)).Include(x => x.FreeDates).Include(x => x.RentingDates).Where(y => y.FreeDates.Any(z => z.StartDate <= fromDate && z.EndDate >= toDate)).Where(p => p.Price >= minPrice && p.Price <= maxPrice).Where(g => g.NumOfGuests >= numOfGuests).Where(g => g.NumOfRooms >= numOfRooms).Where(s => s.Status == true).Where(z => z.Location.Any(c => c.Address.Any(a => a.City == city))).Where(u => u.AppUserId == hostId).OrderByDescending(x => x.Price);
            //    }
            //    else
            //    {
            //        return DBContext.Apartments.Include(x => x.AppUser).Include(x => x.Pictures).Include(x => x.Amenities).Include(x => x.Location.Select(y => y.Address)).Include(x => x.FreeDates).Include(x => x.RentingDates).Where(y => y.FreeDates.Any(z => z.StartDate <= fromDate && z.EndDate >= toDate)).Where(p => p.Price >= minPrice && p.Price <= maxPrice).Where(g => g.NumOfGuests >= numOfGuests).Where(g => g.NumOfRooms >= numOfRooms).Where(s => s.Status == true).Where(z => z.Location.Any(c => c.Address.Any(a => a.City == city))).Where(u => u.AppUserId == hostId);
            //    }
            //}
        }

        public IEnumerable<Apartment> GetAllApartmentsAdminSearch(string statusFilter, int[] amenityFilter, int[] typeFilter, bool searchChecked, string city, string priceSort, DateTime fromDate, DateTime toDate, int minPrice, int maxPrice, int numOfGuests, int numOfRooms)
        {

            IEnumerable<Apartment> apartments;
            if (searchChecked)
            {
                if (city == "noCity")
                {
                    apartments = DBContext.Apartments.Include(x => x.AppUser).Include(x => x.Pictures).Include(x => x.Amenities).Include(x => x.Location.Select(y => y.Address)).Include(x => x.FreeDates).Include(x => x.RentingDates).Where(y => y.FreeDates.Any(z => z.StartDate <= fromDate && z.EndDate >= toDate)).Where(p => p.Price >= minPrice && p.Price <= maxPrice).Where(g => g.NumOfGuests >= numOfGuests).Where(g => g.NumOfRooms >= numOfRooms);
                }
                else
                {
                    apartments = DBContext.Apartments.Include(x => x.AppUser).Include(x => x.Pictures).Include(x => x.Amenities).Include(x => x.Location.Select(y => y.Address)).Include(x => x.FreeDates).Include(x => x.RentingDates).Where(y => y.FreeDates.Any(z => z.StartDate <= fromDate && z.EndDate >= toDate)).Where(p => p.Price >= minPrice && p.Price <= maxPrice).Where(g => g.NumOfGuests >= numOfGuests).Where(g => g.NumOfRooms >= numOfRooms).Where(z => z.Location.Any(c => c.Address.Any(a => a.City == city)));
                }
            }
            else
            {
                apartments = DBContext.Apartments.Include(x => x.AppUser).Include(x => x.Pictures).Include(x => x.Amenities).Include(x => x.Location.Select(y => y.Address)).Include(x => x.FreeDates).Include(x => x.RentingDates);
            }
            if (amenityFilter != null && amenityFilter.Length > 0)
            {
                apartments = apartments.Where(x => amenityFilter.All(y => x.Amenities.Select(g => g.Id).Contains(y)));
            }
            if (typeFilter != null && typeFilter.Length > 0)
            {
                apartments = apartments.Where(x => typeFilter.Contains(x.ApartmentTypeId));
            }

            if (statusFilter == "Enabled")
            {
                apartments = apartments.Where(x => x.Status == true);
            }
            else if (statusFilter == "Disabled")
            {
                apartments = apartments.Where(x => x.Status == false);
            }


            if (priceSort == "Low")
            {
                apartments = apartments.OrderBy(x => x.Price);
            }
            else if (priceSort == "High")
            {
                apartments = apartments.OrderByDescending(x => x.Price);
            }

            return apartments;
        }
        public IEnumerable<Apartment> GetApartmentsHost(int hostId)
        {
            return DBContext.Apartments.Where(x => x.AppUserId == hostId);
        }
       public void DeleteApartment(int apartmentId)
        {
           Apartment apartment =DBContext.Apartments.Where(x => x.Id == apartmentId).Include(x=>x.ApartmentType).Include(x => x.Comments).Include(x => x.FreeDates).Include(x => x.Location.Select(y=>y.Address)).Include(x => x.Pictures).Include(x => x.RentingDates).Include(x => x.Reservations).FirstOrDefault();
        }

    }
}