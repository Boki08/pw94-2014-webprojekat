﻿using Airbnb.Models.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace Airbnb.Persistence.Repository
{

    public class FreeDateRepository : Repository<FreeDate, int>, IFreeDateRepository
    {
        public FreeDateRepository(DbContext context) : base(context)
        {
        }
        protected AirbnbDBContext DBContext { get { return context as AirbnbDBContext; } }


    }
}