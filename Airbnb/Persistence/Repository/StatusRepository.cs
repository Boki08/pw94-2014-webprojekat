﻿using Airbnb.Models.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace Airbnb.Persistence.Repository
{
    public class StatusRepository : Repository<Status, int>, IStatusRepository
    {
        public StatusRepository(DbContext context) : base(context)
        {
        }
        protected AirbnbDBContext DBContext { get { return context as AirbnbDBContext; } }

        public Status GetStatusByDescription(string Description)
        {
            return DBContext.Statuses.Where(x => x.Description == Description).FirstOrDefault();

        }
    }
}