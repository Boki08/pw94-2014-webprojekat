﻿using Airbnb.Models;
using Airbnb.Models.Entities;
using EntityFramework.DynamicFilters;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Web;

namespace Airbnb.Persistence
{
    public class AirbnbDBContext : IdentityDbContext<AirbnbIdentityUser>
    {
        public virtual DbSet<AppUser> AppUsers { get; set; }
        public virtual DbSet<Address> Addresses { get; set; }
        public virtual DbSet<Amenity> Amenities { get; set; }
        public virtual DbSet<ApartmentType> ApartmentTypes { get; set; }
        public virtual DbSet<Apartment> Apartments { get; set; }
        public virtual DbSet<ApartmentPicture> ApartmentPictures { get; set; }
        public virtual DbSet<Comment> Comments { get; set; }
        public virtual DbSet<FreeDate> FreeDates { get; set; }
        //public virtual DbSet<Date> Dates { get; set; }
        public virtual DbSet<RentingDate> RentingDates { get; set; }
        public virtual DbSet<Location> Locations { get; set; }
        public virtual DbSet<Reservation> Reservations { get; set; }
        public virtual DbSet<Status> Statuses { get; set; }

        public AirbnbDBContext() : base("name=AirbnbDB")
        {
        }

        public static AirbnbDBContext Create()
        {
            return new AirbnbDBContext();
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            //modelBuilder.Entity<AppUser>()
            //           .HasOptional(e => e.Comments)
            //           .WithMany()
            //           .WillCascadeOnDelete(false);
            //modelBuilder.Entity<Apartment>()
            //           .HasOptional(e => e.Comments)
            //           .WithMany()
            //           .WillCascadeOnDelete(false);

            modelBuilder.Entity<Apartment>().HasMany(t => t.Amenities).WithMany(t => t.Apartments).Map(m =>
            {
                m.ToTable("ApartmentAmenities");
                m.MapLeftKey("ApartmentId");
                m.MapRightKey("AmenityId");
            });
            //modelBuilder.Entity<AppUser>().Map(m => m.Requires("IsDeleted").HasValue(false)).Ignore(m => m.IsDeleted);

            modelBuilder.Filter("IsDeleted", (ILogicalDelete d) => d.IsDeleted, false);

           


            //The code above works like a filter between the database and our application.
            //You don’t need to filter your entities by checking IsDeleted property every time. 
            //Filtering will work inside of Entity Framework.

            //modelBuilder.Entity<AppUser>().HasRequired(f => f.Reservations).WithRequiredDependent().WillCascadeOnDelete(false);


        }

        public override int SaveChanges()
        {
            UpdateSoftDeleteStatuses();
            return base.SaveChanges();
        }

        public override Task<int> SaveChangesAsync(CancellationToken cancellationToken = default(CancellationToken))
        {
            UpdateSoftDeleteStatuses();
            return base.SaveChangesAsync( cancellationToken);
        }

        private void UpdateSoftDeleteStatuses()
        {

            //foreach (var entry in ChangeTracker.Entries().Where(p => p.Entity is ILogicalDelete))
            //{
            //    switch (entry.State)
            //    {
            //        case EntityState.Added:
            //            (entry.Entity as ILogicalDelete).IsDeleted = false;
            //            //entry.CurrentValues["IsDeleted"] = false;
            //            break;
            //        case EntityState.Deleted:
            //            entry.State = EntityState.Modified;
            //            entry.CurrentValues["IsDeleted"] = true;
            //            break;
            //    }
            //}
            foreach (var entry in ChangeTracker.Entries().Where(p => p.Entity is ILogicalDelete))
            {
                //if (entry.Entity is ILogicalDelete)
                //{
                //    //if (System.Diagnostics.Debugger.IsAttached == false)
                //    //{
                //    //    System.Diagnostics.Debugger.Launch();
                //    //}
                //    switch (entry.State)
                //    {
                //        case EntityState.Added:
                //            entry.CurrentValues[nameof(ILogicalDelete.IsDeleted)] = false;
                //            break;
                //        case EntityState.Deleted:
                //            entry.State = EntityState.Modified;
                //            entry.CurrentValues[nameof(ILogicalDelete.IsDeleted)] = false;
                //            break;
                //    }
                //}
                switch (entry.State)
                {
                   
                    case EntityState.Added:
                        entry.CurrentValues[nameof(ILogicalDelete.IsDeleted)] = false;
                        break;
                    case EntityState.Deleted:
                        entry.State = EntityState.Modified;
                        entry.CurrentValues[nameof(ILogicalDelete.IsDeleted)] = true;
                        break;
                }
            }
        }
        //The above code intercepts any delete queries and converts them to update queries that set the isDeleted column to true. 
        //It also makes sure that any queries that insert any new data set the isDeleted column to false.
        //With the above changes, you can pretend that the data is actually being deleted when you write your EF Core code.
        //Queries that read data will ignore deleted data because the QueryFilter only retains results for entities with isDeleted == false.
    }
}