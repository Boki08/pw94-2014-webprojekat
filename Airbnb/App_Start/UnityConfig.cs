using Airbnb.Models.Entities;
using Airbnb.Persistence;
using Airbnb.Persistence.Repository;
using Airbnb.Persistence.UnitOfWork;
using Airbnb.Providers;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.Owin.Security;
using System;
using System.Data.Entity;
using Unity;
using Unity.AspNet.Mvc;
using Unity.Injection;

namespace Airbnb
{
    /// <summary>
    /// Specifies the Unity configuration for the main container.
    /// </summary>
    public static class UnityConfig
    {
        #region Unity Container
        private static Lazy<IUnityContainer> container =
          new Lazy<IUnityContainer>(() =>
          {
              var container = new UnityContainer();
              RegisterTypes(container);
              return container;
          });

        /// <summary>
        /// Configured Unity Container.
        /// </summary>
        public static IUnityContainer Container => container.Value;
        #endregion

        /// <summary>
        /// Registers the type mappings with the Unity container.
        /// </summary>
        /// <param name="container">The unity container to configure.</param>
        /// <remarks>
        /// There is no need to register concrete types such as controllers or
        /// API controllers (unless you want to change the defaults), as Unity
        /// allows resolving a concrete type even if it was not previously
        /// registered.
        /// </remarks>
        public static void RegisterTypes(IUnityContainer container)
        {
            // NOTE: To load from web.config uncomment the line below.
            // Make sure to add a Unity.Configuration to the using statements.
            // container.LoadConfiguration();

            // TODO: Register your type's mappings here.
            // container.RegisterType<IProductRepository, ProductRepository>();

            container.RegisterType<DbContext, AirbnbDBContext>(new PerRequestLifetimeManager());
            container.RegisterType<ApplicationUserManager>();

            container.RegisterType<IUnitOfWork, AirbnbUnitOfWork>();

            container.RegisterType<IAppUserRepository, AppUserRepository>();
            container.RegisterType<IApartmentRepository, ApartmentRepository>();
            container.RegisterType<IApartmentPictureRepository, ApartmentPictureRepository>();
            container.RegisterType<IApartmentTypeRepository, ApartmentTypeRepository>();
            container.RegisterType<IAmenityRepository, AmenityRepository>();
            container.RegisterType<ICommentRepository, CommentRepository>();
            container.RegisterType<IReservationRepository, ReservationRepository>();
            container.RegisterType<IStatusRepository, StatusRepository>();
            container.RegisterType<IFreeDateRepository, FreeDateRepository>();
            container.RegisterType<IAddressRepository, AddressRepository>();
            container.RegisterType<ILocationRepository, LocationRepository>();
            container.RegisterType<IRentingDateRepository, RentingDateRepository>();
            









            container.RegisterType<ISecureDataFormat<AuthenticationTicket>, CustomJwtFormat>(new InjectionConstructor("http://localhost:61619"));
            container.RegisterType<IUserStore<AirbnbIdentityUser>, UserStore<AirbnbIdentityUser>>(new InjectionConstructor(typeof(DbContext)));
        }
    }
}