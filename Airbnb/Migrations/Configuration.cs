﻿namespace Airbnb.Migrations
{
    using Microsoft.AspNet.Identity;
    using Microsoft.AspNet.Identity.EntityFramework;
    using Airbnb.Models.Entities;
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;
    using System.Data.Entity.Validation;

    internal sealed class Configuration : DbMigrationsConfiguration<Airbnb.Persistence.AirbnbDBContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(Airbnb.Persistence.AirbnbDBContext context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data. E.g.
            //
            //    context.People.AddOrUpdate(
            //      p => p.FullName,
            //      new Person { FullName = "Andrew Peters" },
            //      new Person { FullName = "Brice Lambson" },
            //      new Person { FullName = "Rowan Miller" }
            //    );

            //debug
            //if (System.Diagnostics.Debugger.IsAttached == false)
            //{
            //    System.Diagnostics.Debugger.Launch();
            //}


            if (!context.Roles.Any(r => r.Name == "Admin"))
            {
                var store = new RoleStore<IdentityRole>(context);
                var manager = new RoleManager<IdentityRole>(store);
                var role = new IdentityRole { Name = "Admin" };

                manager.Create(role);
            }

            if (!context.Roles.Any(r => r.Name == "Host"))
            {
                var store = new RoleStore<IdentityRole>(context);
                var manager = new RoleManager<IdentityRole>(store);
                var role = new IdentityRole { Name = "Host" };

                manager.Create(role);
            }

            if (!context.Roles.Any(r => r.Name == "Guest"))
            {
                var store = new RoleStore<IdentityRole>(context);
                var manager = new RoleManager<IdentityRole>(store);
                var role = new IdentityRole { Name = "Guest" };

                manager.Create(role);
            }

            context.AppUsers.AddOrUpdate(

                  u => u.Name,

                  new AppUser() {UserName="Admin", Name = "Admin", Surname = "Adminovic", Gender = "male", Blocked = false }

            );

            context.AppUsers.AddOrUpdate(

                p => p.Name,

                new AppUser() {UserName = "Guest", Name = "Guest", Surname = "Guestovic", Gender = "male",Blocked=false }

            );
            context.AppUsers.AddOrUpdate(

               p => p.Name,

               new AppUser() {UserName = "Host", Name = "Host", Surname = "Hostovic", Gender = "female", Blocked = false }

           );
            try
            {
                // Your code...
                // Could also be before try if you know the exception occurs in SaveChanges

                context.SaveChanges();
            }
            catch (DbEntityValidationException e)
            {
                foreach (var eve in e.EntityValidationErrors)
                {
                    Console.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                        eve.Entry.Entity.GetType().Name, eve.Entry.State);
                    foreach (var ve in eve.ValidationErrors)
                    {
                        Console.WriteLine("- Property: \"{0}\", Error: \"{1}\"",
                            ve.PropertyName, ve.ErrorMessage);
                    }
                }
                throw;
            }
            context.Statuses.AddOrUpdate(s => s.Description, new Status() { Description = "Created" });
            context.Statuses.AddOrUpdate(s => s.Description, new Status() { Description = "Rejected" });
            context.Statuses.AddOrUpdate(s => s.Description, new Status() { Description = "Canceled" });
            context.Statuses.AddOrUpdate(s => s.Description, new Status() { Description = "Accepted" });
            context.Statuses.AddOrUpdate(s => s.Description, new Status() { Description = "Completed" });

            context.ApartmentTypes.AddOrUpdate(s => s.Type, new Airbnb.Models.Entities.ApartmentType() { Type = "Apartment" });
            context.ApartmentTypes.AddOrUpdate(s => s.Type, new Airbnb.Models.Entities.ApartmentType() { Type = "Room" });


            try
            {
                // Your code...
                // Could also be before try if you know the exception occurs in SaveChanges

                context.SaveChanges();
            }
            catch (DbEntityValidationException e)
            {
                foreach (var eve in e.EntityValidationErrors)
                {
                    Console.WriteLine("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                        eve.Entry.Entity.GetType().Name, eve.Entry.State);
                    foreach (var ve in eve.ValidationErrors)
                    {
                        Console.WriteLine("- Property: \"{0}\", Error: \"{1}\"",
                            ve.PropertyName, ve.ErrorMessage);
                    }
                }
                throw;
            }

            var userStore = new UserStore<AirbnbIdentityUser>(context);
            var userManager = new UserManager<AirbnbIdentityUser>(userStore);

            if (!context.Users.Any(u => u.UserName == "Admin"))
            {
                var _appUser = context.AppUsers.FirstOrDefault(a => a.UserName == "Admin");
                var user = new AirbnbIdentityUser() { Id = "Admin", UserName = "Admin", PasswordHash = AirbnbIdentityUser.HashPassword("Admin"), AppUserId = _appUser.Id };
                userManager.Create(user);
                userManager.AddToRole(user.Id, "Admin");
            }
            if (!context.Users.Any(u => u.UserName == "Host"))

            {

                var _appUser = context.AppUsers.FirstOrDefault(a => a.UserName == "Host");
                var user = new AirbnbIdentityUser() { Id = "Host", UserName = "Host", PasswordHash = AirbnbIdentityUser.HashPassword("Host"), AppUserId = _appUser.Id };
                userManager.Create(user);
                userManager.AddToRole(user.Id, "Host");

            }
            if (!context.Users.Any(u => u.UserName == "Guest"))

            {

                var _appUser = context.AppUsers.FirstOrDefault(a => a.UserName == "Guest");
                var user = new AirbnbIdentityUser() { Id = "Guest", UserName = "Guest", PasswordHash = AirbnbIdentityUser.HashPassword("Guest"), AppUserId = _appUser.Id };
                userManager.Create(user);
                userManager.AddToRole(user.Id, "Guest");

            }
        }
    }
}
