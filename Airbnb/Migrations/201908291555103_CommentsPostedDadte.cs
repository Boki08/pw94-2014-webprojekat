namespace Airbnb.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class CommentsPostedDadte : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Comments", "PostedDate", c => c.DateTime(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Comments", "PostedDate");
        }
    }
}
