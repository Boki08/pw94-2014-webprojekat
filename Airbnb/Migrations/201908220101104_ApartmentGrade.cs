namespace Airbnb.Migrations
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity.Infrastructure.Annotations;
    using System.Data.Entity.Migrations;
    
    public partial class ApartmentGrade : DbMigration
    {
        public override void Up()
        {
            RenameTable(name: "dbo.ApartmanTypes", newName: "ApartmentTypes");
            RenameColumn(table: "dbo.Apartments", name: "ApartmanTypeId", newName: "ApartmentTypeId");
            RenameIndex(table: "dbo.Apartments", name: "IX_ApartmanTypeId", newName: "IX_ApartmentTypeId");
            AlterTableAnnotations(
                "dbo.ApartmentTypes",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Type = c.String(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                    },
                annotations: new Dictionary<string, AnnotationValues>
                {
                    { 
                        "DynamicFilter_ApartmanType_IsDeleted",
                        new AnnotationValues(oldValue: "EntityFramework.DynamicFilters.DynamicFilterDefinition", newValue: null)
                    },
                    { 
                        "DynamicFilter_ApartmentType_IsDeleted",
                        new AnnotationValues(oldValue: null, newValue: "EntityFramework.DynamicFilters.DynamicFilterDefinition")
                    },
                });
            
            AddColumn("dbo.Apartments", "Grade", c => c.Double(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Apartments", "Grade");
            AlterTableAnnotations(
                "dbo.ApartmentTypes",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Type = c.String(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                    },
                annotations: new Dictionary<string, AnnotationValues>
                {
                    { 
                        "DynamicFilter_ApartmanType_IsDeleted",
                        new AnnotationValues(oldValue: null, newValue: "EntityFramework.DynamicFilters.DynamicFilterDefinition")
                    },
                    { 
                        "DynamicFilter_ApartmentType_IsDeleted",
                        new AnnotationValues(oldValue: "EntityFramework.DynamicFilters.DynamicFilterDefinition", newValue: null)
                    },
                });
            
            RenameIndex(table: "dbo.Apartments", name: "IX_ApartmentTypeId", newName: "IX_ApartmanTypeId");
            RenameColumn(table: "dbo.Apartments", name: "ApartmentTypeId", newName: "ApartmanTypeId");
            RenameTable(name: "dbo.ApartmentTypes", newName: "ApartmanTypes");
        }
    }
}
