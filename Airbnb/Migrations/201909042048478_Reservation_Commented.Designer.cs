// <auto-generated />
namespace Airbnb.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.1.1-30610")]
    public sealed partial class Reservation_Commented : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(Reservation_Commented));
        
        string IMigrationMetadata.Id
        {
            get { return "201909042048478_Reservation_Commented"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
