namespace Airbnb.Migrations
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity.Infrastructure.Annotations;
    using System.Data.Entity.Migrations;
    
    public partial class ApartmentType : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.ApartmanTypes",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Type = c.String(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_ApartmanType_IsDeleted", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id);
            
            AddColumn("dbo.Apartments", "ApartmanTypeId", c => c.Int(nullable: false));
            CreateIndex("dbo.Apartments", "ApartmanTypeId");
            AddForeignKey("dbo.Apartments", "ApartmanTypeId", "dbo.ApartmanTypes", "Id", cascadeDelete: true);
            DropColumn("dbo.Apartments", "Type");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Apartments", "Type", c => c.Int(nullable: false));
            DropForeignKey("dbo.Apartments", "ApartmanTypeId", "dbo.ApartmanTypes");
            DropIndex("dbo.Apartments", new[] { "ApartmanTypeId" });
            DropColumn("dbo.Apartments", "ApartmanTypeId");
            DropTable("dbo.ApartmanTypes",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_ApartmanType_IsDeleted", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
        }
    }
}
