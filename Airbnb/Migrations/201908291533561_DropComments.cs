namespace Airbnb.Migrations
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity.Infrastructure.Annotations;
    using System.Data.Entity.Migrations;
    
    public partial class DropComments : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Comments", "ApartmentId", "dbo.Apartments");
            DropIndex("dbo.Comments", new[] { "ApartmentId" });
            DropTable("dbo.Comments",
                removedAnnotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_Comment_IsDeleted", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                });
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.Comments",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ApartmentId = c.Int(nullable: false),
                        Review = c.String(nullable: false),
                        Grade = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                    },
                annotations: new Dictionary<string, object>
                {
                    { "DynamicFilter_Comment_IsDeleted", "EntityFramework.DynamicFilters.DynamicFilterDefinition" },
                })
                .PrimaryKey(t => t.Id);
            
            CreateIndex("dbo.Comments", "ApartmentId");
            AddForeignKey("dbo.Comments", "ApartmentId", "dbo.Apartments", "Id", cascadeDelete: true);
        }
    }
}
