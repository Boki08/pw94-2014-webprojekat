namespace Airbnb.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Reservation_Commented : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Reservations", "Commented", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Reservations", "Commented");
        }
    }
}
