namespace Airbnb.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Comment_Approved : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Comments", "Approved", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Comments", "Approved");
        }
    }
}
