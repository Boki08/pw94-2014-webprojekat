namespace Airbnb.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AppUser_UserName : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.AppUsers", "UserName", c => c.String(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.AppUsers", "UserName");
        }
    }
}
