namespace Airbnb.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Amenity_Desc_NotReq : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Amenities", "Description", c => c.String());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Amenities", "Description", c => c.String(nullable: false));
        }
    }
}
