namespace Airbnb.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class DatabaseModel : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Addresses",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        LocationId = c.Int(nullable: false),
                        StreetAndNumber = c.String(nullable: false),
                        City = c.String(nullable: false),
                        PostalCode = c.Double(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Locations", t => t.LocationId, cascadeDelete: true)
                .Index(t => t.LocationId);
            
            CreateTable(
                "dbo.Locations",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ApartmentId = c.Int(nullable: false),
                        Latitude = c.Double(nullable: false),
                        Longitude = c.Double(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Apartments", t => t.ApartmentId, cascadeDelete: true)
                .Index(t => t.ApartmentId);
            
            CreateTable(
                "dbo.Apartments",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        AppUserId = c.Int(nullable: false),
                        Type = c.Int(nullable: false),
                        NumOfRooms = c.Int(nullable: false),
                        NumOfGuests = c.Int(nullable: false),
                        Price = c.Double(nullable: false),
                        CheckInTime = c.DateTime(nullable: false),
                        CheckOutTime = c.DateTime(nullable: false),
                        Status = c.Boolean(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AppUsers", t => t.AppUserId, cascadeDelete: true)
                .Index(t => t.AppUserId);
            
            CreateTable(
                "dbo.Amenities",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ApartmentId = c.Int(nullable: false),
                        Type = c.String(nullable: false),
                        Description = c.String(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Apartments", t => t.ApartmentId, cascadeDelete: true)
                .Index(t => t.ApartmentId);
            
            CreateTable(
                "dbo.Reservations",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        AppUserId = c.Int(),
                        ApartmentId = c.Int(nullable: false),
                        StatusId = c.Int(nullable: false),
                        ReservationDate = c.DateTime(nullable: false),
                        NumOfNights = c.Int(nullable: false),
                        Price = c.Double(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Apartments", t => t.ApartmentId, cascadeDelete: true)
                .ForeignKey("dbo.AppUsers", t => t.AppUserId)
                .ForeignKey("dbo.Statuses", t => t.StatusId, cascadeDelete: true)
                .Index(t => t.AppUserId)
                .Index(t => t.ApartmentId)
                .Index(t => t.StatusId);
            
            CreateTable(
                "dbo.Statuses",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Description = c.String(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Comments",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ApartmentId = c.Int(nullable: false),
                        Review = c.String(nullable: false),
                        Grade = c.Int(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Apartments", t => t.ApartmentId, cascadeDelete: true)
                .Index(t => t.ApartmentId);
            
            CreateTable(
                "dbo.ApartmentPictures",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ApartmentId = c.Int(nullable: false),
                        Data = c.String(nullable: false),
                        IsDeleted = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Apartments", t => t.ApartmentId, cascadeDelete: true)
                .Index(t => t.ApartmentId);
            
            CreateTable(
                "dbo.Dates",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ApartmentId = c.Int(nullable: false),
                        StartDate = c.DateTime(nullable: false),
                        EndDate = c.DateTime(nullable: false),
                        Apartment_Id = c.Int(),
                        Apartment_Id1 = c.Int(),
                        IsDeleted = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Apartments", t => t.ApartmentId, cascadeDelete: true)
                .ForeignKey("dbo.Apartments", t => t.Apartment_Id)
                .ForeignKey("dbo.Apartments", t => t.Apartment_Id1)
                .Index(t => t.ApartmentId)
                .Index(t => t.Apartment_Id)
                .Index(t => t.Apartment_Id1);
            
            AddColumn("dbo.AppUsers", "Name", c => c.String(nullable: false));
            AddColumn("dbo.AppUsers", "Surname", c => c.String(nullable: false));
            AddColumn("dbo.AppUsers", "Gender", c => c.String(nullable: false));
            AddColumn("dbo.AppUsers", "IsDeleted", c => c.Boolean(nullable: false));
            DropColumn("dbo.AppUsers", "FullName");
        }
        
        public override void Down()
        {
            AddColumn("dbo.AppUsers", "FullName", c => c.String());
            DropForeignKey("dbo.Addresses", "LocationId", "dbo.Locations");
            DropForeignKey("dbo.Locations", "ApartmentId", "dbo.Apartments");
            DropForeignKey("dbo.Dates", "Apartment_Id1", "dbo.Apartments");
            DropForeignKey("dbo.Dates", "Apartment_Id", "dbo.Apartments");
            DropForeignKey("dbo.Dates", "ApartmentId", "dbo.Apartments");
            DropForeignKey("dbo.ApartmentPictures", "ApartmentId", "dbo.Apartments");
            DropForeignKey("dbo.Comments", "ApartmentId", "dbo.Apartments");
            DropForeignKey("dbo.Apartments", "AppUserId", "dbo.AppUsers");
            DropForeignKey("dbo.Reservations", "StatusId", "dbo.Statuses");
            DropForeignKey("dbo.Reservations", "AppUserId", "dbo.AppUsers");
            DropForeignKey("dbo.Reservations", "ApartmentId", "dbo.Apartments");
            DropForeignKey("dbo.Amenities", "ApartmentId", "dbo.Apartments");
            DropIndex("dbo.Dates", new[] { "Apartment_Id1" });
            DropIndex("dbo.Dates", new[] { "Apartment_Id" });
            DropIndex("dbo.Dates", new[] { "ApartmentId" });
            DropIndex("dbo.ApartmentPictures", new[] { "ApartmentId" });
            DropIndex("dbo.Comments", new[] { "ApartmentId" });
            DropIndex("dbo.Reservations", new[] { "StatusId" });
            DropIndex("dbo.Reservations", new[] { "ApartmentId" });
            DropIndex("dbo.Reservations", new[] { "AppUserId" });
            DropIndex("dbo.Amenities", new[] { "ApartmentId" });
            DropIndex("dbo.Apartments", new[] { "AppUserId" });
            DropIndex("dbo.Locations", new[] { "ApartmentId" });
            DropIndex("dbo.Addresses", new[] { "LocationId" });
            DropColumn("dbo.AppUsers", "IsDeleted");
            DropColumn("dbo.AppUsers", "Gender");
            DropColumn("dbo.AppUsers", "Surname");
            DropColumn("dbo.AppUsers", "Name");
            DropTable("dbo.Dates");
            DropTable("dbo.ApartmentPictures");
            DropTable("dbo.Comments");
            DropTable("dbo.Statuses");
            DropTable("dbo.Reservations");
            DropTable("dbo.Amenities");
            DropTable("dbo.Apartments");
            DropTable("dbo.Locations");
            DropTable("dbo.Addresses");
        }
    }
}
