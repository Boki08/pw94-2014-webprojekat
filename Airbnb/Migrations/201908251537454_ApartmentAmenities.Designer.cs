// <auto-generated />
namespace Airbnb.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.1.1-30610")]
    public sealed partial class ApartmentAmenities : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(ApartmentAmenities));
        
        string IMigrationMetadata.Id
        {
            get { return "201908251537454_ApartmentAmenities"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
