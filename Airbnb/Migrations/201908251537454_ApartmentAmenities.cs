namespace Airbnb.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ApartmentAmenities : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Amenities", "ApartmentId", "dbo.Apartments");
            DropIndex("dbo.Amenities", new[] { "ApartmentId" });
            CreateTable(
                "dbo.ApartmentAmenities",
                c => new
                    {
                        ApartmentId = c.Int(nullable: false),
                        AmenityId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.ApartmentId, t.AmenityId })
                .ForeignKey("dbo.Apartments", t => t.ApartmentId, cascadeDelete: true)
                .ForeignKey("dbo.Amenities", t => t.AmenityId, cascadeDelete: true)
                .Index(t => t.ApartmentId)
                .Index(t => t.AmenityId);
            
            DropColumn("dbo.Amenities", "ApartmentId");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Amenities", "ApartmentId", c => c.Int(nullable: false));
            DropForeignKey("dbo.ApartmentAmenities", "AmenityId", "dbo.Amenities");
            DropForeignKey("dbo.ApartmentAmenities", "ApartmentId", "dbo.Apartments");
            DropIndex("dbo.ApartmentAmenities", new[] { "AmenityId" });
            DropIndex("dbo.ApartmentAmenities", new[] { "ApartmentId" });
            DropTable("dbo.ApartmentAmenities");
            CreateIndex("dbo.Amenities", "ApartmentId");
            AddForeignKey("dbo.Amenities", "ApartmentId", "dbo.Apartments", "Id", cascadeDelete: true);
        }
    }
}
