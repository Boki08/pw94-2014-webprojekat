namespace Airbnb.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class DropDate : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Dates", "ApartmentId", "dbo.Apartments");
            DropForeignKey("dbo.Dates", "Apartment_Id", "dbo.Apartments");
            DropForeignKey("dbo.Dates", "Apartment_Id1", "dbo.Apartments");
            DropIndex("dbo.Dates", new[] { "ApartmentId" });
            DropIndex("dbo.Dates", new[] { "Apartment_Id" });
            DropIndex("dbo.Dates", new[] { "Apartment_Id1" });
            DropColumn("dbo.Dates", "ApartmentId");
            DropColumn("dbo.Dates", "Apartment_Id");
            DropColumn("dbo.Dates", "Apartment_Id1");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Dates", "Apartment_Id1", c => c.Int());
            AddColumn("dbo.Dates", "Apartment_Id", c => c.Int());
            AddColumn("dbo.Dates", "ApartmentId", c => c.Int(nullable: false));
            CreateIndex("dbo.Dates", "Apartment_Id1");
            CreateIndex("dbo.Dates", "Apartment_Id");
            CreateIndex("dbo.Dates", "ApartmentId");
            AddForeignKey("dbo.Dates", "Apartment_Id1", "dbo.Apartments", "Id");
            AddForeignKey("dbo.Dates", "Apartment_Id", "dbo.Apartments", "Id");
            AddForeignKey("dbo.Dates", "ApartmentId", "dbo.Apartments", "Id", cascadeDelete: true);
        }
    }
}
