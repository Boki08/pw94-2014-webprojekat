﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Airbnb.Models.Entities
{
    public class Apartment : ILogicalDelete
    {
        public int Id { get; set; }
        [ForeignKey("AppUser")]
        public int AppUserId { get; set; }
        [ForeignKey("ApartmentType")]
        public int ApartmentTypeId { get; set; }
        //[ForeignKey("Amenity")]
        //public int AmenityId { get; set; }
        [Required]
        public int NumOfRooms { get; set; }
        [Required]
        public int NumOfGuests { get; set; }
        public double Price { get; set; }
        [Required]
        public DateTime CheckInTime { get; set; }
        [Required]
        public DateTime CheckOutTime { get; set; }
        [Required]
        public bool Status { get; set; }
        [Required]
        public double Grade { get; set; }
        [Required]
        public bool IsDeleted { get; set; }

        public AppUser AppUser { get; set; }
        public ApartmentType ApartmentType { get; set; }

        public List<Amenity> Amenities { get; set; }
        public  List<ApartmentPicture> Pictures { get; set; }
        [JsonIgnore]
        public List<Reservation> Reservations { get; set; }
        public  List<Comment> Comments { get; set; }
        public List<RentingDate> RentingDates { get; set; }
        public List<FreeDate> FreeDates { get; set; }
        public List<Location> Location { get; set; }
    }
}