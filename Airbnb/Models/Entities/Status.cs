﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Airbnb.Models.Entities
{
    [Table("Statuses")]
    public class Status : ILogicalDelete
    {
        public int Id { get; set; }
        [Required]
        public string Description { get; set; }
        [Required]
        public bool IsDeleted { get; set; }

        public List<Reservation> Reservations { get; set; }
    }
}