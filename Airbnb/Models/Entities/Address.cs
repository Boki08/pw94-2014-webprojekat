﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Airbnb.Models.Entities
{
    public class Address : ILogicalDelete
    {
        public int Id { get; set; }
        [ForeignKey("Location")]
        public int LocationId { get; set; }
        [Required]
        public string StreetAndNumber { get; set; }
        [Required]
        public string City { get; set; }
        [Required]
        public double PostalCode { get; set; }
        [Required]
        public bool IsDeleted { get; set; }

        public Location Location { get; set; }
    }
}