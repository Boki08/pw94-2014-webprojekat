﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Airbnb.Models.Entities
{
    public class ApartmentType : ILogicalDelete
    {
        public int Id { get; set; }
        [Required]
        public string Type { get; set; }
        [Required]
        public bool IsDeleted { get; set; }
        public List<Apartment> Apartments { get; set; }
        
    }
}