﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Airbnb.Models.Entities
{
    public class ApartmentPicture : ILogicalDelete
    {
        public int Id { get; set; }
        [ForeignKey("Apartment")]
        public int ApartmentId { get; set; }
        [Required]
        public string Data { get; set; }
        [Required]
        public bool IsDeleted { get; set; }
        public Apartment Apartment { get; set; }
    }
}