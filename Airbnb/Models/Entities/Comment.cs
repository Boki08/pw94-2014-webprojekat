﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Airbnb.Models.Entities
{
    public class Comment : ILogicalDelete
    {
        public int Id { get; set; }
        [ForeignKey("Apartment")]
        public int ApartmentId { get; set; }
        [ForeignKey("AppUser")]
        public int? AppUserId { get; set; }
        [Required]
        public string Review { get; set; }
        [Required]
        public int Grade { get; set; }
        [Required]
        public DateTime PostedDate { get; set; }
        [Required]
        public bool Approved { get; set; }
        [Required]
        public bool IsDeleted { get; set; }
        public virtual Apartment Apartment { get; set; }
        public virtual AppUser AppUser { get; set; }
        
    }
}