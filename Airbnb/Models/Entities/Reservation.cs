﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
namespace Airbnb.Models.Entities
{
    public class Reservation : ILogicalDelete
    {
        public int Id { get; set; }
        [ForeignKey("AppUser")]
        public int? AppUserId { get; set; }
        [ForeignKey("Apartment")]
        public int ApartmentId { get; set; }
        [ForeignKey("Status")]
        public int StatusId { get; set; }
        public DateTime ReservationDate { get; set; }
        public int NumOfNights { get; set; }
        public double Price { get; set; }
        public bool Commented { get; set; }
        [Required]
        public bool IsDeleted { get; set; }

        public Status Status { get; set; }

        public AppUser AppUser { get; set; }
       
        public Apartment Apartment { get; set; }


    }
}