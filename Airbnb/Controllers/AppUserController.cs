﻿using Airbnb.Models;
using Airbnb.Models.Entities;
using Airbnb.Persistence;
using Airbnb.Persistence.UnitOfWork;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Http.Cors;

namespace Airbnb.Controllers
{

    [RoutePrefix("api/appUser")]
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class AppUserController : ApiController
    {
        private readonly IUnitOfWork _unitOfWork;
        private ApplicationUserManager _userManager;

        public AppUserController(IUnitOfWork unitOfWork, ApplicationUserManager userManager)
        {
            UserManager = userManager;
            this._unitOfWork = unitOfWork;
        }
        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? Request.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }
        [HttpGet]
        [Route("getCurrentUser")]
        public IHttpActionResult GetAppUser()
        {
            AppUser appUser;
            try
            {
                var username = User.Identity.Name;

                var user = _unitOfWork.AppUsers.Find(u => u.UserName == username).FirstOrDefault();
                if (user == null)
                {
                    return BadRequest("Data could not be retrieved, try to relog.");
                }
                appUser = user;

                return Ok(appUser);
            }
            catch
            {
                return BadRequest("Data could not be retrieved, try to relog.");
            }
        }
        [Authorize(Roles = "Admin")]
        [HttpPost]
        [Route("registerHost")]
        public IHttpActionResult RegisterHost(RegisterBindingModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            AppUser appUser = new AppUser() { UserName = model.UserName, Name = model.Name, Surname = model.Surname, Gender = model.Gender,Blocked=false };
            var user = new AirbnbIdentityUser() { UserName = model.UserName, AppUser = appUser, PasswordHash = AirbnbIdentityUser.HashPassword(model.Password) };

            IdentityResult result = UserManager.Create(user, model.Password);

            if (!result.Succeeded)
            {
                return GetErrorResult(result);
            }
            UserManager.AddToRole(user.Id, "Host");
            return Ok();
        }
        [HttpPost]
        [Authorize(Roles = "Admin, Host, AppUser")]
        [Route("editAppUser")]
        public IHttpActionResult EditUser(AppUser editedUser)
        {

            AppUser appUser;
            try
            {
                var username = User.Identity.Name;

                var user = _unitOfWork.AppUsers.Find(u => u.UserName == username).FirstOrDefault();
                if (user == null)
                {
                    return BadRequest("Data could not be retrieved, try to relog.");
                }
                appUser = user;

            }
            catch
            {
                return BadRequest("Data could not be retrieved, try to relog.");
            }

            appUser.UserName = editedUser.UserName;
            appUser.Name = editedUser.Name;
            appUser.Surname = editedUser.Surname;
            appUser.Gender = editedUser.Gender;

            AirbnbIdentityUser userIdentity = UserManager.FindById(User.Identity.GetUserId());

            string usernameTemp;
            usernameTemp = userIdentity.UserName;
            //idTemp = userIdentity.Id;

            userIdentity.UserName = appUser.UserName;
            //userIdentity.Id = appUser.UserName;



            IdentityResult result = UserManager.Update(userIdentity);
            if (!result.Succeeded)
            {
                return GetErrorResult(result);
            }


            try
            {

                _unitOfWork.AppUsers.Update(appUser);
                _unitOfWork.Complete();

            }
            catch
            {
                userIdentity.UserName = usernameTemp;
                //userIdentity.Id = idTemp;
                UserManager.Update(userIdentity);

                return BadRequest("Profile could not be edited.");
            }


            //            catch (System.Data.Entity.Validation.DbEntityValidationException e)
            //            {
            //                string rs = "";
            //                foreach (var eve in e.EntityValidationErrors)
            //                {
            //                    rs = string.Format("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:", eve.Entry.Entity.GetType().Name, eve.Entry.State);
            //        Console.WriteLine(rs);

            //                    foreach (var ve in eve.ValidationErrors)
            //                    {
            //                        rs += "<br />" + string.Format("- Property: \"{0}\", Error: \"{1}\"", ve.PropertyName, ve.ErrorMessage);
            //    }
            //}
            //                throw new Exception(rs);
            //            }


            return Ok(appUser);
        }

        private IHttpActionResult GetErrorResult(IdentityResult result)
        {
            if (result == null)
            {
                return InternalServerError();
            }

            if (!result.Succeeded)
            {
                if (result.Errors != null)
                {
                    foreach (string error in result.Errors)
                    {
                        ModelState.AddModelError("", error);
                    }
                }

                if (ModelState.IsValid)
                {
                    // No ModelState errors are available to send, so just return an empty BadRequest.
                    return BadRequest();
                }

                return BadRequest(ModelState);
            }

            return null;
        }
        [Authorize(Roles = "Admin")]
        [HttpGet]
        [Route("allUsers/{pageIndex}/{pageSize}/{selectedRole}/{gender}/{username}")]
        public IHttpActionResult GetAllUsers(int pageIndex, int pageSize, string selectedRole, string gender, string username)
        {
            List<AppUser> source;
            if (selectedRole != "notSet")
            {
                AirbnbDBContext db = new AirbnbDBContext();
                var role = db.Roles.SingleOrDefault(m => m.Name == selectedRole);
                var usersByRole = db.Users.Include(x => x.AppUser).Where(m => m.Roles.All(r => r.RoleId == role.Id));
                if (gender != "notSet" && username != "notSet")
                {
                    source = usersByRole.Select(x => x.AppUser).Where(x => x.Gender == gender && x.UserName.Contains(username)).ToList();
                }
                else if (gender != "notSet" && username == "notSet")
                {
                    source = usersByRole.Select(x => x.AppUser).Where(x => x.Gender == gender).ToList();
                }
                else if (gender == "notSet" && username != "notSet")
                {
                    source = usersByRole.Select(x => x.AppUser).Where(x => x.UserName.Contains(username)).ToList();
                }
                else
                {
                    source = usersByRole.Select(x => x.AppUser).ToList();
                }
                if (source == null || source.Count() < 1)
                {
                    return BadRequest("There are no Users");
                }

            }
            else
            {
                source = _unitOfWork.AppUsers.SearchUsersGenderUsername( gender, username).ToList();

                if (source == null || source.Count() < 1)
                {
                    return BadRequest("There are no Users");
                }
            }


            // Get's No of Rows Count   
            int count = source.Count();

            source = source.Skip((pageIndex - 1) * pageSize).Take(pageSize).ToList();

            // Display TotalCount to Records to User  
            int TotalCount = count;

            // Calculating Totalpage by Dividing (No of Records / Pagesize)  
            int TotalPages = (int)Math.Ceiling(count / (double)pageSize);

            // Returns List of Customer after applying Paging   
            var items = source.Skip((pageIndex - 1) * pageSize).Take(pageSize).ToList();


            // Object which we are going to send in header   
            var paginationMetadata = new
            {
                totalCount = TotalCount,
                pageSize,
                currentPage = pageIndex,
                totalPages = TotalPages
            };

            // Setting Header  
            HttpContext.Current.Response.Headers.Add("Access-Control-Expose-Headers", "Paging-Headers");
            HttpContext.Current.Response.Headers.Add("Paging-Headers", JsonConvert.SerializeObject(paginationMetadata));

            return Ok(items);
        }

        [Authorize(Roles = "Admin")]
        [HttpGet]
        [Route("getUserById/{userId}")]
        public IHttpActionResult GetAppUserById(int userId)
        {
            try
            {
                AirbnbDBContext db = new AirbnbDBContext();
                var user = db.Users.Include(x=>x.AppUser).SingleOrDefault(m => m.AppUserId == userId);
                var role= db.Roles.Where(m => m.Users.Any(n=>n.UserId == user.Id)).FirstOrDefault().Name;
                //var usersByRole = db.Users.Include(x => x.AppUser).Where(m => m.Roles.All(r => r.RoleId == role.Id));

                // var appUser = _unitOfWork.AppUsers.Get(userId);
                var appUser = user.AppUser;

                var paginationMetadata = new
                {
                    userRole = role
                };

                // Setting Header  
                HttpContext.Current.Response.Headers.Add("Access-Control-Expose-Headers", "Paging-Headers");
                HttpContext.Current.Response.Headers.Add("Paging-Headers", JsonConvert.SerializeObject(paginationMetadata));

                return Ok(appUser);
            }
            catch
            {
                return BadRequest("User could not be found");
            }
        }
        [Authorize(Roles = "Admin")]
        [HttpGet]
        [Route("activateUser/{userId}/{activated}")]
        public IHttpActionResult ApproveComment(int userId, bool activated)
        {
            AppUser user;
            try
            {

                user = _unitOfWork.AppUsers.Get(userId);
                if (user == null)
                {
                    return BadRequest("User could not be found");
                }
            }
            catch
            {
                return BadRequest("User could not be found");
            }
            user.Blocked = activated;
            try
            {
                _unitOfWork.AppUsers.Update(user);
                _unitOfWork.Complete();
            }
            catch
            {
                if (user.Blocked)
                {
                    return BadRequest("User could not be banned");
                }
                else
                {
                    return BadRequest("User could not be unbanned");
                }
            }
            return Ok(user);
        }
    }
}