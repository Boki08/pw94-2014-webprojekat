﻿using Airbnb.Persistence.UnitOfWork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;

namespace Airbnb.Controllers
{
    [RoutePrefix("api/apartmentType")]
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class ApartmentTypeController : ApiController
    {
        private readonly IUnitOfWork _unitOfWork;

        public ApartmentTypeController(IUnitOfWork unitOfWork)
        {
            this._unitOfWork = unitOfWork;
        }

        [HttpGet]
        [Route("getAllApartmentTypes/")]
        public IHttpActionResult getAllApartmentTypes()
        {

            var items = _unitOfWork.ApartmentTypes.GetAll().ToList();

            if (items == null || items.Count < 1)
            {
                return BadRequest("There are no ApartmentTypes");
            }

            return Ok(items);
        }
    }
}
