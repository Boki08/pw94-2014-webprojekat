﻿using Airbnb.Models.Entities;
using Airbnb.Persistence.UnitOfWork;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Http.Cors;

namespace Airbnb.Controllers
{
    [RoutePrefix("api/amenity")]
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class AmenityController : ApiController
    {
        private readonly IUnitOfWork _unitOfWork;

        public AmenityController(IUnitOfWork unitOfWork)
        {
            this._unitOfWork = unitOfWork;
        }
        [HttpGet]
        [Route("getAllAmenities")]
        public IHttpActionResult getAllApartmentTypes()
        {

            var items = _unitOfWork.Amenities.GetAll().ToList();

            if (items == null || items.Count < 1)
            {
                return BadRequest("There are no Amenities");
            }

            return Ok(items);
        }
        [HttpGet]
        [Authorize(Roles = "Admin")]
        [Route("getAllAmenitiesAdmin/{pageIndex}/{pageSize}")]
        public IHttpActionResult getAllAmenitiesAdmin(int pageIndex, int pageSize)
        {

            var items = _unitOfWork.Amenities.GetAll().ToList();

            if (items == null || items.Count < 1)
            {
                return BadRequest("There are no Amenities");
            }
            int count = items.Count();



            int TotalCount = count;
            items = items.Skip((pageIndex - 1) * pageSize).Take(pageSize).ToList();

            // Calculating Totalpage by Dividing (No of Records / Pagesize)  
            int TotalPages = (int)Math.Ceiling(count / (double)pageSize);


            // Object which we are going to send in header   
            var paginationMetadata = new
            {
                totalCount = TotalCount,
                pageSize,
                currentPage = pageIndex,
                totalPages = TotalPages
            };

            // Setting Header  
            HttpContext.Current.Response.Headers.Add("Access-Control-Expose-Headers", "Paging-Headers");
            HttpContext.Current.Response.Headers.Add("Paging-Headers", JsonConvert.SerializeObject(paginationMetadata));
            // Returing List of Customers Collections  

            return Ok(items);
        }
        [HttpPost]
        [Authorize(Roles = "Admin")]
        [Route("addAmenity")]
        public IHttpActionResult AddAmenity(Amenity amenity)
        {

            //amenity.IsDeleted = false;
            try
            {
                _unitOfWork.Amenities.Add(amenity);
                _unitOfWork.Complete();
            }
            catch
            {
                return BadRequest("Amenity could not be added");
            }

            return Created("Amenity added", amenity);
        }
        [HttpPost]
        [Authorize(Roles = "Admin")]
        [Route("editAmenity")]
        public IHttpActionResult EditAmenity(Amenity amenity)
        {

            amenity.IsDeleted = false;
            try
            {
                _unitOfWork.Amenities.Update(amenity);
                _unitOfWork.Complete();
            }
            catch
            {
                return BadRequest("Amenity could not be edited");
            }

            return Created("Amenity edited", amenity);
        }
        [HttpGet]
        [Authorize(Roles = "Admin")]
        [Route("deleteAmenity/{amenityId}")]
        public IHttpActionResult DeleteAmenity(int amenityId)
        {

            Amenity amenity;
            try
            {
                amenity= _unitOfWork.Amenities.Get(amenityId);
            }
            catch
            {
                return BadRequest("Amenity could not be found");
            }
            if (amenity == null)
            {
                return BadRequest("Amenity could not be found");
            }

            try
            {
                _unitOfWork.Amenities.Remove(amenity);
                _unitOfWork.Complete();
            }
            catch
            {
                return BadRequest("Amenity could not be deleted");
            }

            return Ok("Amenity deleted");
        }
    }
}
