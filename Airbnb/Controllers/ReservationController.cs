﻿using Airbnb.Models.Entities;
using Airbnb.Persistence.UnitOfWork;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Http.Cors;

namespace Airbnb.Controllers
{
    [RoutePrefix("api/reservation")]
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class ReservationController : ApiController
    {
        private readonly IUnitOfWork _unitOfWork;

        public ReservationController(IUnitOfWork unitOfWork)
        {
            this._unitOfWork = unitOfWork;
        }

        [Authorize(Roles = "Guest")]
        [HttpGet]
        [Route("getAllUserReservations/{pageIndex}/{pageSize}/{sortType}")]
        public IHttpActionResult getAllUserReservations(int pageIndex, int pageSize, string sortType)
        {

            AppUser appUser;
            try
            {
                var username = User.Identity.Name;

                var user = _unitOfWork.AppUsers.Find(u => u.UserName == username).FirstOrDefault();
                if (user == null)
                {
                    return BadRequest("Data could not be retrieved, try to relog.");
                }
                appUser = user;
            }
            catch
            {
                return BadRequest("Data could not be retrieved, try to relog.");
            }

            List<Reservation> reservations=new List<Reservation>();
            try
            {
                reservations = _unitOfWork.Reservations.GetAllUserReservationsSort( appUser.Id, sortType).ToList();
            }
            catch
            {
                return BadRequest("There are no reservations");
            }
            if (reservations == null || reservations.Count<1)
            {
                return BadRequest("There are no reservations");
            }


            int count = reservations.Count();

            reservations = reservations.Skip((pageIndex - 1) * pageSize).Take(pageSize).ToList();
            int TotalCount = count;

            // Calculating Totalpage by Dividing (No of Records / Pagesize)  
            int TotalPages = (int)Math.Ceiling(count / (double)pageSize);


            // Object which we are going to send in header   
            var paginationMetadata = new
            {
                totalCount = TotalCount,
                pageSize,
                currentPage = pageIndex,
                totalPages = TotalPages
            };

            // Setting Header  
            HttpContext.Current.Response.Headers.Add("Access-Control-Expose-Headers", "Paging-Headers");
            HttpContext.Current.Response.Headers.Add("Paging-Headers", JsonConvert.SerializeObject(paginationMetadata));
            // Returing List of Customers Collections  



            return Ok(reservations);
        }
        [Authorize(Roles = "Guest")]
        [HttpGet]
        [Route("getUserReservation/{reservationId}")]
        public IHttpActionResult getUserReservation(int reservationId)
        {

            AppUser appUser;
            try
            {
                var username = User.Identity.Name;

                var user = _unitOfWork.AppUsers.Find(u => u.UserName == username).FirstOrDefault();
                if (user == null)
                {
                    return BadRequest("Data could not be retrieved, try to relog.");
                }
                appUser = user;
            }
            catch
            {
                return BadRequest("Data could not be retrieved, try to relog.");
            }

            Reservation reservation;
            try
            {
                reservation = _unitOfWork.Reservations.GetUserReservation(appUser.Id, reservationId);
            }
            catch
            {
                return BadRequest("Reservation does not exist");
            }
            if (reservation == null)
            {
                return BadRequest("Reservation does not exist");
            }

            return Ok(reservation);
        }

        [Authorize(Roles = "Guest")]
        [HttpGet]
        [Route("cancelReservation/{reservationId}")]
        public IHttpActionResult cancelReservation(int reservationId)
        {
            int guestId;
            try
            {
                var username = User.Identity.Name;

                var user = _unitOfWork.AppUsers.Find(u => u.UserName == username).FirstOrDefault();
                if (user == null)
                {
                    return BadRequest("Data could not be retrieved, try to relog.");
                }
                guestId = user.Id;
            }
            catch
            {
                return BadRequest("Data could not be retrieved, try to relog.");
            }

            Reservation reservation;
            try
            {
                reservation = _unitOfWork.Reservations.GetUserReservation(guestId, reservationId);
            }
            catch
            {
                return BadRequest("Reservation does not exist");
            }
            if (reservation == null)
            {
                return BadRequest("Reservation does not exist");
            }

            if (reservation.Status.Description == "Canceled" || reservation.Status.Description == "Rejected" || reservation.Status.Description == "Completed")
            {
                return BadRequest("Reservation was already canceled, rejected or completed");
            }


            Status status;
            try
            {
                status = _unitOfWork.Statuses.Find(x => x.Description == "Canceled").FirstOrDefault();
                if (status == null)
                {
                    return BadRequest("Data could not be retrieved, try to relog.");
                }
            }
            catch
            {
                return BadRequest("Data could not be retrieved, try to relog.");
            }


            List<FreeDate> freeDates = _unitOfWork.FreeDates.Find(x => x.ApartmentId == reservation.ApartmentId).ToList();
            FreeDate selectedDate = new FreeDate() { ApartmentId = reservation.ApartmentId };
            bool dateAdded = false;
            foreach (FreeDate freeDate in freeDates)
            {
                if (freeDate.EndDate.AddDays(1) == reservation.ReservationDate)
                {
                    selectedDate = freeDate;
                    selectedDate.EndDate = reservation.ReservationDate.AddDays(reservation.NumOfNights);
                    foreach (FreeDate freeDate2 in freeDates)
                    {
                        if (selectedDate.EndDate.AddDays(1) == freeDate2.StartDate)
                        {
                            freeDate2.StartDate = selectedDate.StartDate;
                            _unitOfWork.FreeDates.Update(freeDate2);
                            _unitOfWork.Complete();
                            _unitOfWork.FreeDates.Remove(selectedDate);
                            _unitOfWork.Complete();
                            dateAdded = true;
                            break;
                        }
                    }
                    if (dateAdded == false)
                    {
                        _unitOfWork.FreeDates.Update(selectedDate);
                        _unitOfWork.Complete();
                        dateAdded = true;
                    }
                }
                else if (freeDate.StartDate.Subtract(TimeSpan.FromDays(1)) == reservation.ReservationDate.AddDays(reservation.NumOfNights))
                {
                    selectedDate = freeDate;
                    selectedDate.StartDate = reservation.ReservationDate;
                    foreach (FreeDate freeDate2 in freeDates)
                    {
                        if (selectedDate.StartDate.Subtract(TimeSpan.FromDays(1)) == freeDate2.EndDate)
                        {
                            freeDate2.EndDate = selectedDate.EndDate;
                            _unitOfWork.FreeDates.Update(freeDate2);
                            _unitOfWork.Complete();
                            dateAdded = true;
                            break;
                        }
                    }
                    if (dateAdded == false)
                    {
                        _unitOfWork.FreeDates.Update(selectedDate);
                        _unitOfWork.Complete();
                        dateAdded = true;
                    }
                }
            }
            if (dateAdded == false)
            {
                selectedDate.StartDate = reservation.ReservationDate;
                selectedDate.EndDate = reservation.ReservationDate.AddDays(reservation.NumOfNights);
                _unitOfWork.FreeDates.Add(selectedDate);
                _unitOfWork.Complete();
            }





            reservation.Status = status;

            try
            {
                _unitOfWork.Reservations.Update(reservation);
                _unitOfWork.Complete();
            }
            catch
            {
                return BadRequest("Reservation could not be canceled");
            }
            return Created("Reservation was canceled", reservation);
        }

        [Authorize(Roles = "Guest")]
        [HttpGet]
        [Route("rentApartment/{apartmentId}")]
        public IHttpActionResult rentApartment(int apartmentId, [FromUri(Name = "fromDate")] DateTime fromDate, [FromUri(Name = "toDate")] DateTime toDate)
        {

            AppUser appUser;
            try
            {
                var username = User.Identity.Name;

                var user = _unitOfWork.AppUsers.Find(u => u.UserName == username).FirstOrDefault();
                if (user == null)
                {
                    return BadRequest("Data could not be retrieved, try to relog.");
                }
                appUser = user;
            }
            catch
            {
                return BadRequest("Data could not be retrieved, try to relog.");
            }
            Apartment apartment;
            try
            {
                apartment = _unitOfWork.Apartments.GetEnabledApartment(apartmentId);
                if (apartment == null)
                {
                    return BadRequest("Data could not be retrieved, try to relog.");
                }
            }
            catch
            {
                return BadRequest("Data could not be retrieved, try to relog.");
            }
            Status status;
            try
            {
                status = _unitOfWork.Statuses.Find(x=>x.Description== "Created").FirstOrDefault();
                if (status == null)
                {
                    return BadRequest("Data could not be retrieved, try to relog.");
                }
            }
            catch
            {
                return BadRequest("Data could not be retrieved, try to relog.");
            }

            fromDate = fromDate.Date;
            toDate = toDate.Date;
            double numberfNights = (toDate - fromDate).TotalDays;
            double price = apartment.Price * numberfNights;
           
            Reservation reservation = new Reservation() { Apartment = apartment, AppUserId = appUser.Id, IsDeleted = false, Commented = false,ReservationDate= fromDate.Date, NumOfNights=(Int32)numberfNights,Price= price ,Status= status};


            List<FreeDate> freeDates = _unitOfWork.FreeDates.Find(x=>x.ApartmentId== apartmentId).ToList();
            FreeDate selectedDate = new FreeDate() { ApartmentId = apartmentId }, newEarlyDate=new FreeDate() { ApartmentId=apartmentId},newLaterDate = new FreeDate() { ApartmentId = apartmentId };
            foreach (FreeDate freeDate in freeDates)
            {
                if(freeDate.StartDate<= fromDate && freeDate.EndDate >= toDate)
                {
                    selectedDate = freeDate;
                    break;
                }
            }
           
           if(selectedDate.StartDate< fromDate)
            {
                newEarlyDate.StartDate = selectedDate.StartDate;
               // newEarlyDate.EndDate = fromDate;
                newEarlyDate.EndDate= fromDate.Subtract(TimeSpan.FromDays(1));
                

                _unitOfWork.FreeDates.Add(newEarlyDate);
                _unitOfWork.Complete();
            }
            if (selectedDate.EndDate > toDate)
            {
                //newLaterDate.StartDate = toDate;
                newLaterDate.StartDate = toDate.AddDays(1);
                newLaterDate.EndDate = selectedDate.EndDate;
             

                _unitOfWork.FreeDates.Add(newLaterDate);
                _unitOfWork.Complete();
            }
            _unitOfWork.FreeDates.Remove(selectedDate);
            _unitOfWork.Complete();

            try
            {
                _unitOfWork.Reservations.Add(reservation);
                _unitOfWork.Complete();
            }
            catch
            {
                return BadRequest("Reservation could not be created");
            }


            return Created("Reservation was created!", reservation);
        }

        [Authorize(Roles = "Host")]
        [HttpGet]
        [Route("getAllHostReservations/{pageIndex}/{pageSize}/{sortType}/{selectedUsername}")]
        public IHttpActionResult getAllHostReservations(int pageIndex, int pageSize, string sortType,string selectedUsername, [FromUri] string[] statusFilter)
        {

            AppUser appUser;
            try
            {
                var username = User.Identity.Name;

                var user = _unitOfWork.AppUsers.Find(u => u.UserName == username).FirstOrDefault();
                if (user == null)
                {
                    return BadRequest("Data could not be retrieved, try to relog.");
                }
                appUser = user;
            }
            catch
            {
                return BadRequest("Data could not be retrieved, try to relog.");
            }

            List<Reservation> reservations = new List<Reservation>();
            try
            {
                reservations = _unitOfWork.Reservations.GetAllHostReservationsSort( appUser.Id, sortType, selectedUsername, statusFilter).ToList();
            }
            catch
            {
                return BadRequest("There are no reservations");
            }
            if (reservations == null || reservations.Count<0)
            {
                return BadRequest("There are no reservations");
            }


            int count = reservations.Count();

            reservations = reservations.Skip((pageIndex - 1) * pageSize).Take(pageSize).ToList();

            int TotalCount = count;

            // Calculating Totalpage by Dividing (No of Records / Pagesize)  
            int TotalPages = (int)Math.Ceiling(count / (double)pageSize);


            // Object which we are going to send in header   
            var paginationMetadata = new
            {
                totalCount = TotalCount,
                pageSize,
                currentPage = pageIndex,
                totalPages = TotalPages
            };

            // Setting Header  
            HttpContext.Current.Response.Headers.Add("Access-Control-Expose-Headers", "Paging-Headers");
            HttpContext.Current.Response.Headers.Add("Paging-Headers", JsonConvert.SerializeObject(paginationMetadata));
            // Returing List of Customers Collections  



            return Ok(reservations);
        }
        [Authorize(Roles = "Admin")]
        [HttpGet]
        [Route("getAllAdminReservations/{pageIndex}/{pageSize}/{sortType}/{selectedUsername}")]
        public IHttpActionResult GetAllAdminReservations(int pageIndex, int pageSize, string sortType, string selectedUsername, [FromUri] string[] statusFilter)
        {

            List<Reservation> reservations = new List<Reservation>();
            try
            {
                reservations = _unitOfWork.Reservations.GetAllAdminReservationsSort(sortType, selectedUsername, statusFilter).ToList();
            }
            catch
            {
                return BadRequest("There are no reservations");
            }
            if (reservations == null || reservations.Count < 0)
            {
                return BadRequest("There are no reservations");
            }


            int count = reservations.Count();

            reservations = reservations.Skip((pageIndex - 1) * pageSize).Take(pageSize).ToList();

            int TotalCount = count;

            // Calculating Totalpage by Dividing (No of Records / Pagesize)  
            int TotalPages = (int)Math.Ceiling(count / (double)pageSize);


            // Object which we are going to send in header   
            var paginationMetadata = new
            {
                totalCount = TotalCount,
                pageSize,
                currentPage = pageIndex,
                totalPages = TotalPages
            };

            // Setting Header  
            HttpContext.Current.Response.Headers.Add("Access-Control-Expose-Headers", "Paging-Headers");
            HttpContext.Current.Response.Headers.Add("Paging-Headers", JsonConvert.SerializeObject(paginationMetadata));
            // Returing List of Customers Collections  

            return Ok(reservations);
        }
        [Authorize(Roles = "Host")]
        [HttpGet]
        [Route("getHostReservation/{reservationId}")]
        public IHttpActionResult getHostReservation(int reservationId)
        {

            AppUser appUser;
            try
            {
                var username = User.Identity.Name;

                var user = _unitOfWork.AppUsers.Find(u => u.UserName == username).FirstOrDefault();
                if (user == null)
                {
                    return BadRequest("Data could not be retrieved, try to relog.");
                }
                appUser = user;
            }
            catch
            {
                return BadRequest("Data could not be retrieved, try to relog.");
            }

            Reservation reservation;
            try
            {
                reservation = _unitOfWork.Reservations.GetHostReservation(appUser.Id, reservationId);
            }
            catch
            {
                return BadRequest("Reservation does not exist");
            }
            if (reservation == null)
            {
                return BadRequest("Reservation does not exist");
            }

            return Ok(reservation);
        }
        [Authorize(Roles = "Admin")]
        [HttpGet]
        [Route("getAdminReservation/{reservationId}")]
        public IHttpActionResult GetAdminReservation(int reservationId)
        {
            Reservation reservation;
            try
            {
                reservation = _unitOfWork.Reservations.GetAdminReservation(reservationId);
            }
            catch
            {
                return BadRequest("Reservation does not exist");
            }
            if (reservation == null)
            {
                return BadRequest("Reservation does not exist");
            }

            return Ok(reservation);
        }
        [Authorize(Roles = "Host")]
        [HttpGet]
        [Route("getAcceptRejectReservation/{reservationId}/{accept}")]
        public IHttpActionResult getAcceptRejectReservation(int reservationId,bool accept)
        {

            AppUser appUser;
            try
            {
                var username = User.Identity.Name;

                var user = _unitOfWork.AppUsers.Find(u => u.UserName == username).FirstOrDefault();
                if (user == null)
                {
                    return BadRequest("Data could not be retrieved, try to relog.");
                }
                appUser = user;
            }
            catch
            {
                return BadRequest("Data could not be retrieved, try to relog.");
            }

            Reservation reservation;
            try
            {
                reservation = _unitOfWork.Reservations.GetHostReservation(appUser.Id, reservationId);
            }
            catch
            {
                return BadRequest("Reservation does not exist");
            }
            if (reservation == null)
            {
                return BadRequest("Reservation does not exist");
            }

            if (reservation.Status.Description == "Created")
            {
                Status status;
                if (accept)
                {
                    try
                    {
                        status = _unitOfWork.Statuses.GetStatusByDescription("Accepted");
                    }
                    catch
                    {
                        return BadRequest("Status does not exist");
                    }
                }
                else
                {
                    try
                    {
                        status = _unitOfWork.Statuses.GetStatusByDescription("Rejected");
                    }
                    catch
                    {
                        return BadRequest("Status does not exist");
                    }
                }
                reservation.Status = status;
            }
            try
            {

                _unitOfWork.Reservations.Update(reservation);
                _unitOfWork.Complete();

            }
            catch
            {
                if (accept)
                {
                    return BadRequest("Reservation could not be accepted.");
                }
                else
                {
                    return BadRequest("Reservation could not be rejected.");
                }
            }
            return Ok(reservation);
        }

        
            [Authorize(Roles = "Host")]
        [HttpGet]
        [Route("getCompleteReservation/{reservationId}")]
        public IHttpActionResult getCompleteReservation(int reservationId)
        {

            AppUser appUser;
            try
            {
                var username = User.Identity.Name;

                var user = _unitOfWork.AppUsers.Find(u => u.UserName == username).FirstOrDefault();
                if (user == null)
                {
                    return BadRequest("Data could not be retrieved, try to relog.");
                }
                appUser = user;
            }
            catch
            {
                return BadRequest("Data could not be retrieved, try to relog.");
            }

            Reservation reservation;
            try
            {
                reservation = _unitOfWork.Reservations.GetHostReservation(appUser.Id, reservationId);
            }
            catch
            {
                return BadRequest("Reservation does not exist");
            }
            if (reservation == null)
            {
                return BadRequest("Reservation does not exist");
            }

            DateTime today = DateTime.Today;
            DateTime reservationEnd = reservation.ReservationDate.AddDays(reservation.NumOfNights);
            if (reservation.Status.Description == "Accepted" && reservationEnd< today)
            {
                Status status;

                    try
                    {
                        status = _unitOfWork.Statuses.GetStatusByDescription("Completed");
                    }
                    catch
                    {
                        return BadRequest("Status does not exist");
                    }
                    reservation.Status = status;

            }
            try
            {

                _unitOfWork.Reservations.Update(reservation);
                _unitOfWork.Complete();

            }
            catch
            {
                    return BadRequest("Reservation could not be completed.");
            }
            return Ok(reservation);
        }
    }
}
