﻿using Airbnb.Models.Entities;
using Airbnb.Persistence.UnitOfWork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;

namespace Airbnb.Controllers
{
    [RoutePrefix("api/comment")]
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class CommentController : ApiController
    {
        private readonly IUnitOfWork _unitOfWork;

        public CommentController(IUnitOfWork unitOfWork)
        {
            this._unitOfWork = unitOfWork;
        }

        [HttpGet]
        [Route("getApartmentComments/{apartmentId}")]
        public IHttpActionResult GetApartmentComments(int apartmentId)
        {
            List<Comment> comments;
            try
            {

                comments = _unitOfWork.Comments.GetApartmenComments(apartmentId).ToList();
                if (comments == null)
                {
                    return BadRequest("There are no comments for this apartment");
                }
            }
            catch
            {
                return BadRequest("Can't get comments right now");
            }
            return Ok(comments);
        }
        [Authorize(Roles = "Admin, Host")]
        [HttpGet]
        [Route("getAllApartmentComments/{apartmentId}")]
        public IHttpActionResult GetAllApartmentComments(int apartmentId)
        {
            List<Comment> comments;
            try
            {

                comments = _unitOfWork.Comments.GetAllApartmenComments(apartmentId).ToList();
                if (comments == null)
                {
                    return BadRequest("There are no comments for this apartment");
                }
            }
            catch
            {
                return BadRequest("Can't get comments right now");
            }
            return Ok(comments);
        }
        [Authorize(Roles = "Host")]
        [HttpGet]
        [Route("approveComment/{commentId}")]
        public IHttpActionResult ApproveComment(int commentId)
        {
            Comment comment;
            try
            {

                comment = _unitOfWork.Comments.Get(commentId);
                if (comment == null)
                {
                    return BadRequest("Comment could not be found");
                }
            }
            catch
            {
                return BadRequest("Comment could not be found");
            }
            comment.Approved = !comment.Approved;
            try
            {
                _unitOfWork.Comments.Update(comment);
                _unitOfWork.Complete();
            }
            catch
            {
                if (comment.Approved)
                {
                    return BadRequest("Comment could not be enabled");
                }
                else
                {
                    return BadRequest("Comment could not be disabled");
                }
            }
            return Ok(comment);
        }
        [Authorize(Roles = "Guest")]
        [HttpPost]
        [Route("postComment/{reservationId}")]
        public IHttpActionResult PostComment([FromUri(Name = "reservationId")] int reservationId, [FromBody()] Comment comment)
        {
            AppUser appUser;
            try
            {
                var username = User.Identity.Name;

                var user = _unitOfWork.AppUsers.Find(u => u.UserName == username).FirstOrDefault();
                if (user == null)
                {
                    return BadRequest("Data could not be retrieved, try to relog.");
                }
                appUser = user;
            }
            catch
            {
                return BadRequest("Data could not be retrieved, try to relog.");
            }
            Reservation reservation;
            try
            {
                reservation = _unitOfWork.Reservations.GetUserReservation(appUser.Id, reservationId);
            }
            catch
            {
                return BadRequest("Reservation does not exist");
            }
            if (reservation == null)
            {
                return BadRequest("Reservation does not exist");
            }

            if (reservation.ApartmentId != comment.ApartmentId)
            {
                return BadRequest("Reservation and apartment dont match");
            }

            if (reservation.Status.Description != "Rejected" && reservation.Status.Description != "Completed")//
            {
                return BadRequest("Reservation is not rejected or canceled");
            }
            if (reservation.Commented == true)
            {
                return BadRequest("Reservation was already commented");
            }

            reservation.Commented = true;
            try
            {
                _unitOfWork.Reservations.Update(reservation);
                _unitOfWork.Complete();
            }
            catch
            {
                return BadRequest("Comment could not be posted");
            }

            comment.AppUserId = appUser.Id;
            comment.IsDeleted = false;
            comment.PostedDate = DateTime.Now;
            comment.Approved = false;

            try
            {
                _unitOfWork.Comments.Add(comment);
                _unitOfWork.Complete();
            }
            catch
            {
                return BadRequest("Comment could not be posted");
            }


            return Created("Comment was posted", reservation);
        }
    }

}
