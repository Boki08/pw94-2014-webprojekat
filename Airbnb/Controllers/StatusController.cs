﻿using Airbnb.Persistence.UnitOfWork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;

namespace Airbnb.Controllers
{

    [RoutePrefix("api/status")]
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class StatusController : ApiController
    {
        private readonly IUnitOfWork _unitOfWork;

        public StatusController(IUnitOfWork unitOfWork)
        {
            this._unitOfWork = unitOfWork;
        }

        [HttpGet]
        [Route("getReservationStatuses/")]
        public IHttpActionResult getReservationStatuses()
        {

            var items = _unitOfWork.Statuses.GetAll().ToList();

            if (items == null || items.Count < 1)
            {
                return BadRequest("There are no Reservation statuses");
            }

            return Ok(items);
        }
    }
}
