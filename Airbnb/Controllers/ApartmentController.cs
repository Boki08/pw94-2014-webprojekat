﻿using Airbnb.Models.Entities;
using Airbnb.Persistence;
using Airbnb.Persistence.UnitOfWork;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Core.Objects;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Validation;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web;
using System.Web.Http;
using System.Web.Http.Cors;

namespace Airbnb.Controllers
{
    [RoutePrefix("api/apartment")]
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class ApartmentController : ApiController
    {
        private readonly IUnitOfWork _unitOfWork;

        public ApartmentController(IUnitOfWork unitOfWork)
        {
            this._unitOfWork = unitOfWork;
        }

        //[HttpGet]
        //[Route("getAllApartments/{pageIndex}/{pageSize}/{priceSort}")]
        //public IHttpActionResult getAllApartments(int pageIndex, int pageSize, string priceSort)
        //{

        //    var items = _unitOfWork.Apartments.GetAllApartments(priceSort).ToList();

        //    if (items == null || items.Count < 1)
        //    {
        //        return BadRequest("There are no Apartments");
        //    }

        //    int count = items.Count();



        //    int TotalCount = count;

        //    // Calculating Totalpage by Dividing (No of Records / Pagesize)  
        //    int TotalPages = (int)Math.Ceiling(count / (double)pageSize);


        //    // Object which we are going to send in header   
        //    var paginationMetadata = new
        //    {
        //        totalCount = TotalCount,
        //        pageSize,
        //        currentPage = pageIndex,
        //        totalPages = TotalPages
        //    };

        //    // Setting Header  
        //    HttpContext.Current.Response.Headers.Add("Access-Control-Expose-Headers", "Paging-Headers");
        //    HttpContext.Current.Response.Headers.Add("Paging-Headers", JsonConvert.SerializeObject(paginationMetadata));
        //    // Returing List of Customers Collections  
        //    return Ok(items);
        //}

        [HttpGet]
        [Route("getApartment/{apartmentId}")]
        public IHttpActionResult getApartment(int apartmentId)
        {

            Apartment apartment;
            try
            {
                apartment = _unitOfWork.Apartments.GetApartmentLocAmeHostPicType(apartmentId);
            }
            catch
            {
                return BadRequest("Apartment does not exist");
            }
            if (apartment == null)
            {
                return BadRequest("Apartment does not exist");
            }

            return Ok(apartment);
        }

        [HttpGet]
        [Route("getAllApartmentsSearch/{pageIndex}/{pageSize}/{searchChecked}/{city}/{priceSort}/{minPrice}/{maxPrice}/{numOfGuests}/{numOfRooms}")]
        public IHttpActionResult GetAllApartmentsSearch(int pageIndex, int pageSize,bool searchChecked, string city, string priceSort, int minPrice, int maxPrice, int numOfGuests, int numOfRooms, [FromUri(Name = "fromDate")] DateTime fromDate, [FromUri(Name = "toDate")] DateTime toDate, [FromUri(Name = "amenityFilter")] int[] amenityFilter, [FromUri(Name = "typeFilter")] int[] typeFilter)
        {
            var items = _unitOfWork.Apartments.GetAllApartmentsSearch(amenityFilter, typeFilter, searchChecked, city, priceSort, fromDate, toDate, minPrice, maxPrice, numOfGuests, numOfRooms).ToList();

            if (items == null || items.Count < 1)
            {
                return BadRequest("There are no Apartments");
            }

            int count = items.Count();

            items = items.Skip((pageIndex - 1) * pageSize).Take(pageSize).ToList();

            int TotalCount = count;

            // Calculating Totalpage by Dividing (No of Records / Pagesize)  
            int TotalPages = (int)Math.Ceiling(count / (double)pageSize);


            // Object which we are going to send in header   
            var paginationMetadata = new
            {
                totalCount = TotalCount,
                pageSize,
                currentPage = pageIndex,
                totalPages = TotalPages
            };

            // Setting Header  
            HttpContext.Current.Response.Headers.Add("Access-Control-Expose-Headers", "Paging-Headers");
            HttpContext.Current.Response.Headers.Add("Paging-Headers", JsonConvert.SerializeObject(paginationMetadata));
            // Returing List of Customers Collections  
            return Ok(items);
        }

        [Authorize(Roles = "Host")]
        [HttpGet]////main
        [Route("getAllApartmentsHostSearch/{pageIndex}/{pageSize}/{searchChecked}/{city}/{priceSort}/{statusFilter}/{minPrice}/{maxPrice}/{numOfGuests}/{numOfRooms}")]
        public IHttpActionResult GetAllApartmentsHostSearch(int pageIndex, int pageSize,bool searchChecked, string city, string priceSort,string statusFilter, int minPrice, int maxPrice, int numOfGuests, int numOfRooms, [FromUri(Name = "fromDate")] DateTime fromDate, [FromUri(Name = "toDate")] DateTime toDate, [FromUri(Name = "amenityFilter")] int[] amenityFilter, [FromUri(Name = "typeFilter")] int[] typeFilter)
        {
            int hostId;
            try
            {
                var username = User.Identity.Name;

                var user = _unitOfWork.AppUsers.Find(u => u.UserName == username).FirstOrDefault();
                if (user == null)
                {
                    return BadRequest("Data could not be retrieved, try to relog.");
                }
                hostId = user.Id;
            }
            catch
            {
                return BadRequest("Data could not be retrieved, try to relog.");
            }

            var items = _unitOfWork.Apartments.GetAllApartmentsHostSearch( hostId, statusFilter, amenityFilter, typeFilter, searchChecked, city, priceSort, fromDate, toDate, minPrice, maxPrice, numOfGuests, numOfRooms).ToList();

            if (items == null || items.Count < 1)
            {
                return BadRequest("There are no Apartments");
            }

            int count = items.Count();

            items = items.Skip((pageIndex - 1) * pageSize).Take(pageSize).ToList();

            int TotalCount = count;

            // Calculating Totalpage by Dividing (No of Records / Pagesize)  
            int TotalPages = (int)Math.Ceiling(count / (double)pageSize);


            // Object which we are going to send in header   
            var paginationMetadata = new
            {
                totalCount = TotalCount,
                pageSize,
                currentPage = pageIndex,
                totalPages = TotalPages
            };

            // Setting Header  
            HttpContext.Current.Response.Headers.Add("Access-Control-Expose-Headers", "Paging-Headers");
            HttpContext.Current.Response.Headers.Add("Paging-Headers", JsonConvert.SerializeObject(paginationMetadata));
            // Returing List of Customers Collections  
            return Ok(items);
        }
        [Authorize(Roles = "Admin")]
        [HttpGet]
        [Route("getAllApartmentsAdminSearch/{pageIndex}/{pageSize}/{searchChecked}/{city}/{priceSort}/{statusFilter}/{minPrice}/{maxPrice}/{numOfGuests}/{numOfRooms}")]
        public IHttpActionResult GetAllApartmentsAdminSearch(int pageIndex, int pageSize, bool searchChecked,  string city, string priceSort, string statusFilter, int minPrice, int maxPrice, int numOfGuests, int numOfRooms, [FromUri(Name = "fromDate")] DateTime fromDate, [FromUri(Name = "toDate")] DateTime toDate, [FromUri(Name = "amenityFilter")] int[] amenityFilter, [FromUri(Name = "typeFilter")] int[] typeFilter)
        {
            var items = _unitOfWork.Apartments.GetAllApartmentsAdminSearch(statusFilter,  amenityFilter, typeFilter, searchChecked, city, priceSort, fromDate, toDate, minPrice, maxPrice, numOfGuests, numOfRooms).ToList();

            if (items == null || items.Count < 1)
            {
                return BadRequest("There are no Apartments");
            }

            int count = items.Count();

            items = items.Skip((pageIndex - 1) * pageSize).Take(pageSize).ToList();

            int TotalCount = count;

            // Calculating Totalpage by Dividing (No of Records / Pagesize)  
            int TotalPages = (int)Math.Ceiling(count / (double)pageSize);


            // Object which we are going to send in header   
            var paginationMetadata = new
            {
                totalCount = TotalCount,
                pageSize,
                currentPage = pageIndex,
                totalPages = TotalPages
            };

            // Setting Header  
            HttpContext.Current.Response.Headers.Add("Access-Control-Expose-Headers", "Paging-Headers");
            HttpContext.Current.Response.Headers.Add("Paging-Headers", JsonConvert.SerializeObject(paginationMetadata));
            // Returing List of Customers Collections  
            return Ok(items);
        }


        [Authorize(Roles = "Host")]
        [HttpGet]
        [Route("getApartmentsHost/{pageIndex}/{pageSize}")]
        public IHttpActionResult GetApartmentsHost(int pageIndex, int pageSize)
        {
            int hostId;
            try
            {
                var username = User.Identity.Name;

                var user = _unitOfWork.AppUsers.Find(u => u.UserName == username).FirstOrDefault();
                if (user == null)
                {
                    return BadRequest("Data could not be retrieved, try to relog.");
                }
                hostId = user.Id;
            }
            catch
            {
                return BadRequest("Data could not be retrieved, try to relog.");
            }

            var items = _unitOfWork.Apartments.GetApartmentsHost( hostId).ToList();

            if (items == null || items.Count < 1)
            {
                return BadRequest("There are no Apartments");
            }

            int count = items.Count();

            items = items.Skip((pageIndex - 1) * pageSize).Take(pageSize).ToList();

            int TotalCount = count;

            // Calculating Totalpage by Dividing (No of Records / Pagesize)  
            int TotalPages = (int)Math.Ceiling(count / (double)pageSize);


            // Object which we are going to send in header   
            var paginationMetadata = new
            {
                totalCount = TotalCount,
                pageSize,
                currentPage = pageIndex,
                totalPages = TotalPages
            };

            // Setting Header  
            HttpContext.Current.Response.Headers.Add("Access-Control-Expose-Headers", "Paging-Headers");
            HttpContext.Current.Response.Headers.Add("Paging-Headers", JsonConvert.SerializeObject(paginationMetadata));
            // Returing List of Customers Collections  
            return Ok(items);
        }

        [Authorize(Roles = "Admin")]
        [HttpGet]
        [Route("getApartmentsAdmin/{pageIndex}/{pageSize}")]
        public IHttpActionResult GetApartmentsAdmin(int pageIndex, int pageSize)
        {

            var items = _unitOfWork.Apartments.GetAll().ToList();

            if (items == null || items.Count < 1)
            {
                return BadRequest("There are no Apartments");
            }

            int count = items.Count();

            items = items.Skip((pageIndex - 1) * pageSize).Take(pageSize).ToList();

            int TotalCount = count;

            // Calculating Totalpage by Dividing (No of Records / Pagesize)  
            int TotalPages = (int)Math.Ceiling(count / (double)pageSize);


            // Object which we are going to send in header   
            var paginationMetadata = new
            {
                totalCount = TotalCount,
                pageSize,
                currentPage = pageIndex,
                totalPages = TotalPages
            };

            // Setting Header  
            HttpContext.Current.Response.Headers.Add("Access-Control-Expose-Headers", "Paging-Headers");
            HttpContext.Current.Response.Headers.Add("Paging-Headers", JsonConvert.SerializeObject(paginationMetadata));
            // Returing List of Customers Collections  
            return Ok(items);
        }


        [Authorize(Roles = "Host")]
        [HttpGet]
        [Route("getApartmentInfoHost/{apartmentId}")]
        public IHttpActionResult getApartmentInfoHost(int apartmentId)
        {
            int hostId;
            try
            {
                var username = User.Identity.Name;

                var user = _unitOfWork.AppUsers.Find(u => u.UserName == username).FirstOrDefault();
                if (user == null)
                {
                    return BadRequest("Data could not be retrieved, try to relog.");
                }
                hostId = user.Id;
            }
            catch
            {
                return BadRequest("Data could not be retrieved, try to relog.");
            }

            Apartment apartment;
            try
            {
                apartment = _unitOfWork.Apartments.GetApartmentLocAmeHostPicTypeHost(apartmentId, hostId);
            }
            catch
            {
                return BadRequest("Apartment does not exist");
            }
            if (apartment == null)
            {
                return BadRequest("Apartment does not exist");
            }

            return Ok(apartment);
        }
        [Authorize(Roles = "Admin")]
        [HttpGet]
        [Route("getApartmentInfoAdmin/{apartmentId}")]
        public IHttpActionResult GetApartmentInfoAdmin(int apartmentId)
        {


            Apartment apartment;
            try
            {
                apartment = _unitOfWork.Apartments.GetApartmentLocAmeHostPicTypeAdmin(apartmentId);
            }
            catch
            {
                return BadRequest("Apartment does not exist");
            }
            if (apartment == null)
            {
                return BadRequest("Apartment does not exist");
            }

            return Ok(apartment);
        }

        [Authorize(Roles = "Guest")]
        [HttpGet]
        [Route("getDisabledDates/{apartmentId}")]
        public IHttpActionResult getDisabledDates(int apartmentId)
        {
            Apartment apartment;
            try
            {
                apartment = _unitOfWork.Apartments.GetEnabledApartmentWithReservations(apartmentId);
            }
            catch
            {
                return BadRequest("Apartment does not exist");
            }
            if (apartment == null)
            {
                return BadRequest("Apartment does not exist");
            }

            List<Reservation> reservations = apartment.Reservations.Where(x=>x.ReservationDate.AddDays(x.NumOfNights)>DateTime.Now.Date).ToList();
            List<DateTime> disabledDates = new List<DateTime>();

            foreach (Reservation r in reservations)
            {
                for(int day=0;day<=r.NumOfNights;day++)
                {
                    disabledDates.Add(new DateTime(year: r.ReservationDate.Year, month:r.ReservationDate.Month,day: r.ReservationDate.AddDays(day).Day));
                }
            }

            return Ok(disabledDates);
        }

        [HttpGet]
        [Route("getApartmentPictures/{apartmentId}")]
        public IHttpActionResult getApartmentPictures(int apartmentId)
        {

            var photos = _unitOfWork.ApartmentPictures.Find(x => x.ApartmentId == apartmentId).ToList();

            if (photos.Count == 0)
            {
                photos.Add(new ApartmentPicture() { Data = "default-placeholder.png", ApartmentId = apartmentId });
            }

            return Ok(photos);
        }
        [HttpGet]
        [Route("getApartmentPicture/{apartmentId}")]
        public IHttpActionResult getApartmentPicture(int apartmentId)
        {

            ApartmentPicture photo = _unitOfWork.ApartmentPictures.GetApartmentPicture(apartmentId);

            if (photo == null)
            {
                photo = new ApartmentPicture() { Data = "default-placeholder.png", ApartmentId = apartmentId };
            }

            return Ok(photo);
        }


        [Authorize(Roles = "Host")]
        [HttpPost]
        [Route("addApartment")]
        public IHttpActionResult AddApartment()
        {
            int appUserId;
            try
            {
                var username = User.Identity.Name;

                var user = _unitOfWork.AppUsers.Find(u => u.UserName == username).FirstOrDefault();
                if (user == null)
                {
                    return BadRequest("Data could not be retrieved, try to relog.");
                }
                appUserId = user.Id;

            }
            catch
            {
                return BadRequest("Data could not be retrieved, try to relog.");
            }
            var httpRequest = HttpContext.Current.Request;

            string imageName = null;


            int picturesNum = Int32.Parse(httpRequest["PicturesNum"]);
            int amenitiesNum = Int32.Parse(httpRequest["AmenitiesNum"]);


            Apartment apartment = new Apartment();
            apartment.AppUserId = appUserId;
            apartment.ApartmentTypeId = Int32.Parse(httpRequest["ApartmentTypeId"].Trim());
            apartment.CheckInTime = DateTime.Parse(httpRequest["CheckInTime"].Trim());
            apartment.CheckOutTime = DateTime.Parse(httpRequest["CheckOutTime"].Trim());
            apartment.NumOfGuests = Int32.Parse(httpRequest["NumOfGuests"]);
            apartment.NumOfRooms = Int32.Parse(httpRequest["NumOfRooms"]);
            apartment.Grade = 0;
            apartment.IsDeleted = false;
            apartment.Status = false;
            apartment.RentingDates = new List<RentingDate>() { new RentingDate() { IsDeleted = false, StartDate = DateTime.Parse(httpRequest["fromDate"].Trim()).Date, EndDate = DateTime.Parse(httpRequest["toDate"].Trim()).Date } };
            apartment.FreeDates = new List<FreeDate>() { new FreeDate() { IsDeleted = false, StartDate = DateTime.Parse(httpRequest["fromDate"].Trim()).Date, EndDate = DateTime.Parse(httpRequest["toDate"].Trim()).Date } };
            apartment.Location = new List<Location> { new Location() { Longitude = double.Parse(httpRequest["Longitude"], CultureInfo.InvariantCulture), Latitude = double.Parse(httpRequest["Latitude"], CultureInfo.InvariantCulture), Address = new List<Address>() { new Address() { StreetAndNumber = httpRequest["AddressNum"].Trim(), PostalCode = double.Parse(httpRequest["PostalCode"].Trim()), City = httpRequest["City"].Trim(), IsDeleted = false } }, IsDeleted = false } };

            apartment.Price = double.Parse(httpRequest["CostPerNight"]);
            apartment.IsDeleted = false;
            apartment.Amenities = new List<Amenity>();
            for (int i = 0; i < amenitiesNum; i++)
            {
                Amenity amenity = _unitOfWork.Amenities.Get(Int32.Parse(httpRequest[String.Format("Amenity{0}", i)]));
                //apartment.Amenities.Add(new Amenity() { Id=Int32.Parse(httpRequest[String.Format("Amenity{0}", i)]) });
                apartment.Amenities.Add(amenity);
            }



           
            try
            {
                _unitOfWork.Apartments.Add(apartment);
                _unitOfWork.Complete();
            }
            catch
            {
                return BadRequest("Apartment could not be added");
            }


            if (picturesNum < 1)
            {

                _unitOfWork.ApartmentPictures.Add(new ApartmentPicture() { Data = "default-placeholder.png", ApartmentId = apartment.Id });
                _unitOfWork.Complete();
            }
            else
            {
                for (int i = 0; i < picturesNum; i++)
                {
                    var postedFile = httpRequest.Files[String.Format("Image{0}", i)];
                    imageName = new string(Path.GetFileNameWithoutExtension(postedFile.FileName).Take(10).ToArray()).Replace(" ", "-");
                    imageName = imageName + DateTime.Now.ToString("yymmssfff") + Path.GetExtension(postedFile.FileName);
                    var filePath = HttpContext.Current.Server.MapPath("~/Images/" + imageName);
                    postedFile.SaveAs(filePath);
                    _unitOfWork.ApartmentPictures.Add(new ApartmentPicture() { Data = imageName, ApartmentId = apartment.Id });
                    _unitOfWork.Complete();
                }
            }

            return Ok("Apartment was created");
        }

        [Authorize(Roles = "Admin")]
        [HttpGet]
        [Route("changeApartmentStatus/{apartmentId}/{apartmentStatus}")]
        public IHttpActionResult changeApartmentStatus(int apartmentId, bool apartmentStatus)
        {

            Apartment apartment;
            try
            {
                apartment = _unitOfWork.Apartments.Get(apartmentId);
            }
            catch
            {
                return BadRequest("Apartment could not be found");
            }
            if (apartment == null)
            {
                return BadRequest("Apartment could not be found");
            }

            try
            {
                apartment.Status = apartmentStatus;
                _unitOfWork.Apartments.Update(apartment);
                _unitOfWork.Complete();
            }
            catch
            {
                return BadRequest("Apartment status could not be changed");
            }

            return Ok("Apartment status was changed");
        }

        [Authorize(Roles = "Host,Admin")]
        [HttpGet]
        [Route("deleteApartment/{apartmentId}/{hostId}")]
        public IHttpActionResult DeleteApartment(int apartmentId, int hostId )
        {
            int appUserId;
            try
            {
                var username = User.Identity.Name;

                var user = _unitOfWork.AppUsers.Find(u => u.UserName == username).FirstOrDefault();
                if (user == null)
                {
                    return BadRequest("Data could not be retrieved, try to relog.");
                }
                appUserId = user.Id;

            }
            catch
            {
                return BadRequest("Data could not be retrieved, try to relog.");
            }
            AirbnbDBContext db = new AirbnbDBContext();
            var userR = db.Users.Include(x => x.AppUser).SingleOrDefault(m => m.AppUserId == appUserId);
            var role = db.Roles.Where(m => m.Users.Any(n => n.UserId == userR.Id)).FirstOrDefault().Name;

            if (role == "Admin")
            {
                appUserId = hostId;
            }

            Apartment apartment;
            try
            {
                apartment = _unitOfWork.Apartments.Find(x=>x.Id==apartmentId && x.AppUserId== appUserId).FirstOrDefault();
            }
            catch
            {
                return BadRequest("Apartment could not be found");
            }
            if (apartment == null)
            {
                return BadRequest("Apartment could not be found");
            }

            try
            {
                using (var context = new AirbnbDBContext())
                {

                    var apartment1 = context.Apartments.Include(s => s.Amenities).Where(s => s.Id == apartmentId).FirstOrDefault();

                    if (apartment1.Amenities != null)
                    {

                        var amenitiesToRemove = apartment1.Amenities.ToList();


                        foreach (var amenity in amenitiesToRemove)
                        {
                            apartment1.Amenities.Remove(amenity);
                        }
                        context.SaveChanges();
                    }
                    IEnumerable<FreeDate> freeDates = _unitOfWork.FreeDates.Find(x => x.ApartmentId == apartmentId);
                    _unitOfWork.FreeDates.RemoveRange(freeDates);
                    _unitOfWork.Complete();
                    IEnumerable<Comment> comments = _unitOfWork.Comments.Find(x => x.ApartmentId == apartmentId);
                    _unitOfWork.Comments.RemoveRange(comments);
                    _unitOfWork.Complete();

                    IEnumerable<Location> location = _unitOfWork.Locations.Find(x => x.ApartmentId == apartmentId);
                    List<Location> locations = location.ToList();
                    int locationId = locations.FirstOrDefault().Id;
                    IEnumerable<Address> address = _unitOfWork.Addresses.Find(x => x.LocationId == locationId);
                    _unitOfWork.Addresses.RemoveRange(address);
                    _unitOfWork.Complete();
                    _unitOfWork.Locations.RemoveRange(location);
                    _unitOfWork.Complete();
                    IEnumerable<ApartmentPicture> pictures = _unitOfWork.ApartmentPictures.Find(x => x.ApartmentId == apartmentId);
                    _unitOfWork.ApartmentPictures.RemoveRange(pictures);
                    _unitOfWork.Complete();
                    IEnumerable<RentingDate> rentingDates = _unitOfWork.RentingDates.Find(x => x.ApartmentId == apartmentId);
                    _unitOfWork.RentingDates.RemoveRange(rentingDates);
                    _unitOfWork.Complete();
                    IEnumerable<Reservation> reservations = _unitOfWork.Reservations.Find(x => x.ApartmentId == apartmentId);
                    _unitOfWork.Reservations.RemoveRange(reservations);
                    _unitOfWork.Complete();
                    
                }
                _unitOfWork.Apartments.Remove(apartment);
                _unitOfWork.Complete();
            }
            catch
            {
                return BadRequest("Apartment could not be deleted");
            }
            //catch (DbEntityValidationException e)
            //{
            //    foreach (var eve in e.EntityValidationErrors)
            //    {
            //        Console.WriteLine("Entity of type \"{0}\" in the state \"{1}\" has the following validation errors:",
            //            eve.Entry.Entity.GetType().Name, eve.Entry.State);
            //        foreach (var ve in eve.ValidationErrors)
            //        {
            //            Console.WriteLine("- Property: \"{0}\", Error: \"{1}\"",
            //                ve.PropertyName, ve.ErrorMessage);
            //        }

            //        foreach (var ve in eve.ValidationErrors)
            //        {
            //            Console.WriteLine("- Property: \"{0}\", Value: \"{1}\", Error: \"{2}\"",
            //                ve.PropertyName,
            //                eve.Entry.CurrentValues.GetValue<object>(ve.PropertyName),
            //                ve.ErrorMessage);
            //        }
            //    }
            //}

            return Ok("Apartment deleted");

        }

        [Authorize(Roles = "Host,Admin")]
        [HttpPost]
        [Route("editApartment")]
        public IHttpActionResult EditApartment()
        {

            int appUserId;
            try
            {
                var username = User.Identity.Name;

                var user = _unitOfWork.AppUsers.Find(u => u.UserName == username).FirstOrDefault();
                if (user == null)
                {
                    return BadRequest("Data could not be retrieved, try to relog.");
                }
                appUserId = user.Id;

            }
            catch
            {
                return BadRequest("Data could not be retrieved, try to relog.");
            }
            AirbnbDBContext db = new AirbnbDBContext();
            var userR = db.Users.Include(x => x.AppUser).SingleOrDefault(m => m.AppUserId == appUserId);
            var role = db.Roles.Where(m => m.Users.Any(n => n.UserId == userR.Id)).FirstOrDefault().Name;

            var httpRequest = HttpContext.Current.Request;
            if (role == "Admin")
            {
                appUserId = Int32.Parse(httpRequest["AppUserId"]);
            }

            string imageName = null;


            int picturesNum = Int32.Parse(httpRequest["PicturesNum"]);
            int amenitiesNum = Int32.Parse(httpRequest["AmenitiesNum"]);

            int apartmentId = Int32.Parse(httpRequest["ApartmentId"].Trim());

            Apartment apartment = _unitOfWork.Apartments.GetApartmentForEdit(apartmentId, appUserId);

            apartment.AppUserId = appUserId;
            apartment.ApartmentTypeId = Int32.Parse(httpRequest["ApartmentTypeId"].Trim());
            apartment.CheckInTime = DateTime.Parse(httpRequest["CheckInTime"].Trim());
            apartment.CheckOutTime = DateTime.Parse(httpRequest["CheckOutTime"].Trim());
            apartment.NumOfGuests = Int32.Parse(httpRequest["NumOfGuests"]);
            apartment.NumOfRooms = Int32.Parse(httpRequest["NumOfRooms"]);

            //apartment.Status = false;
            if (apartment.RentingDates[0].EndDate.Date != DateTime.Parse(httpRequest["toDate"].Trim()).Date || apartment.RentingDates[0].StartDate.Date != DateTime.Parse(httpRequest["fromDate"].Trim()).Date)
            {
                apartment.RentingDates[0].EndDate = DateTime.Parse(httpRequest["toDate"].Trim()).Date;
                apartment.RentingDates[0].StartDate = DateTime.Parse(httpRequest["fromDate"].Trim()).Date;

                IEnumerable<FreeDate> freeDatesE = _unitOfWork.FreeDates.Find(x => x.ApartmentId == apartmentId);
                _unitOfWork.FreeDates.RemoveRange(freeDatesE);
                _unitOfWork.Complete();

                FreeDate freeDate = new FreeDate() { ApartmentId = apartmentId, IsDeleted = false, StartDate = DateTime.Parse(httpRequest["fromDate"].Trim()).Date, EndDate = DateTime.Parse(httpRequest["toDate"].Trim()).Date };
                _unitOfWork.FreeDates.Add(freeDate);
                _unitOfWork.Complete();
                List<FreeDate> freeDates = _unitOfWork.FreeDates.Find(x => x.ApartmentId == apartmentId).ToList();
                List<Reservation> reservations = _unitOfWork.Reservations.ForApartmentWithStatus(apartmentId).ToList();
                FreeDate newEarlyDate, newLaterDate;
                bool changes = false;
                foreach (Reservation r in reservations)
                {
                    if (changes)
                    {
                        freeDates = _unitOfWork.FreeDates.Find(x => x.ApartmentId == apartmentId).ToList();
                        changes = false;
                    }
                    foreach (FreeDate f in freeDates)
                    {

                        if (r.ReservationDate.AddDays(r.NumOfNights) >= f.StartDate && r.ReservationDate <= f.StartDate)
                        {
                            f.StartDate = r.ReservationDate.AddDays(r.NumOfNights + 1);
                        }
                        else if (r.ReservationDate.AddDays(r.NumOfNights) >= f.EndDate && r.ReservationDate <= f.EndDate)
                        {
                            f.EndDate = r.ReservationDate.Subtract(TimeSpan.FromDays(1));
                        }
                        else if (r.ReservationDate.AddDays(r.NumOfNights) <= f.EndDate && r.ReservationDate >= f.StartDate)
                        {
                            newEarlyDate = new FreeDate() { ApartmentId = apartmentId }; newLaterDate = new FreeDate() { ApartmentId = apartmentId };
                            if (f.StartDate < r.ReservationDate)
                            {
                                newEarlyDate.StartDate = f.StartDate;

                                newEarlyDate.EndDate = r.ReservationDate.Subtract(TimeSpan.FromDays(1));


                                _unitOfWork.FreeDates.Add(newEarlyDate);
                                _unitOfWork.Complete();
                            }
                            if (f.EndDate > r.ReservationDate.AddDays(r.NumOfNights))
                            {

                                newLaterDate.StartDate = r.ReservationDate.AddDays(r.NumOfNights).AddDays(1);
                                newLaterDate.EndDate = f.EndDate;


                                _unitOfWork.FreeDates.Add(newLaterDate);
                                _unitOfWork.Complete();
                            }
                            _unitOfWork.FreeDates.Remove(f);
                            _unitOfWork.Complete();
                            changes = true;
                        }
                    }
                }
            }

            //apartment.FreeDates = new List<FreeDate>() { new FreeDate() { IsDeleted = false, StartDate = DateTime.Parse(httpRequest["fromDate"].Trim()), EndDate = DateTime.Parse(httpRequest["toDate"].Trim()) } };

            apartment.Location[0].Latitude = double.Parse(httpRequest["Latitude"], CultureInfo.InvariantCulture);
            apartment.Location[0].Longitude = double.Parse(httpRequest["Longitude"], CultureInfo.InvariantCulture);
            apartment.Location[0].Address[0].City = httpRequest["City"].Trim();
            apartment.Location[0].Address[0].PostalCode = double.Parse(httpRequest["PostalCode"].Trim());
            apartment.Location[0].Address[0].StreetAndNumber = httpRequest["AddressNum"].Trim();

            apartment.Price = double.Parse(httpRequest["CostPerNight"]);

            using (var context = new AirbnbDBContext())
            {
               
                var apartment1 = context.Apartments.Include(s => s.Amenities).Where(s => s.Id == apartmentId).FirstOrDefault();

                if (apartment1.Amenities != null)
                {
                  
                    var amenitiesToRemove = apartment1.Amenities.ToList();

                    
                    foreach (var amenity in amenitiesToRemove)
                    {
                        apartment1.Amenities.Remove(amenity);
                    }
                    context.SaveChanges();
                }
            }

            try
            {
                _unitOfWork.Apartments.Update(apartment);
                _unitOfWork.Complete();
            }
            catch
            {
                return BadRequest("Apartment could not be added");
            }


            for (int i = 0; i < amenitiesNum; i++)
            {
                Amenity amenity = _unitOfWork.Amenities.Get(Int32.Parse(httpRequest[String.Format("Amenity{0}", i)]));
                //apartment.Amenities.Add(new Amenity() { Id=Int32.Parse(httpRequest[String.Format("Amenity{0}", i)]) });
                apartment.Amenities.Add(amenity);
            }


            try
            {
                _unitOfWork.Apartments.Update(apartment);
                _unitOfWork.Complete();
            }
            catch
            {
                return BadRequest("Apartment could not be added");
            }

            //catch (DbEntityValidationException e)
            //{
            //    foreach (var eve in e.EntityValidationErrors)
            //    {
            //        Console.WriteLine("Entity of type \"{0}\" in the state \"{1}\" has the following validation errors:",
            //            eve.Entry.Entity.GetType().Name, eve.Entry.State);
            //        foreach (var ve in eve.ValidationErrors)
            //        {
            //            Console.WriteLine("- Property: \"{0}\", Error: \"{1}\"",
            //                ve.PropertyName, ve.ErrorMessage);
            //        }

            //        foreach (var ve in eve.ValidationErrors)
            //        {
            //            Console.WriteLine("- Property: \"{0}\", Value: \"{1}\", Error: \"{2}\"",
            //                ve.PropertyName,
            //                eve.Entry.CurrentValues.GetValue<object>(ve.PropertyName),
            //                ve.ErrorMessage);
            //        }
            //    }
            //}


            if (picturesNum >0)
            {

                IEnumerable<ApartmentPicture> photos = _unitOfWork.ApartmentPictures.Find(x => x.ApartmentId == apartmentId);
                _unitOfWork.ApartmentPictures.RemoveRange(photos);
                _unitOfWork.Complete();

                for (int i = 0; i < picturesNum; i++)
                {
                    var postedFile = httpRequest.Files[String.Format("Image{0}", i)];
                    imageName = new string(Path.GetFileNameWithoutExtension(postedFile.FileName).Take(10).ToArray()).Replace(" ", "-");
                    imageName = imageName + DateTime.Now.ToString("yymmssfff") + Path.GetExtension(postedFile.FileName);
                    var filePath = HttpContext.Current.Server.MapPath("~/Images/" + imageName);
                    postedFile.SaveAs(filePath);
                    _unitOfWork.ApartmentPictures.Add(new ApartmentPicture() { Data = imageName, ApartmentId = apartment.Id });
                    _unitOfWork.Complete();
                }
            }


            return Ok("Apartment was edited");
        }



        [HttpGet]
        [Route("getApartmentPictureData")]
        public HttpResponseMessage getApartmentPictureData(string path)
        {
            if (path == null)
            {
                path = "default-placeholder.png";
            }

            var filePath = HttpContext.Current.Server.MapPath("~/Images/" + path);
            if (!File.Exists(filePath))
            {
                path = "default-placeholder.png";
                filePath = HttpContext.Current.Server.MapPath("~/Images/" + path);
            }
            var ext = Path.GetExtension(filePath);

            var contents = File.ReadAllBytes(filePath);

            MemoryStream ms = new MemoryStream(contents);

            var response = Request.CreateResponse(HttpStatusCode.OK);
            response.Content = new StreamContent(ms);
            response.Content.Headers.ContentType = new MediaTypeHeaderValue("image/" + ext);

            return response;
        }
    }
}
