import { Injectable } from '@angular/core';

import { Headers, RequestOptions } from '@angular/http';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { Observable } from 'rxjs';



@Injectable({
  providedIn: 'root'
})
export class ApartmentTypeServices {

  constructor(private httpClient: HttpClient) { }

  GetApartmentTypes(): Observable<any> {
    return this.httpClient.get('http://localhost:61619/api/apartmentType/getAllApartmentTypes', { observe: 'response' });
  }


}
