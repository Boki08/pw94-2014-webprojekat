import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Observable } from 'rxjs';
import { appUserData } from '../models/appUserData';
import { registerData } from '../models/registerData';


@Injectable({
    providedIn: 'root'
})
export class UserServices {

    constructor(private httpClient: HttpClient) { }

    getProfile():Observable<any>{
        return this.httpClient.get("http://localhost:61619/api/appUser/getCurrentUser", { observe: 'response' });
      }
    LogOut():Observable<any>{
        return this.httpClient.post("http://localhost:61619/api/Account/Logout",localStorage.jwt);
      }
      
      EditUser(userData:appUserData):Observable<any>{

    
        return this.httpClient.post('http://localhost:61619/api/appUser/editAppUser', userData, {observe: 'response' });
      }

      EditPassword(NewPassword): Observable<any> {
        console.log(NewPassword);
        return this.httpClient.post("http://localhost:61619/api/Account/ChangePassword", NewPassword);
      }

      GetAllUsers(pageIndex:number,pageSize:number,selectedRole:string,selectedGender:string,selectedUsername:string): Observable<any> {
        return this.httpClient.get("http://localhost:61619/api/appUser/allUsers/"+pageIndex+"/"+pageSize+"/"+selectedRole+"/"+selectedGender+"/"+selectedUsername, { observe: 'response' }) ;
        
      }
      ActivateUser(userId:number,activated:boolean): Observable<any> {
        return this.httpClient.get("http://localhost:61619/api/appUser/activateUser/"+userId+"/"+activated, { observe: 'response' }) ;
      }
      getProfileById(userId:number):Observable<any>{
        return this.httpClient.get("http://localhost:61619/api/appUser/getUserById/"+userId, { observe: 'response' });
      }

      registerHost(host:registerData):Observable<any>{
        return this.httpClient.post("http://localhost:61619/api/appUser/registerHost", host);
      }
      register(NewUser): Observable<any> {
        return this.httpClient.post("http://localhost:61619/api/Account/Register", NewUser);
      }
}
