
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Observable } from 'rxjs';
import { appUserData } from '../models/appUserData';


@Injectable({
    providedIn: 'root'
})
export class ReservationServices {

    constructor(private httpClient: HttpClient) { }

    getAllUserReservations(pageIndex:number, pageSize:number,sortType:string):Observable<any>{
        return this.httpClient.get("http://localhost:61619/api/reservation/getAllUserReservations/"+pageIndex+"/"+pageSize+"/"+sortType, { observe: 'response' });
      }

      getUserReservation(reservationId:number):Observable<any>{
        return this.httpClient.get("http://localhost:61619/api/reservation/getUserReservation/"+reservationId, { observe: 'response' });
      }

      rentApartment(apartmentId:number,fromDate: string, toDate: string):Observable<any>{
        return this.httpClient.get("http://localhost:61619/api/reservation/rentApartment/"+apartmentId+"?fromDate=" +fromDate+"&toDate=" +toDate, { observe: 'response' });
      }
      cancelReservation(reservationId:number):Observable<any>{
        return this.httpClient.get("http://localhost:61619/api/reservation/cancelReservation/"+reservationId, { observe: 'response' });
      }

      
      getAllHostReservations(pageIndex:number, pageSize:number,sortType:string,selectedUsername:string , filter:string[]):Observable<any>{
        let uri:string="http://localhost:61619/api/reservation/getAllHostReservations/"+pageIndex+"/"+pageSize+"/"+sortType+"/"+selectedUsername;
        if(filter.length>0){
          uri=uri+'?';
          filter.forEach(element => {
            uri=uri+'statusFilter='+element+"&";
          });
          uri=uri.slice(0, -1);
        }
        
        return this.httpClient.get(uri, { observe: 'response' });
      }
      
      getHostReservation(reservationId:number):Observable<any>{
        return this.httpClient.get("http://localhost:61619/api/reservation/getHostReservation/"+reservationId, { observe: 'response' });
      }
      getAllAdminReservations(pageIndex:number, pageSize:number,sortType:string,selectedUsername:string , filter:string[]):Observable<any>{
        let uri:string="http://localhost:61619/api/reservation/getAllAdminReservations/"+pageIndex+"/"+pageSize+"/"+sortType+"/"+selectedUsername;
        if(filter.length>0){
          uri=uri+'?';
          filter.forEach(element => {
            uri=uri+'statusFilter='+element+"&";
          });
          uri=uri.slice(0, -1);
        }
        
        return this.httpClient.get(uri, { observe: 'response' });
      }
      
      getAdminReservation(reservationId:number):Observable<any>{
        return this.httpClient.get("http://localhost:61619/api/reservation/getAdminReservation/"+reservationId, { observe: 'response' });
      }
      
      getAcceptRejectReservation(reservationId:number,accept:boolean):Observable<any>{
        return this.httpClient.get("http://localhost:61619/api/reservation/getAcceptRejectReservation/"+reservationId+"/"+accept, { observe: 'response' });
      }
      getCompleteReservation(reservationId:number,):Observable<any>{
        return this.httpClient.get("http://localhost:61619/api/reservation/getCompleteReservation/"+reservationId, { observe: 'response' });
      }
}