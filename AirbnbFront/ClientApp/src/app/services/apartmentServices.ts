import { Injectable } from '@angular/core';

import { Headers, RequestOptions } from '@angular/http';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { Observable } from 'rxjs';

import { apartmentData } from '../models/apartmentData';
import { amenityData } from '../models/amenityData';
import { FileItem, FileUploader } from 'ng2-file-upload';


@Injectable({
  providedIn: 'root'
})
export class ApartmentServices {

  constructor(private httpClient: HttpClient) { }




/*   GetAllApartments(pageIndex: number, pageSize: number,priceSort:string): Observable<any> {
    return this.httpClient.get("http://localhost:61619/api/apartment/getAllApartments/"+pageIndex+"/"+pageSize+"/"+priceSort, { observe: 'response' });

  } */
  //fali sort
  /* GetAllApartmentsSearch(pageIndex: number, pageSize: number, fromDate: string, toDate: string, minPrice: number, maxPrice: number, numOfGuests: number, numOfRooms: number): Observable<any> {
    return this.httpClient.get("http://localhost:61619/api/apartment/getAllApartmentsSearch/"+pageIndex+"/"+pageSize+"/1"+"/"+fromDate+"/"+toDate+"/"+minPrice+"/"+maxPrice+"/"+numOfGuests+"/"+numOfRooms, { observe: 'response' });
    
  } */
  
  getApartmentsHost(pageIndex: number, pageSize: number): Observable<any> {
    return this.httpClient.get("http://localhost:61619/api/apartment/getApartmentsHost/"+pageIndex+"/"+pageSize, { observe: 'response' });
  }
  
  getApartmentInfoHost(apartmentId: number): Observable<any> {
    return this.httpClient.get("http://localhost:61619/api/apartment/getApartmentInfoHost/"+apartmentId, { observe: 'response' });
  }
  getApartmentsAdmin(pageIndex: number, pageSize: number): Observable<any> {
    return this.httpClient.get("http://localhost:61619/api/apartment/getApartmentsAdmin/"+pageIndex+"/"+pageSize, { observe: 'response' });
  }
  
  getApartmentInfoAdmin(apartmentId: number): Observable<any> {
    return this.httpClient.get("http://localhost:61619/api/apartment/getApartmentInfoAdmin/"+apartmentId, { observe: 'response' });
  }
  
  changeApartmentStatus(apartmentId: number,apartmentStatus:boolean): Observable<any> {
    return this.httpClient.get("http://localhost:61619/api/apartment/changeApartmentStatus/"+apartmentId+"/"+apartmentStatus, { observe: 'response' });
  }
  GetAllApartmentsSearch(pageIndex: number, pageSize: number,searchChecked:boolean,amenityFilter: number[],typeFilter: number[],city:string,priceSort:string, fromDate: string, toDate: string, minPrice: number, maxPrice: number, numOfGuests: number, numOfRooms: number): Observable<any> {
    //return this.httpClient.get(, { observe: 'response' });
    let uri:string="http://localhost:61619/api/apartment/getAllApartmentsSearch/"+pageIndex+"/"+pageSize+"/"+searchChecked+"/"+city+"/"+priceSort+"/"+minPrice+"/"+maxPrice+"/"+numOfGuests+"/"+numOfRooms+"?fromDate=" +fromDate+"&toDate=" +toDate
    if(amenityFilter.length>0){
      amenityFilter.forEach(element => {
        uri=uri+'&amenityFilter='+element;
      });
    }
    if(typeFilter.length>0){
      typeFilter.forEach(element => {
        uri=uri+'&typeFilter='+element;
      });
    }

    return this.httpClient.get(uri, { observe: 'response' });
  }
  
  GetAllApartmentsHostSearch(pageIndex: number, pageSize: number,searchChecked:boolean,statusFilter: string,amenityFilter: number[],typeFilter: number[],city:string,priceSort:string, fromDate: string, toDate: string, minPrice: number, maxPrice: number, numOfGuests: number, numOfRooms: number): Observable<any> {

    let uri:string="http://localhost:61619/api/apartment/getAllApartmentsHostSearch/"+pageIndex+"/"+pageSize+"/"+searchChecked+"/"+city+"/"+priceSort+"/"+statusFilter+"/"+minPrice+"/"+maxPrice+"/"+numOfGuests+"/"+numOfRooms+"?fromDate=" +fromDate+"&toDate=" +toDate;
    if(amenityFilter.length>0){
      amenityFilter.forEach(element => {
        uri=uri+'&amenityFilter='+element;
      });
    }
    if(typeFilter.length>0){
      typeFilter.forEach(element => {
        uri=uri+'&typeFilter='+element;
      });
    }

    return this.httpClient.get(uri, { observe: 'response' });
  }
  GetAllApartmentsAdminSearch(pageIndex: number, pageSize: number,searchChecked:boolean,statusFilter: string,amenityFilter: number[],typeFilter: number[],city:string,priceSort:string, fromDate: string, toDate: string, minPrice: number, maxPrice: number, numOfGuests: number, numOfRooms: number): Observable<any> {

    let uri:string="http://localhost:61619/api/apartment/getAllApartmentsAdminSearch/"+pageIndex+"/"+pageSize+"/"+searchChecked+"/"+city+"/"+priceSort+"/"+statusFilter+"/"+minPrice+"/"+maxPrice+"/"+numOfGuests+"/"+numOfRooms+"?fromDate=" +fromDate+"&toDate=" +toDate;
    if(amenityFilter.length>0){
      amenityFilter.forEach(element => {
        uri=uri+'&amenityFilter='+element;
      });
    }
    if(typeFilter.length>0){
      typeFilter.forEach(element => {
        uri=uri+'&typeFilter='+element;
      });
    }


    return this.httpClient.get(uri, { observe: 'response' });
  }
  
  

  deleteApartment(apartmentId: number,hostId:number): Observable<any> {
    return this.httpClient.get("http://localhost:61619/api/apartment/deleteApartment/"+apartmentId+"/"+hostId, { observe: 'response' });
    
  }
  GetApartment(apartmentId: number): Observable<any> {
    return this.httpClient.get("http://localhost:61619/api/apartment/getApartment/"+apartmentId, { observe: 'response' });
    
  }
  GetDisabledDates(apartmentId: number): Observable<any> {
    return this.httpClient.get("http://localhost:61619/api/apartment/getDisabledDates/"+apartmentId, { observe: 'response' });
    
  }
  getImage(name: string): Observable<Blob> {
    return this.httpClient.get('http://localhost:61619/api/apartment/getApartmentPictureData?path=' + name, { responseType: "blob" });
  }
  AddApartment(apartment: any, photos: FileUploader): Observable<any> {

    const formData: FormData = new FormData();
/* let counter=0;
    for (let i = 0; i < photos.length; i++) {
      if( photos[i].name!=":assets:images:default-placeholder.png"){
      formData.append('Image' + counter, photos[i], photos[i].name);
      counter+=1;
    }

    } */
    let amenitiesNum:number=0;
    let photosNum:number=0;

    for ( photosNum = 0; photosNum < photos.queue.length; photosNum++) {
      let fileItem = photos.queue[photosNum]._file;
      console.log(fileItem.name);
      formData.append('Image' + photosNum, fileItem);
      formData.append('fileSeq', 'seq'+photosNum);
    }
    formData.append('PicturesNum', photosNum.toString());

    for (amenitiesNum = 0; amenitiesNum < apartment.Amenities.length; amenitiesNum++) {
      formData.append('Amenity' + amenitiesNum, apartment.Amenities[amenitiesNum].item_id.toString());
    }
    formData.append('AmenitiesNum', amenitiesNum.toString());


    formData.append('ApartmentTypeId', apartment.ApartmentType[0].item_id.toString());
    formData.append('CheckInTime', apartment.CheckInTime.toISOString());
    formData.append('CheckOutTime', apartment.CheckOutTime.toISOString());
    formData.append('NumOfGuests', apartment.NumOfGuests[0].item_id.toString());
    formData.append('NumOfRooms', apartment.NumOfRooms[0].item_id.toString());
    formData.append('fromDate', apartment.RentingDates[0].StartDate.toISOString());
    formData.append('toDate',  apartment.RentingDates[0].EndDate.toISOString());
    formData.append('AddressNum',  apartment.AddressNum.toString());
    formData.append('City',  apartment.City.toString());
    formData.append('CostPerNight',  apartment.CostPerNight.toString());
    formData.append('PostalCode',  apartment.PostalCode.toString());
    formData.append('Latitude',  apartment.Location[0].Latitude.toString());
    formData.append('Longitude',  apartment.Location[0].Longitude.toString());

    return this.httpClient.post('http://localhost:61619/api/apartment/addApartment', formData);

  }

  EditApartment(apartment: any, photos: FileUploader): Observable<any> {

    const formData: FormData = new FormData();
/* let counter=0;
    for (let i = 0; i < photos.length; i++) {
      if( photos[i].name!=":assets:images:default-placeholder.png"){
      formData.append('Image' + counter, photos[i], photos[i].name);
      counter+=1;
    }

    } */
    let amenitiesNum:number=0;
    let photosNum:number=0;

    for ( photosNum = 0; photosNum < photos.queue.length; photosNum++) {
      let fileItem = photos.queue[photosNum]._file;
      console.log(fileItem.name);
      formData.append('Image' + photosNum, fileItem);
      formData.append('fileSeq', 'seq'+photosNum);
    }
    formData.append('PicturesNum', photosNum.toString());

    for (amenitiesNum = 0; amenitiesNum < apartment.Amenities.length; amenitiesNum++) {
      formData.append('Amenity' + amenitiesNum, apartment.Amenities[amenitiesNum].item_id.toString());
    }
    formData.append('AmenitiesNum', amenitiesNum.toString());

    formData.append('AppUserId',apartment.AppUserId.toString() );
    formData.append('ApartmentId', apartment.Id.toString());
    formData.append('ApartmentTypeId', apartment.ApartmentType[0].item_id.toString());
    formData.append('CheckInTime', apartment.CheckInTime.toISOString());
    formData.append('CheckOutTime', apartment.CheckOutTime.toISOString());
    formData.append('NumOfGuests', apartment.NumOfGuests[0].item_id.toString());
    formData.append('NumOfRooms', apartment.NumOfRooms[0].item_id.toString());
    formData.append('fromDate', apartment.RentingDates[0].StartDate.toISOString());
    formData.append('toDate',  apartment.RentingDates[0].EndDate.toISOString());
    formData.append('AddressNum',  apartment.AddressNum.toString());
    formData.append('City',  apartment.City.toString());
    formData.append('CostPerNight',  apartment.CostPerNight.toString());
    formData.append('PostalCode',  apartment.PostalCode.toString());
    formData.append('Latitude',  apartment.Location[0].Latitude.toString());
    formData.append('Longitude',  apartment.Location[0].Longitude.toString());

    return this.httpClient.post('http://localhost:61619/api/apartment/editApartment', formData);

  }
}
