import { Injectable, Output, EventEmitter } from '@angular/core';

import { Headers, RequestOptions } from '@angular/http';
import { HttpClient, HttpHeaders, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Config } from '../../../node_modules/protractor';
import { logInData } from '../models/logInData';
import { btmNavDataService } from '../bottom-navbar/btmNavDataService';
import { finalize } from 'rxjs/operators'
import { ToasterServiceComponent } from '../toaster-service/toaster-service.component';


@Injectable({
    providedIn: 'root'
})
export class LogInService {
    showProgress: boolean = true;
    constructor( private toasterService: ToasterServiceComponent,private btmNavMessageService: btmNavDataService,private httpClient: HttpClient) {
        this.btmNavMessageService.currentMessage.subscribe(message => this.showProgress = message) 
    }

    getTheToken(user: logInData): any {
        this.btmNavMessageService.changeMessage(true);
        let headers = new HttpHeaders();
        headers = headers.append('Content-type', 'application/x-www-form-urlencoded');

        if (!localStorage.jwt) {
           let x = this.httpClient.post('http://localhost:61619/oauth/token', `username=${user.Username}&password=${user.Password}&grant_type=password`, { "headers": headers }) as Observable<any>
            return x.pipe(finalize(
                () => {
                    this.btmNavMessageService.changeMessage(false);
                    
                }))
                .subscribe(
                    data => {
                    console.log(data.access_token);

                    let jwt = data.access_token;

                    let jwtData = jwt.split('.')[1]
                    let decodedJwtJsonData = window.atob(jwtData)
                    let decodedJwtData = JSON.parse(decodedJwtJsonData)

                    let role = decodedJwtData.role

                    console.log('jwtData: ' + jwtData)
                    console.log('decodedJwtJsonData: ' + decodedJwtJsonData)
                    console.log('decodedJwtData: ' + decodedJwtData)
                    console.log('Role ' + role)

                    localStorage.setItem('jwt', jwt)
                    localStorage.setItem('role', role);

                    this.toasterService.Info("Logged In", 'Info');
                    let win = (window as any);
                    win.location.reload();
                },
                error => {
                    this.toasterService.Error(error.error.error_description, 'Error');

                }
            );
        }
    }
    ngOnDestroy() {
        this.btmNavMessageService.changeMessage(false);
      }
}