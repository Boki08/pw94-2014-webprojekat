import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Observable } from 'rxjs';
import { commentData } from '../models/commentData';


@Injectable({
    providedIn: 'root'
})
export class CommentServices {

    constructor(private httpClient: HttpClient) { }


    GetApartmentComments(apartmentId: number): Observable<any> {
        return this.httpClient.get("http://localhost:61619/api/comment/getApartmentComments/" + apartmentId, { observe: 'response' });
    }
    GetAllApartmentComments(apartmentId: number): Observable<any> {
        return this.httpClient.get("http://localhost:61619/api/comment/getAllApartmentComments/" + apartmentId, { observe: 'response' });
    }
    
    PostComment(reservationId:number,comment: commentData): Observable<any> {
        return this.httpClient.post("http://localhost:61619/api/comment/postComment/"+reservationId, comment, { observe: 'response' });
    }
    
    ApproveComment(commentId: number): Observable<any> {
        return this.httpClient.get("http://localhost:61619/api/comment/approveComment/" + commentId, { observe: 'response' });
    }
}
