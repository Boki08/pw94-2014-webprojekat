
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Observable } from 'rxjs';
import { appUserData } from '../models/appUserData';


@Injectable({
    providedIn: 'root'
})
export class ReservationStatusServices {
    constructor(private httpClient: HttpClient) { }
    GetReservationStatuses(): Observable<any> {
        return this.httpClient.get('http://localhost:61619/api/status/getReservationStatuses', { observe: 'response' });
      }
    
}