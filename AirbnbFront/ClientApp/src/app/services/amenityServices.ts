import { Injectable } from '@angular/core';

import { Headers, RequestOptions } from '@angular/http';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { Observable } from 'rxjs';
import { amenityData } from '../models/amenityData';



@Injectable({
  providedIn: 'root'
})
export class AmenityServices {

  constructor(private httpClient: HttpClient) { }

  GetAllAmenities(): Observable<any> {
    return this.httpClient.get('http://localhost:61619/api/amenity/getAllAmenities', { observe: 'response' });
  }
  GetAllAmenitiesAdmin(pageIndex:number, pageSize:number): Observable<any> {
    return this.httpClient.get('http://localhost:61619/api/amenity/getAllAmenitiesAdmin/'+pageIndex+"/"+pageSize, { observe: 'response' });
  }
  
  AddAmenity(amenity:amenityData): Observable<any> {
    return this.httpClient.post('http://localhost:61619/api/amenity/addAmenity',amenity, { observe: 'response' });
  }
  
  EditAmenity(amenity:amenityData): Observable<any> {
    return this.httpClient.post('http://localhost:61619/api/amenity/editAmenity',amenity, { observe: 'response' });
  }
  DeleteAmenity(amenityId:number): Observable<any> {
    return this.httpClient.get('http://localhost:61619/api/amenity/deleteAmenity/'+amenityId, { observe: 'response' });
  }
}
