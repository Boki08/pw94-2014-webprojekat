﻿import { Component, NgZone } from '@angular/core';
import { dropdownData } from '../models/dropdownData';
import { AmenityServices } from '../services/amenityServices';
import { ApartmentTypeServices } from '../services/apartmentTypeServices';
import { btmNavDataService } from '../bottom-navbar/btmNavDataService';
import { ToasterServiceComponent } from '../toaster-service/toaster-service.component';
import { finalize } from 'rxjs/operators';
import { apartmentTypeData } from '../models/apartmentTypeData';
import { amenityData } from '../models/amenityData';
import { NgbDate, NgbCalendar } from '@ng-bootstrap/ng-bootstrap';
import { FormGroup, FormControl, Validators, FormBuilder, NgForm } from '@angular/forms';
import OlMap from 'ol/Map';
import OlXYZ from 'ol/source/XYZ';
import OlTileLayer from 'ol/layer/Tile';
import OlView from 'ol/View';
import Feature from 'ol/Feature.js';
import Point from 'ol/geom/Point.js';
import { Tile as TileLayer, Vector as VectorLayer } from 'ol/layer.js';
import { OSM, Vector as VectorSource } from 'ol/source.js';
import { toLonLat, transform } from 'ol/proj.js';
import { Overlay } from 'ol/Overlay.js';
import { fromLonLat } from 'ol/proj';
import { FileUploader } from "ng2-file-upload";
import { apartmentData } from '../models/apartmentData';
import { ApartmentServices } from '../services/apartmentServices';
import { locationData } from '../models/locationData';
import { rentingDate } from '../models/rentingDate';
import { ActivatedRoute } from '@angular/router';

@Component({
    selector: 'app-edit-apartment',
    templateUrl: './edit-apartment.component.html',
    styleUrls: ['./edit-apartment.component.css']
})
/** edit-apartment component*/
export class EditApartmentComponent {

    /** add-apartment ctor */

    showProgress: boolean = true;
    stopNav: number = 0;
    dropdownVisible: boolean = false;
    mapCreated: boolean = false;

    dropdownApartmentTypes = [];
    selectedApartmentType = [];
    //apartmentTypesTemp: apartmentTypeData[];//vrv ne treba
    dropdownApartmentTypesSettings = {};

    dropdownListAmenities = [];
    selectedAmenities = [];
    dropdownAmenitiesSettings = {};

    dropdownListNumOfRooms = [];
    selectedNumOfRooms = [];
    dropdownNumOfRoomsSettings = {};

    dropdownListNumOfGuests = [];
    selectedNumOfGuests = [];
    dropdownNumOfGuestsSettings = {};

    CheckInTime = { hour: 14, minute: 0 };
    CheckOutTime = { hour: 10, minute: 0 };

    hoveredDate: NgbDate;

    fromDate: NgbDate;
    todayDate: NgbDate;

    startDate: NgbDate;
    endDate: NgbDate;

    fromDateString: string;
    toDate: NgbDate;
    toDateString: string;
    dateString: string;
    cityInput: string;
    postalCodeInput: number;
    streetAndNumberInput: string;

    addApartmentForm: FormGroup;
    CostPerNight: FormControl;
    DatePicker: FormControl;
    document: FormControl;
    Amenities: FormControl;
    ApartmentType: FormControl;
    NumOfRooms: FormControl;
    NumOfGuests: FormControl;
    AddressNum: FormControl;
    City: FormControl;
    PostalCode: FormControl;

    map: OlMap;
    source: OlXYZ;
    layer: OlTileLayer;
    markerLayer: VectorLayer;
    view: OlView;
    markerSource = new VectorSource();
    locationAdded: boolean = false;

    disableBtn: boolean = true;

    lon: number;
    lat: number;
    apartmentId: number;
    cost: number;

    apartment: apartmentData;

    public uploader: FileUploader = new FileUploader({
        isHTML5: true
    });

    //amenitiesTemp: amenityData[];//vrv ne treba
    constructor(private activatedRoute: ActivatedRoute, private apartmentService: ApartmentServices, private fb: FormBuilder, private zone: NgZone, private calendar: NgbCalendar, private toasterService: ToasterServiceComponent, private btmNavMessageService: btmNavDataService, private amenityServices: AmenityServices, private apartmentTypeServices: ApartmentTypeServices, ) {
        activatedRoute.params.subscribe(params => { this.apartmentId = params["apartmentId"] });
        this.fromDate = this.calendar.getToday();
        this.todayDate = this.calendar.getToday();

        this.toDate = this.calendar.getNext(calendar.getToday(), 'd', 10);

        zone.onStable.subscribe(() => {
            this.createMap();

        });
    }
    ngOnInit() {
        this.btmNavMessageService.currentMessage.subscribe(message => this.showProgress = message)
        this.btmNavMessageService.changeMessage(true);
        /* this.uploadForm = this.fb.group({
            document: [null, null],
            type:  [null, Validators.compose([Validators.required])]
          }); */





        this.dropdownApartmentTypesSettings = {
            singleSelection: true,
            idField: 'item_id',
            textField: 'item_text',

            allowSearchFilter: false
        };
        this.dropdownAmenitiesSettings = {
            singleSelection: false,
            idField: 'item_id',
            textField: 'item_text',
            selectAllText: 'Select All',
            unSelectAllText: 'UnSelect All',
            itemsShowLimit: 2,
            allowSearchFilter: true
        };

        this.dropdownNumOfRoomsSettings = {
            singleSelection: true,
            idField: 'item_id',
            textField: 'item_text',

            allowSearchFilter: false
        };

        this.dropdownNumOfGuestsSettings = {
            singleSelection: true,
            idField: 'item_id',
            textField: 'item_text',

            allowSearchFilter: false
        };
        /* this.dropdownListNumOfGuests = [
            new dropdownData('1', '1 - Guest'), new dropdownData('2', '2 - Guests'), new dropdownData('3', '3 - Guests'), new dropdownData('4', '4 - Guests'), new dropdownData('5', '5 - Guests'), new dropdownData('6', '6 - Guests'), new dropdownData('7', '7 - Guests'), new dropdownData('8', '8 - Guests'), new dropdownData('9', '9 - Guests'), new dropdownData('10', '10+ - Guests'),
        ]; */
        this.dropdownListNumOfGuests = [
            { item_id: 1, item_text: '1 - Guest' }, { item_id: 2, item_text: '2 - Guest' }, { item_id: 3, item_text: '3 - Guest' }, { item_id: 4, item_text: '4 - Guest' }, { item_id: 5, item_text: '5 - Guest' }, { item_id: 6, item_text: '6 - Guest' }, { item_id: 7, item_text: '7 - Guest' }, { item_id: 8, item_text: '8 - Guest' }, { item_id: 9, item_text: '9 - Guest' }, { item_id: 10, item_text: '10+ - Guest' }
        ];
        this.selectedNumOfGuests = this.dropdownListNumOfGuests[0];

        /* this.addApartmentForm = this.fb.group({
            ApartmentType: [this.selectedNumOfGuests]
        }); */

        this.dropdownListNumOfRooms = [
            new dropdownData('1', '1 - Room'), new dropdownData('2', '2 - Rooms'), new dropdownData('3', '3 - Rooms'), new dropdownData('4', '4 - Rooms'), new dropdownData('5', '5 - Rooms'), new dropdownData('6', '6 - Rooms'), new dropdownData('7', '7 - Rooms'), new dropdownData('8', '8 - Rooms'), new dropdownData('9', '9 - Rooms'), new dropdownData('10', '10+ - Rooms'),
        ];
        this.selectedNumOfRooms = [new dropdownData('1', '1 - Room')];



        this.apartmentTypeServices.GetApartmentTypes().pipe(finalize(
            () => {
                this.StopNav();
            }))
            .subscribe(
                data => {
                    // this.apartmentTypesTemp = data.body as apartmentTypeData[];

                    for (let a of data.body as apartmentTypeData[]) {
                        this.dropdownApartmentTypes.push({ item_id: a.Id, item_text: a.Type })
                    }

                },
                error => {
                    if (error.error.Message == undefined) {
                        this.toasterService.Error("Undefined Error", 'Error');
                    }
                    else {
                        this.toasterService.Error(error.error.Message, 'Error');
                    }
                }
            );

        this.amenityServices.GetAllAmenities().pipe(finalize(
            () => {
                this.StopNav();
            }))
            .subscribe(
                data => {
                    // this.amenitiesTemp = data.body as amenityData[];
                    //this.apartmentTypesTemp.push(new apartmentTypeData(-1, "All"))
                    for (let a of data.body as amenityData[]) {
                        this.dropdownListAmenities.push({ item_id: a.Id, item_text: a.Type })
                    }

                },
                error => {
                    if (error.error.Message == undefined) {
                        this.toasterService.Error("Undefined Error", 'Error');
                    }
                    else {
                        this.toasterService.Error(error.error.Message, 'Error');
                    }
                }
            );
        this.apartmentService.GetApartment(this.apartmentId).pipe(finalize(
            () => {

                this.CreateFormControls();
                this.CreateForm();
                this.StopNav();
            }))
            .subscribe(
                data => {
                    this.apartment = data.body as apartmentData;
                    //this.mainVisible = true;


                    let date: Date = new Date(this.apartment.RentingDates[0].EndDate);
                    this.endDate = new NgbDate(date.getFullYear(), date.getMonth() + 1, date.getDate());


                    date = new Date(this.apartment.RentingDates[0].StartDate);
                    let startDateNgb: NgbDate = new NgbDate(date.getFullYear(), date.getMonth() + 1, date.getDate());
                    //if (this.startDate.before(startDateNgb)) {
                    this.startDate = startDateNgb;
                    //}
                    date = new Date(this.apartment.CheckInTime);
                    this.CheckInTime = { hour: date.getHours(), minute: date.getMinutes() };
                    date = new Date(this.apartment.CheckOutTime);
                    this.CheckOutTime = { hour: date.getHours(), minute: date.getMinutes() };

                    this.apartment.Amenities.forEach(element => {
                        this.selectedAmenities.push({ item_id: element.Id, item_text: element.Type })
                    });
                    this.selectedApartmentType = [{ item_id: this.apartment.ApartmentType.Id, item_text: this.apartment.ApartmentType.Type }];
                    this.selectedNumOfGuests = [this.dropdownListNumOfGuests[this.apartment.NumOfGuests - 1]];
                    this.selectedNumOfRooms = [this.dropdownListNumOfRooms[this.apartment.NumOfRooms - 1]];
                    this.cost = this.apartment.Price;
                    this.cityInput = this.apartment.Location[0].Address[0].City;
                    this.postalCodeInput = this.apartment.Location[0].Address[0].PostalCode;
                    this.streetAndNumberInput = this.apartment.Location[0].Address[0].StreetAndNumber;

                    let endDate: Date = new Date(this.apartment.RentingDates[0].EndDate);
                    let startDate: Date = new Date(this.apartment.RentingDates[0].StartDate);


                    this.toDateString = endDate.getDate() + "/" + endDate.getMonth() + "/" + endDate.getFullYear();
                    this.fromDateString = startDate.getDate() + "/" + startDate.getMonth() + "/" + startDate.getFullYear();
                    this.dateString = this.fromDateString + " - " + this.toDateString;

                    this.fromDate = new NgbDate(startDate.getFullYear(), startDate.getMonth() + 1, startDate.getDate())
                    this.toDate = new NgbDate(endDate.getFullYear(), endDate.getMonth() + 1, endDate.getDate())

                },
                error => {
                    //this.showApartmentWarning = false;
                    if (error.error.Message == undefined) {
                        this.toasterService.Error("Undefined Error", 'Error');
                    }
                    else {
                        this.toasterService.Error(error.error.Message, 'Error');
                    }
                }
            );

    }
    ngOnDestroy() {
        this.btmNavMessageService.changeMessage(false);
    }


    CreateFormControls() {
        this.CostPerNight = new FormControl('', [
            Validators.required,
            Validators.maxLength(7),
            Validators.pattern("^[0-9]+(\.[0-9][0-9]?)?$"),
        ]);
        this.Amenities = new FormControl(this.selectedAmenities, [
            Validators.required,

        ]);
        this.ApartmentType = new FormControl(this.selectedNumOfGuests, [
            Validators.required,

        ]);
        this.NumOfRooms = new FormControl(this.selectedNumOfRooms, [
            Validators.required,

        ]);
        this.NumOfGuests = new FormControl(this.selectedNumOfGuests, [
            Validators.required,

        ]);
        this.AddressNum = new FormControl(this.streetAndNumberInput, [
            Validators.required,
            Validators.maxLength(50),
        ]);
        this.City = new FormControl(this.cityInput, [
            Validators.required,
            Validators.maxLength(10),
        ]);
        this.PostalCode = new FormControl(this.postalCodeInput, [
            Validators.required,
            Validators.maxLength(7),
        ]);
        /*  this.fb.group({
             document: [null, null],
             type:  [null, Validators.compose([Validators.required])]
           }); */
        // this.DatePicker = new FormControl('', [
        //    Validators.required,
        // ]);


    }
    CreateForm() {
        this.addApartmentForm = new FormGroup({
            CostPerNight: this.CostPerNight,
            Amenities: this.Amenities,
            ApartmentType: this.ApartmentType,
            NumOfRooms: this.NumOfRooms,
            NumOfGuests: this.NumOfGuests,
            AddressNum: this.AddressNum,
            City: this.City,
            PostalCode: this.PostalCode,
            // DatePicker:this.DatePicker,
        });
    }

    createMap() {
        if (this.dropdownVisible && !this.mapCreated) {
            this.mapCreated = true;
            this.source = new OlXYZ({
                url: 'http://tile.osm.org/{z}/{x}/{y}.png'
            });

            this.layer = new OlTileLayer({
                source: this.source
            });


            this.view = new OlView({
                center: fromLonLat([this.apartment.Location[0].Longitude, this.apartment.Location[0].Latitude]),
                zoom: 17
            });

            this.map = new OlMap({
                target: 'map',
                layers: [this.layer],
                view: this.view, maxResolution: 1000

            });


            //var source = new VectorSource({});
            this.markerLayer = new VectorLayer({ source: this.markerSource });
            this.map.addLayer(this.markerLayer);

            this.map.on('singleclick', (event) => {
                var lonLat = toLonLat(event.coordinate);
                this.addMarker(lonLat[0], lonLat[1]);
            });

            this.lat = this.apartment.Location[0].Latitude;
            this.lon = this.apartment.Location[0].Longitude;
            var iconFeature = new Feature({
                geometry: new Point(transform([this.apartment.Location[0].Longitude, this.apartment.Location[0].Latitude], 'EPSG:4326',
                    'EPSG:3857')),
                /*  name: 'Null Island',
                 population: 4000,
                 rainfall: 500 */
            });

            this.markerSource.addFeature(iconFeature);
        }
    }
    addMarker(lon: number, lat: number): void {
        console.log('lon:', lon);
        console.log('lat:', lat);

        this.lon = lon;
        this.lat = lat;

        var iconFeatures = [];

        /*         if (this.markers == null) {
                    this.markers = new Layer.Markers("Markers");
                    this.map.addLayer(this.markers);
                } */

        var iconFeature = new Feature({
            geometry: new Point(transform([lon, lat], 'EPSG:4326',
                'EPSG:3857')),
            /* name: 'Null Island',
            population: 4000,
            rainfall: 500 */
        });
        this.map.removeLayer(this.markerLayer);
        this.markerSource = new VectorSource();
        this.markerLayer = new VectorLayer({ source: this.markerSource });
        this.map.addLayer(this.markerLayer);

        this.markerSource.addFeature(iconFeature);
        this.locationAdded = true
    }

    uploadSubmit() {
        for (let i = 0; i < this.uploader.queue.length; i++) {
            let fileItem = this.uploader.queue[i]._file;
            if (fileItem.size > 10000000) {
                alert("Each File should be less than 10 MB of size.");
                return;
            }
        }
        for (let j = 0; j < this.uploader.queue.length; j++) {
            let data = new FormData();
            let fileItem = this.uploader.queue[j]._file;
            console.log(fileItem.name);
            data.append('file', fileItem);
            data.append('fileSeq', 'seq' + j);
            //data.append( 'dataType', this.addApartmentForm.controls.type.value);
            // this.uploadFile(data).subscribe(data => alert(data.message));
        }
        this.uploader.clearQueue();
    }

    /* uploadFile(data: FormData): Observable {
      return this.http.post('http://localhost:8080/upload', data);
    } */
    /* onDeSelectNumOfGuests(){
        this.disableBtn=false;
    }
    onSelectNumOfGuests(){
      this.disableBtn=false;
    } */
    onSelectAmenity() {
        this.selectedAmenities;
    }
    onSubmit(apartment: apartmentData, form: NgForm) {
        this.disableBtn = true;
        this.btmNavMessageService.changeMessage(true);

        apartment.Id = this.apartmentId;
        apartment.AppUserId = this.apartment.AppUserId;
        apartment.CheckOutTime = new Date();
        apartment.CheckOutTime.setHours(this.CheckOutTime.hour);
        apartment.CheckOutTime.setMinutes(this.CheckOutTime.minute);
        apartment.CheckInTime = new Date();
        apartment.CheckInTime.setHours(this.CheckInTime.hour);
        apartment.CheckInTime.setMinutes(this.CheckInTime.minute);
        apartment.RentingDates = [new rentingDate(0, 0, new Date(), new Date(), null)];
        apartment.RentingDates[0].StartDate.setFullYear(this.fromDate.year);
        apartment.RentingDates[0].StartDate.setMonth(this.fromDate.month - 1);
        apartment.RentingDates[0].StartDate.setDate(this.fromDate.day);
        apartment.RentingDates[0].EndDate.setFullYear(this.toDate.year);
        apartment.RentingDates[0].EndDate.setMonth(this.toDate.month - 1);
        apartment.RentingDates[0].EndDate.setDate(this.toDate.day);
        apartment.Location = [new locationData(0, 0, 0, 0, null, null)];
        apartment.Location[0].Latitude = this.lat;
        apartment.Location[0].Longitude = this.lon;


        this.apartmentService.EditApartment(apartment, this.uploader).pipe(finalize(
            () => {

                this.btmNavMessageService.changeMessage(false);
                this.disableBtn = false;
            }))
            .subscribe(
                data => {

                    this.toasterService.Info(data, 'Info');
                },
                error => {
                    this.toasterService.Error(error.error.Message, 'Error');
                }
            );

    }
    StopNav() {
        if (this.stopNav == 2) {
            this.btmNavMessageService.changeMessage(false);
            this.dropdownVisible = true;
            this.disableBtn = false;

        }
        else if (this.stopNav < 2) {
            this.stopNav += 1;
        }
    }


    onDateSelection(date: NgbDate) {
        if (!this.fromDate && !this.toDate) {
            this.fromDate = date;
            this.fromDateString = date.day + "/" + date.month + "/" + date.year;
            this.dateString = this.fromDateString + " - " + this.toDateString;
        } else if (this.fromDate && !this.toDate && date.after(this.fromDate)) {
            this.toDate = date;
            this.toDateString = date.day + "/" + date.month + "/" + date.year;
            this.dateString = this.fromDateString + " - " + this.toDateString;
        } else {
            this.toDate = null;
            this.fromDate = date;
            this.fromDateString = date.day + "/" + date.month + "/" + date.year;
            this.dateString = this.fromDateString + " - " + this.toDateString;
        }
    }

    isHovered(date: NgbDate) {
        return this.fromDate && !this.toDate && this.hoveredDate && date.after(this.fromDate) && date.before(this.hoveredDate);
    }

    isInside(date: NgbDate) {
        return date.after(this.fromDate) && date.before(this.toDate);
    }

    isRange(date: NgbDate) {
        return date.equals(this.fromDate) || date.equals(this.toDate) || this.isInside(date) || this.isHovered(date);
    }
}