﻿/// <reference path="../../../../node_modules/@types/jasmine/index.d.ts" />
import { TestBed, async, ComponentFixture, ComponentFixtureAutoDetect } from '@angular/core/testing';
import { BrowserModule, By } from "@angular/platform-browser";
import { AdminReservationsComponent } from './admin-reservations.component';

let component: AdminReservationsComponent;
let fixture: ComponentFixture<AdminReservationsComponent>;

describe('admin-reservations component', () => {
    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [ AdminReservationsComponent ],
            imports: [ BrowserModule ],
            providers: [
                { provide: ComponentFixtureAutoDetect, useValue: true }
            ]
        });
        fixture = TestBed.createComponent(AdminReservationsComponent);
        component = fixture.componentInstance;
    }));

    it('should do something', async(() => {
        expect(true).toEqual(true);
    }));
});