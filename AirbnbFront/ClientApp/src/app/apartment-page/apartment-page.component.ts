﻿import { Component, OnInit, ViewChild, ElementRef, NgZone } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { btmNavDataService } from '../bottom-navbar/btmNavDataService';
import { apartmentData } from '../models/apartmentData';
import { ApartmentServices } from '../services/apartmentServices';
import { finalize } from 'rxjs/operators';
import { ToasterServiceComponent } from '../toaster-service/toaster-service.component';
import OlMap from 'ol/Map';
import OlXYZ from 'ol/source/XYZ';
import OlTileLayer from 'ol/layer/Tile';
import OlView from 'ol/View';
import Feature from 'ol/Feature.js';
import Point from 'ol/geom/Point.js';
import { Tile as TileLayer, Vector as VectorLayer } from 'ol/layer.js';
import { OSM, Vector as VectorSource } from 'ol/source.js';
import { toLonLat, transform } from 'ol/proj.js';
import { Overlay } from 'ol/Overlay.js';
import { fromLonLat } from 'ol/proj';
import { CommentServices } from '../services/commentServices';
import { commentData } from '../models/commentData';
import { NgbDate, NgbDateStruct, NgbCalendar } from '@ng-bootstrap/ng-bootstrap';
import { ReservationServices } from '../services/reservationServices';

@Component({
    selector: 'app-apartment-page',
    templateUrl: './apartment-page.component.html',
    styleUrls: ['./apartment-page.component.css']
})
/** apartment-page component*/
export class ApartmentPageComponent implements OnInit {

    showProgress: boolean = true;
    apartmentId: number;
    apartment: apartmentData;
    stopNav: number = 0;
    showApartmentWarning: boolean = false;
    mainVisible: boolean = false;
    showModalProgress: boolean = false;
    comments: commentData[];
    mapCreated: boolean = false;
    showDatePicker: boolean = false;
    rentButtonDisabled: boolean = true;
    appUserLog: boolean = false;
    hostLog: boolean = false;
    adminLog: boolean = false;
    logedIn: boolean = false;

    map: OlMap;
    source: OlXYZ;
    layer: OlTileLayer;
    view: OlView;
    markerSource = new VectorSource();

    isDisabled = (date: NgbDateStruct, current: { month: number, year: number }) => {
        return this.disabledNgbDates.find(x => NgbDate.from(x).equals(date)) ? true : false;
    }

    hoveredDate: NgbDate;
    fromDate: NgbDate;
    startDate: NgbDate;
    endDate: NgbDate;
    fromDateString: string;
    toDate: NgbDate;
    toDateString: string;
    dateString: string;


    disabledNgbDates: NgbDate[] = [];
    disabledDates: Date[] = [];


    /** apartment-page ctor */
    constructor(private reservationServices: ReservationServices, private calendar: NgbCalendar, private commentServices: CommentServices, private zone: NgZone, private toasterService: ToasterServiceComponent, private apartmentServices: ApartmentServices, private activatedRoute: ActivatedRoute, private btmNavMessageService: btmNavDataService) {
        activatedRoute.params.subscribe(params => { this.apartmentId = params["apartmentId"] });
        zone.onStable.subscribe(() => {
            this.createMap();

        });

        if (localStorage.role != null && localStorage.role == "Guest") {
            this.appUserLog = true;
        }
        else if (localStorage.role != null && localStorage.role == "Host") {
            this.hostLog = true;
        }
        else if (localStorage.role != null && localStorage.role == "Admin") {
            this.adminLog = true;
        }
        else {
            this.logedIn = false;
        }
    }

    ngOnInit(): void {
        this.btmNavMessageService.currentMessage.subscribe(message => this.showProgress = message)
        this.btmNavMessageService.changeMessage(true);
        this.startDate = this.calendar.getToday();
        if (this.hostLog) {
            this.apartmentServices.getApartmentInfoHost(this.apartmentId).pipe(finalize(
                () => {
                    this.StopNav();
                }))
                .subscribe(
                    data => {
                        this.apartment = data.body as apartmentData;
                        this.mainVisible = true;

                        // this.createMap();
                        let date: Date = new Date(this.apartment.RentingDates[0].EndDate);
                        this.endDate = new NgbDate(date.getFullYear(), date.getMonth() + 1, date.getDate());


                        date = new Date(this.apartment.RentingDates[0].StartDate);
                        let startDateNgb: NgbDate = new NgbDate(date.getFullYear(), date.getMonth() + 1, date.getDate());
                        if (this.startDate.before(startDateNgb)) {
                            this.startDate = startDateNgb;
                        }

                    },
                    error => {
                        this.showApartmentWarning = false;
                        if (error.error.Message == undefined) {
                            this.toasterService.Error("Undefined Error", 'Error');
                        }
                        else {
                            this.toasterService.Error(error.error.Message, 'Error');
                        }
                    }
                );
        }
        else if (this.adminLog) {
            this.apartmentServices.getApartmentInfoAdmin(this.apartmentId).pipe(finalize(
                () => {
                    this.StopNav();
                }))
                .subscribe(
                    data => {
                        this.apartment = data.body as apartmentData;
                        this.mainVisible = true;

                        // this.createMap();
                        let date: Date = new Date(this.apartment.RentingDates[0].EndDate);
                        this.endDate = new NgbDate(date.getFullYear(), date.getMonth() + 1, date.getDate());


                        date = new Date(this.apartment.RentingDates[0].StartDate);
                        let startDateNgb: NgbDate = new NgbDate(date.getFullYear(), date.getMonth() + 1, date.getDate());
                        if (this.startDate.before(startDateNgb)) {
                            this.startDate = startDateNgb;
                        }

                    },
                    error => {
                        this.showApartmentWarning = false;
                        if (error.error.Message == undefined) {
                            this.toasterService.Error("Undefined Error", 'Error');
                        }
                        else {
                            this.toasterService.Error(error.error.Message, 'Error');
                        }
                    }
                );
        }
        else {
            this.apartmentServices.GetApartment(this.apartmentId).pipe(finalize(
                () => {
                    this.StopNav();
                }))
                .subscribe(
                    data => {
                        this.apartment = data.body as apartmentData;
                        this.mainVisible = true;

                        // this.createMap();
                        let date: Date = new Date(this.apartment.RentingDates[0].EndDate);
                        this.endDate = new NgbDate(date.getFullYear(), date.getMonth() + 1, date.getDate());


                        date = new Date(this.apartment.RentingDates[0].StartDate);
                        let startDateNgb: NgbDate = new NgbDate(date.getFullYear(), date.getMonth() + 1, date.getDate());
                        if (this.startDate.before(startDateNgb)) {
                            this.startDate = startDateNgb;
                        }

                    },
                    error => {
                        this.showApartmentWarning = false;
                        if (error.error.Message == undefined) {
                            this.toasterService.Error("Undefined Error", 'Error');
                        }
                        else {
                            this.toasterService.Error(error.error.Message, 'Error');
                        }
                    }
                );


        }

    }
    /* ngAfterViewInit(){
        this.createMap();
    } */
    ngOnDestroy() {
        this.btmNavMessageService.changeMessage(false);
    }
    getDisabledDates() {
        this.rentButtonDisabled = true;
        this.showDatePicker = false;
        this.apartmentServices.GetDisabledDates(this.apartmentId).pipe(finalize(
            () => {

            }))
            .subscribe(
                data => {
                    //this.disabledDates = data.body as NgbDate[];
                    //this.mainVisible = true;
                    data.body.forEach(element => {
                        let date: Date = new Date(element);
                        this.disabledDates.push(new Date(element));
                        this.disabledNgbDates.push(new NgbDate(date.getFullYear(), date.getMonth() + 1, date.getDate()));
                    });
                    this.showDatePicker = true;
                },
                error => {
                    // this.showApartmentWarning = false;
                    if (error.error.Message == undefined) {
                        this.toasterService.Error("Undefined Error", 'Error');
                    }
                    else {
                        this.toasterService.Error(error.error.Message, 'Error');
                    }
                }
            );
    }
    createMap() {
        if (this.mainVisible && !this.mapCreated) {
            this.mapCreated = true;
            this.source = new OlXYZ({
                url: 'http://tile.osm.org/{z}/{x}/{y}.png'
            });

            this.layer = new OlTileLayer({
                source: this.source
            });



            this.view = new OlView({
                center: fromLonLat([this.apartment.Location[0].Longitude, this.apartment.Location[0].Latitude]),
                zoom: 17
            });

            this.map = new OlMap({
                target: 'map',
                layers: [this.layer],
                view: this.view, maxResolution: 1000

            });


            //var source = new VectorSource({});
            var layer = new VectorLayer({ source: this.markerSource });
            this.map.addLayer(layer);


            /* var marker = new Feature({
                geometry: new Point([this.apartment.Location[0].Longitude, this.apartment.Location[0].Latitude])
            });
            this.markerSource.addFeature(marker); */
            /*  var marker = new Feature({
                 geometry: new Point([this.apartment.Location[0].Latitude, this.apartment.Location[0].Longitude])
             });
             this.markerSource.addFeature(marker); */

            var iconFeature = new Feature({
                geometry: new Point(transform([this.apartment.Location[0].Longitude, this.apartment.Location[0].Latitude], 'EPSG:4326',
                    'EPSG:3857')),
                /*  name: 'Null Island',
                 population: 4000,
                 rainfall: 500 */
            });

            this.markerSource.addFeature(iconFeature);
        }
    }
    setCheckBoxService(event, comment: commentData, i: number) {
        this.showModalProgress = true;
        // this.checkBoxDisabled = true;
        let serviceActivated: boolean;
        if (event.target.checked) {
            serviceActivated = true;
        }
        else {
            serviceActivated = false;
        }


        this.commentServices.ApproveComment(comment.Id).pipe(finalize(
            () => {
                // this.checkBoxDisabled=false
                this.showModalProgress = false;
            }))
            .subscribe(
                data => {

                    this.comments[i] = data.body as commentData;
                    if (this.comments[i].Approved) {
                        this.toasterService.Info("Comment allowed", 'Info');
                    }
                    else {
                        this.toasterService.Info("Comment not allowed", 'Info');
                    }
                },
                error => {

                    this.toasterService.Error(error.error.Message, 'Error');

                }
            )
    }
    StopNav() {
        if (this.stopNav == 0) {
            this.btmNavMessageService.changeMessage(false);
            //this.dropdownVisible = true;

        }
        else if (this.stopNav < 2) {
            this.stopNav += 1;
        }
    }
    rentApartment() {
        this.showModalProgress = true;
        let toDate = new Date(this.toDate.year, this.toDate.month - 1, this.toDate.day).toISOString();
        let fromDate = new Date(this.fromDate.year, this.fromDate.month - 1, this.fromDate.day).toISOString();
        this.reservationServices.rentApartment(this.apartmentId, fromDate, toDate).pipe(finalize(
            () => {
                this.showModalProgress = false;
            }))
            .subscribe(
                data => {

                    this.toasterService.Success('Reservation was created!', 'Success');

                },
                error => {
                    this.toasterService.Error(error.error.Message, 'Error');
                })
    }
    getComments() {
        this.showModalProgress = true;
        if (this.hostLog == true || this.adminLog == true) {
            this.commentServices.GetAllApartmentComments(this.apartmentId).pipe(finalize(
                () => {
                    this.showModalProgress = false;
                }))
                .subscribe(
                    data => {

                        this.comments = data.body as commentData[];

                    },
                    error => {
                        this.toasterService.Error(error.error.Message, 'Error');
                    })
        }

        else {
            this.commentServices.GetApartmentComments(this.apartmentId).pipe(finalize(
                () => {
                    this.showModalProgress = false;
                }))
                .subscribe(
                    data => {

                        this.comments = data.body as commentData[];

                    },
                    error => {
                        this.toasterService.Error(error.error.Message, 'Error');
                    })
        }
    }


    onDateSelection(date: NgbDate) {
        let toDateTemp: Date = new Date(date.year, date.month - 1, date.day);
        /* this.disabledDates.forEach(element => {
            if (element.toISOString() == toDateTemp.toISOString()) {
                allow = false;
                return;
                
            }
        }); */
        /* for(let i=0;i<this.disabledDates.length;i++){
            if (this.disabledDates[i].toISOString() == toDateTemp.toISOString()) {
                return;
                
            }
        } */

        if (!this.fromDate && !this.toDate) {
            this.fromDate = date;
            this.fromDateString = date.day + "/" + date.month + "/" + date.year;
            this.dateString = this.fromDateString + " - " + this.toDateString;
        } else if (this.fromDate && !this.toDate && date.after(this.fromDate)) {

            let testDate: Date = new Date(this.fromDate.year, this.fromDate.month - 1, this.fromDate.day);
            for (testDate; testDate.toISOString() != toDateTemp.toISOString(); testDate.setDate(testDate.getDate() + 1)) {
                for (let i = 0; i < this.disabledDates.length; i++) {
                    if (this.disabledDates[i].toISOString() == testDate.toISOString()) {
                        return;

                    }
                }
            }
            this.toDate = date;
            this.toDateString = date.day + "/" + date.month + "/" + date.year;
            this.dateString = this.fromDateString + " - " + this.toDateString;

            this.rentButtonDisabled = false;
        } else {
            this.toDate = null;
            this.fromDate = date;
            this.fromDateString = date.day + "/" + date.month + "/" + date.year;
            this.dateString = this.fromDateString + " - " + this.toDateString;
        }

    }

    isHovered(date: NgbDate) {
        return this.fromDate && !this.toDate && this.hoveredDate && date.after(this.fromDate) && date.before(this.hoveredDate);
    }

    isInside(date: NgbDate) {
        return date.after(this.fromDate) && date.before(this.toDate);
    }

    isRange(date: NgbDate) {
        return date.equals(this.fromDate) || date.equals(this.toDate) || this.isInside(date) || this.isHovered(date);
    }
}