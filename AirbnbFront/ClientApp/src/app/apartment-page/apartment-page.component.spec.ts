﻿/// <reference path="../../../../node_modules/@types/jasmine/index.d.ts" />
import { TestBed, async, ComponentFixture, ComponentFixtureAutoDetect } from '@angular/core/testing';
import { BrowserModule, By } from "@angular/platform-browser";
import { ApartmentPageComponent } from './apartment-page.component';

let component: ApartmentPageComponent;
let fixture: ComponentFixture<ApartmentPageComponent>;

describe('apartment-page component', () => {
    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [ ApartmentPageComponent ],
            imports: [ BrowserModule ],
            providers: [
                { provide: ComponentFixtureAutoDetect, useValue: true }
            ]
        });
        fixture = TestBed.createComponent(ApartmentPageComponent);
        component = fixture.componentInstance;
    }));

    it('should do something', async(() => {
        expect(true).toEqual(true);
    }));
});