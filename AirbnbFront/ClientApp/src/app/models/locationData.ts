import { appUserData } from './appUserData';
import { addressData } from './addressData';

export class locationData{
    constructor(
        public Id:number,
        public ApartmentId :number,
        public Latitude  :number ,
        public Longitude  :number ,
        public AppUser:appUserData,
        public Address:addressData[],
        
    ){ }
}