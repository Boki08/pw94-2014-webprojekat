export class registerData{
    constructor(
        public Name: string,
        public Surname: string,
        public Gender: string,
        public Username: string,
        public Password: string,
        public ConfirmPassword: string
    ){ }
}
