import { apartmentData } from './apartmentData';

export class rentingDate{
    constructor(
        public Id: number,
        public ApartmentId: number,
        public StartDate: Date,
        public EndDate: Date,
        public Apartment: apartmentData,
    ){ }
}
