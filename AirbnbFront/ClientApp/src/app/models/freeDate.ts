import { apartmentData } from './apartmentData';

export class freeDate{
    constructor(
        public Id: number,
        public ApartmentId: number,
        public StartDate: Date,
        public EndDate: Date,
        public Apartment: apartmentData,
    ){ }
}
