import { reservationData } from './reservationData';


export class statusData{
    constructor(
        public Id:number,
        public Description :string,
        public ApartmentId  :number ,
        public Reservations  :reservationData[],

    ){ }
}
