import { locationData } from './locationData';

export class addressData{
    constructor(
        public Id:number,
        public LocationId :number,
        public StreetAndNumber  :string ,
        public City  :string ,
        public PostalCode:number,
        public Location:locationData,
        
    ){ }
}
