import { apartmentData } from "./apartmentData";
import { reservationData } from "./reservationData";
export class appUserData{
    constructor(
        public Id:number,
        public UserName:string,
        public Name :string,
        public Surname  :string ,
        public Gender  :string ,
        public Blocked:boolean,
        public ApartmentsForRenting  :apartmentData[] ,
        public Reservations :reservationData[] ,

    ){ }
}

