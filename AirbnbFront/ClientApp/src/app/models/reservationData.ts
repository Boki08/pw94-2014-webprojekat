import { appUserData } from './appUserData';
import { apartmentData } from './apartmentData';
import { statusData } from './statusData';


export class reservationData{
    constructor(
        public Id:number,
        public AppUserId :number,
        public ApartmentId  :number ,
        public StatusId  :number ,
        public Price  :number ,
        public NumOfNights :number,
        public Commented:boolean,
        public ReservationDate: Date,
        public AppUser :appUserData,
        public Apartment: apartmentData,
        public Status: statusData,
    ){ }
}


