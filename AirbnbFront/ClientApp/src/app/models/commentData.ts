import { apartmentData } from "./apartmentData";
import { reservationData } from "./reservationData";
import { appUserData } from './appUserData';
export class commentData{
    constructor(
        public Id:number,
        public ApartmentId :number,
        public AppUserId  :number ,
        public Review  :string ,
        public Grade  :number ,
        public Approved:boolean,
        public PostedDate  :Date ,
        public Apartments  :apartmentData[] ,
        public AppUsers :appUserData[] ,

    ){ }
}
