import { apartmentTypeData } from "./apartmentTypeData";
import { amenityData } from "./amenityData";
import { locationData } from "./locationData";
import { apartmentPictureData } from './apartmentPictureData';
import { rentingDate } from './rentingDate';
import { freeDate } from './freeDate';

export class apartmentData{
    constructor(
        public Id:number,
        public AppUserId :number,
        public ApartmentTypeId :number,
        public NumOfRooms  :number ,
        public NumOfGuests  :number ,
        public Price  :number ,
        public CheckInTime :Date ,
        public CheckOutTime :Date ,
        public Status :boolean ,
        public Grade :number ,
        public ApartmentType :apartmentTypeData ,
        public Amenities :amenityData[] ,
        public Pictures :apartmentPictureData[] ,
        public RentingDates :rentingDate[] ,
        public FreeDates :freeDate[] ,
        public Comments :Comment[] ,
        public Location :locationData[] ,
    ){ }
}


