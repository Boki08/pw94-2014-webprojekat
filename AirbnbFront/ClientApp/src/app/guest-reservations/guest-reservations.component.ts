﻿import { Component } from '@angular/core';
import { ToasterServiceComponent } from '../toaster-service/toaster-service.component';
import { btmNavDataService } from '../bottom-navbar/btmNavDataService';
import { ReservationServices } from '../services/reservationServices';
import { reservationData } from '../models/reservationData';
import { finalize } from 'rxjs/operators';
import { commentData } from '../models/commentData';
import { CommentServices } from '../services/commentServices';

@Component({
    selector: 'app-guest-reservations',
    templateUrl: './guest-reservations.component.html',
    styleUrls: ['./guest-reservations.component.css']
})
/** guest-reservations component*/
export class GuestReservationsComponent {
    /** guest-reservations ctor */
    showRadioButtons: boolean = false;
    showProgress: boolean;
    showReservations: boolean = false;
    showReservationsWarning: boolean = false;
    reservationProgress: boolean = true;
    showReservationData: boolean = false;
    showCancelReservation: boolean = false;

    pageIndex: number = 1;
    totalPagesNumber: number = 0;
    pageSize: number = 5;
    reservationCounter: number;
    selectedGrade: number=5;

    sortType: string = 'NoSort';

    reservations: reservationData[];
    reservationData: reservationData;
    constructor(private commentServices: CommentServices, private ReservationServices: ReservationServices, private toasterService: ToasterServiceComponent, private btmNavMessageService: btmNavDataService) {

    }
    ngOnInit() {
        this.btmNavMessageService.currentMessage.subscribe(message => this.showProgress = message)
        this.getAllGuestReservations(this.sortType);
    }
    getAllGuestReservations(sortType: string) {
        this.btmNavMessageService.changeMessage(true);
        /*  if (type == "Guest") { */
        this.ReservationServices.getAllUserReservations(this.pageIndex, this.pageSize, sortType).pipe(finalize(
            () => {
                this.btmNavMessageService.changeMessage(false);
            }))
            .subscribe(
                data => {

                    this.reservations = data.body as reservationData[];


                    let jsonData = JSON.parse(data.headers.get('Paging-Headers'));

                    this.pageIndex = jsonData.currentPage;
                    this.pageSize = jsonData.pageSize;
                    this.totalPagesNumber = jsonData.totalPages;
                    this.showReservations = true;
                    this.showReservationsWarning = false;

                },
                error => {
                    if (error.error.Message === "There are no reservations") {
                        this.showReservations = false;
                        this.showReservationsWarning = true;
                    }
                    else {
                        this.showReservationsWarning = false;
                        this.toasterService.Error(error.error.Message, 'Error');
                    }

                })
    }

    reservationDetails(reservation: reservationData, counter: number) {
        this.reservationData = reservation;
        this.reservationCounter = counter;

        this.reservationProgress = true;
        this.showReservationData = false;
        this.showCancelReservation = false;
        this.ReservationServices.getUserReservation(this.reservationData.Id).pipe(finalize(
            () => {
                this.reservationProgress = false;
            }))
            .subscribe(
                data => {

                    this.reservationData = data.body as reservationData;
                    if (this.reservationData.Status.Description == 'Created' || this.reservationData.Status.Description == 'Accepted') {
                        this.showCancelReservation = true;
                    }
                    else {
                        this.showCancelReservation = false;
                    }
                    //this.users[counter] = this.userData;

                    this.showReservationData = true;
                },
                error => {
                    this.showCancelReservation = false;
                    this.showReservationData = false;
                    this.toasterService.Error(error.error.Message, 'Error');
                })


    }
    CancelReservation() {
        this.reservationProgress = true;
        this.ReservationServices.cancelReservation(this.reservationData.Id).pipe(finalize(
            () => {
                this.reservationProgress = false;
            }))
            .subscribe(
                data => {

                    this.reservationData = data.body as reservationData;
                    this.reservations[this.reservationCounter] = this.reservationData;
                    if (this.reservationData.Status.Description == 'Created' || this.reservationData.Status.Description == 'Accepted') {
                        this.showCancelReservation = true;
                    }
                    else {
                        this.showCancelReservation = false;
                    }
                    //this.users[counter] = this.userData;

                },
                error => {
                    this.showCancelReservation = true;
                    this.toasterService.Error(error.error.Message, 'Error');
                })
    }
    onSubmit(comment: commentData) {
        this.reservationProgress = true;
        comment.Grade = Number(this.selectedGrade);
        comment.ApartmentId = this.reservationData.ApartmentId;
        comment.AppUserId = this.reservationData.AppUserId;

        this.commentServices.PostComment(this.reservationData.Id, comment).pipe(finalize(
            () => {
                this.reservationProgress = false;
            }))
            .subscribe(
                data => {
                    this.reservationData = data.body as reservationData;
                    this.reservations[this.reservationCounter] = this.reservationData;

                    this.toasterService.Info("Comment was posted", 'Info');
                },
                error => {
                    this.toasterService.Error(error.error.Message, 'Error');
                    console.log(error);
                })
    }
    GetSelectedGrade(grade: number) {
        this.selectedGrade = grade;
        //this.btnDisabled = false;
    }
    set page(val: number) {
        if (val !== this.pageIndex) {

            this.pageIndex = val;
            this.getAllGuestReservations(this.sortType);
        }
    }
    setRadioPrice(value: string): void {
        if (value == 'Low') {
            this.sortType = 'Low';
            this.getAllGuestReservations('Low');
        }
        else if (value == 'High') {
            this.sortType = 'High';
            this.getAllGuestReservations('High');
        }
        else {
            this.sortType = 'NoSort';
            this.getAllGuestReservations('NoSort');
        }
    }
}