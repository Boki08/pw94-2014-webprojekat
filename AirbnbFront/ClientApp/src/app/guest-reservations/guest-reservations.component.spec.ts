﻿/// <reference path="../../../../node_modules/@types/jasmine/index.d.ts" />
import { TestBed, async, ComponentFixture, ComponentFixtureAutoDetect } from '@angular/core/testing';
import { BrowserModule, By } from "@angular/platform-browser";
import { GuestReservationsComponent } from './guest-reservations.component';

let component: GuestReservationsComponent;
let fixture: ComponentFixture<GuestReservationsComponent>;

describe('guest-reservations component', () => {
    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [ GuestReservationsComponent ],
            imports: [ BrowserModule ],
            providers: [
                { provide: ComponentFixtureAutoDetect, useValue: true }
            ]
        });
        fixture = TestBed.createComponent(GuestReservationsComponent);
        component = fixture.componentInstance;
    }));

    it('should do something', async(() => {
        expect(true).toEqual(true);
    }));
});