﻿/// <reference path="../../../../node_modules/@types/jasmine/index.d.ts" />
import { TestBed, async, ComponentFixture, ComponentFixtureAutoDetect } from '@angular/core/testing';
import { BrowserModule, By } from "@angular/platform-browser";
import { HostReservationsComponent } from './host-reservations.component';

let component: HostReservationsComponent;
let fixture: ComponentFixture<HostReservationsComponent>;

describe('host-reservations component', () => {
    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [ HostReservationsComponent ],
            imports: [ BrowserModule ],
            providers: [
                { provide: ComponentFixtureAutoDetect, useValue: true }
            ]
        });
        fixture = TestBed.createComponent(HostReservationsComponent);
        component = fixture.componentInstance;
    }));

    it('should do something', async(() => {
        expect(true).toEqual(true);
    }));
});