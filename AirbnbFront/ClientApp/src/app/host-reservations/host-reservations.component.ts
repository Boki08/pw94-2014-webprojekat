﻿import { Component } from '@angular/core';
import { reservationData } from '../models/reservationData';
import { btmNavDataService } from '../bottom-navbar/btmNavDataService';
import { ToasterServiceComponent } from '../toaster-service/toaster-service.component';
import { ReservationServices } from '../services/reservationServices';
import { ReservationStatusServices } from '../services/reservationStatusServices';
import { finalize } from 'rxjs/operators';
import { statusData } from '../models/statusData';
import { dropdownData } from '../models/dropdownData';
import { FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
    selector: 'app-host-reservations',
    templateUrl: './host-reservations.component.html',
    styleUrls: ['./host-reservations.component.css']
})
/** host-reservations component*/
export class HostReservationsComponent {
    /** host-reservations ctor */

    showRadioButtons: boolean = false;
    showProgress: boolean;
    showReservations: boolean = false;
    showReservationsWarning: boolean = false;
    reservationProgress: boolean = true;
    showReservationData: boolean = false;
    dropdownVisible: boolean = false;
    //showAcceptReservation: boolean = false;
    showCompletedButton: boolean = false;

    pageIndex: number = 1;
    totalPagesNumber: number = 0;
    pageSize: number = 5;
    reservationCounter: number;
    stopNav: number = 0;

    sortType: string = 'NoSort';
    selectedUsername : string= "notSet";

    reservations: reservationData[];
    reservationData: reservationData;
    reservationStatuses: statusData[];
    filter: string[]=[];

    addSearchForm: FormGroup;
    UsernameControl: FormControl;

    dropdownListStatuses = [];
    selectedStatuses: dropdownData[] = [];
    dropdownStatusesSettings = {};
    constructor(private ReservationStatusServices: ReservationStatusServices, private ReservationServices: ReservationServices, private toasterService: ToasterServiceComponent, private btmNavMessageService: btmNavDataService) {

    }
    ngOnInit() {
        this.btmNavMessageService.currentMessage.subscribe(message => this.showProgress = message)
        this.getAllHostReservations(this.sortType);

        this.CreateFormControls();
        this.CreateForm();

        this.dropdownStatusesSettings = {
            singleSelection: false,
            idField: 'item_id',
            textField: 'item_text',
            selectAllText: 'Select All',
            unSelectAllText: 'UnSelect All',
            itemsShowLimit: 2,
            allowSearchFilter: true
        };

        this.ReservationStatusServices.GetReservationStatuses().pipe(finalize(
            () => {
                this.StopNav();
            }))
            .subscribe(
                data => {
                    this.reservationStatuses = data.body as statusData[];
                    for (let a of this.reservationStatuses) {
                        this.dropdownListStatuses.push({ item_id: a.Id, item_text: a.Description })
                    }
                    this.dropdownVisible = true;
                },
                error => {
                    this.toasterService.Error(error.error.Message, 'Error');
                })
    }
    CreateFormControls() {
        this.UsernameControl = new FormControl('', [
            Validators.maxLength(30),
        ]);


    }
    CreateForm() {
        this.addSearchForm = new FormGroup({
            UsernameControl: this.UsernameControl,
        });
    }

    GetReservations() {

        if (this.addSearchForm.value.UsernameControl == "") {
            this.selectedUsername = "notSet"
        }
        else {
            this.selectedUsername = this.addSearchForm.value.UsernameControl;
        }
        this.getAllHostReservations(this.sortType);
    }

    getAllHostReservations(sortType: string) {
        this.btmNavMessageService.changeMessage(true);

        this.filter = [];
        if (this.selectedStatuses.length > 0) {
            
            this.selectedStatuses.forEach(element => {
                this.filter.push(element.item_text);
            });
        }

        /*  if (type == "Guest") { */
        this.ReservationServices.getAllHostReservations(this.pageIndex, this.pageSize, sortType,this.selectedUsername , this.filter).pipe(finalize(
            () => {
                this.StopNav();
            }))
            .subscribe(
                data => {

                    this.reservations = data.body as reservationData[];


                    let jsonData = JSON.parse(data.headers.get('Paging-Headers'));

                    this.pageIndex = jsonData.currentPage;
                    this.pageSize = jsonData.pageSize;
                    this.totalPagesNumber = jsonData.totalPages;
                    this.showReservations = true;
                    this.showReservationsWarning = false;

                },
                error => {
                    if (error.error.Message === "There are no Reservaions") {
                        this.showReservations = false;
                        this.showReservationsWarning = true;
                    }
                    else {
                        this.showReservationsWarning = false;
                        this.toasterService.Error(error.error.Message, 'Error');
                    }

                })
    }

    reservationDetails(reservation: reservationData, counter: number) {
        this.reservationData = reservation;
        this.reservationCounter = counter;

        this.reservationProgress = true;
        this.showReservationData = false;
        this.showCompletedButton = false;
        this.ReservationServices.getHostReservation(this.reservationData.Id).pipe(finalize(
            () => {
                this.reservationProgress = false;
            }))
            .subscribe(
                data => {

                    this.reservationData = data.body as reservationData;

                    let reservationDate:Date= new Date(this.reservationData.ReservationDate);
                    let today:Date= new Date();
                    today.setHours(0,0,0,0);
                    reservationDate.setDate(reservationDate.getDate()+this.reservationData.NumOfNights)
                    if(today> reservationDate){
                        this.showCompletedButton = true;
                    }

                    this.showReservationData = true;
                },
                error => {

                    this.showReservationData = false;
                    this.toasterService.Error(error.error.Message, 'Error');
                })
    }
    AcceptReservation() {
        this.reservationProgress = true;


        this.ReservationServices.getAcceptRejectReservation(this.reservationData.Id, true).pipe(finalize(
            () => {
                this.reservationProgress = false;
            }))
            .subscribe(
                data => {

                    this.reservationData = data.body as reservationData;
                    this.reservations[this.reservationCounter]=this.reservationData;
                },
                error => {

                    this.toasterService.Error(error.error.Message, 'Error');
                })
    }
    RejectReservation() {
        this.reservationProgress = true;


        this.ReservationServices.getAcceptRejectReservation(this.reservationData.Id, false).pipe(finalize(
            () => {
                this.reservationProgress = false;
            }))
            .subscribe(
                data => {

                    this.reservationData = data.body as reservationData;
                    this.reservations[this.reservationCounter]=this.reservationData;
                },
                error => {

                    this.toasterService.Error(error.error.Message, 'Error');
                })
    }
    CompleteReservation(){
        this.reservationProgress = true;


        this.ReservationServices.getCompleteReservation(this.reservationData.Id).pipe(finalize(
            () => {
                this.reservationProgress = false;
            }))
            .subscribe(
                data => {

                    this.reservationData = data.body as reservationData;
                    this.reservations[this.reservationCounter]=this.reservationData;
                    this.showCompletedButton=false;
                },
                error => {

                    this.toasterService.Error(error.error.Message, 'Error');
                })
    }
    set page(val: number) {
        if (val !== this.pageIndex) {

            this.pageIndex = val;
            this.getAllHostReservations(this.sortType);
        }
    }
    setRadioPrice(value: string): void {
        if (value == 'Low') {
            this.sortType = 'Low';
            this.getAllHostReservations('Low');
        }
        else if (value == 'High') {
            this.sortType = 'High';
            this.getAllHostReservations('High');
        }
        else {
            this.sortType = 'NoSort';
            this.getAllHostReservations('NoSort');
        }
    }
    
    onSelectStatis(item: dropdownData) {
        this.getAllHostReservations(this.sortType);
    }
    onDeSelectStatus(item: dropdownData) {
        this.getAllHostReservations(this.sortType);
    }
    onSelectAllStatuses(items: dropdownData[]) {
        this.getAllHostReservations(this.sortType);
    }
    onDeSelectAllStatuses(items: dropdownData[]) {
        this.getAllHostReservations(this.sortType);
    }

    StopNav() {
        if (this.stopNav == 1) {
            this.btmNavMessageService.changeMessage(false);
            //this.dropdownVisible = true;

        }
        else if (this.stopNav < 1) {
            this.stopNav += 1;
        }
    }
}