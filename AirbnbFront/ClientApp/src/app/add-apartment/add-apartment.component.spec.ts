﻿/// <reference path="../../../../node_modules/@types/jasmine/index.d.ts" />
import { TestBed, async, ComponentFixture, ComponentFixtureAutoDetect } from '@angular/core/testing';
import { BrowserModule, By } from "@angular/platform-browser";
import { AddApartmentComponent } from './add-apartment.component';

let component: AddApartmentComponent;
let fixture: ComponentFixture<AddApartmentComponent>;

describe('add-apartment component', () => {
    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [ AddApartmentComponent ],
            imports: [ BrowserModule ],
            providers: [
                { provide: ComponentFixtureAutoDetect, useValue: true }
            ]
        });
        fixture = TestBed.createComponent(AddApartmentComponent);
        component = fixture.componentInstance;
    }));

    it('should do something', async(() => {
        expect(true).toEqual(true);
    }));
});