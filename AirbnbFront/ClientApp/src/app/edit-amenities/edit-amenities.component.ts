﻿import { Component } from '@angular/core';
import { amenityData } from '../models/amenityData';
import { ToasterServiceComponent } from '../toaster-service/toaster-service.component';
import { btmNavDataService } from '../bottom-navbar/btmNavDataService';
import { AmenityServices } from '../services/amenityServices';
import { finalize } from 'rxjs/operators';
import { FormGroup, FormControl, Validators, NgForm } from '@angular/forms';

@Component({
    selector: 'app-edit-amenities',
    templateUrl: './edit-amenities.component.html',
    styleUrls: ['./edit-amenities.component.css']
})
/** edit-amenities component*/
export class EditAmenitiesComponent {
    /** edit-amenities ctor */
    pageIndex: number = 1;
    totalPagesNumber: number = 0;
    pageSize: number = 5;
    amenityPosition: number;

    showAmenitiesWarning: boolean = false;
    showAmenities: boolean = false;
    showProgress: boolean;
    editFormVisible: boolean = false;
    addAmenityProgress: boolean = false;
    editAmenityProgress: boolean = false;

    addAmenityForm: FormGroup;
    editAmenityForm: FormGroup;
    Type: FormControl;
    EditType: FormControl;

    amenities: amenityData[];
    selectedAmenity: amenityData;
    constructor(private AmenityServices: AmenityServices, private toasterService: ToasterServiceComponent, private btmNavMessageService: btmNavDataService) {

    }
    ngOnInit() {
        this.btmNavMessageService.currentMessage.subscribe(message => this.showProgress = message)
        this.btmNavMessageService.changeMessage(true);

        this.CreateFormControls();
        this.CreateForm();
        this.GetAllAmenities();

    }
    GetAllAmenities() {
        this.showAmenities = false;
        this.AmenityServices.GetAllAmenitiesAdmin(this.pageIndex, this.pageSize).pipe(finalize(
            () => {
                if(this.amenities.length==0){
                    this.showAmenitiesWarning = true;
                }
                else{
                    this.showAmenitiesWarning = false;
                }
                this.btmNavMessageService.changeMessage(false);
            }))
            .subscribe(
                data => {
                    this.amenities = data.body as amenityData[];
                    

                    let jsonData = JSON.parse(data.headers.get('Paging-Headers'));

                    this.pageIndex = jsonData.currentPage;
                    this.pageSize = jsonData.pageSize;
                    this.totalPagesNumber = jsonData.totalPages;

                    this.showAmenities = true;
                },
                error => {
                    this.toasterService.Error(error.error.Message, 'Error');
                    
                })
    }
    ngOnDestroy() {
        this.btmNavMessageService.changeMessage(false);
    }
    CreateFormControls() {
        this.Type = new FormControl('', [
            Validators.required,
            Validators.maxLength(20),
        ]);
        this.EditType = new FormControl('', [
            Validators.required,
            Validators.maxLength(20),
        ]);
    }
    CreateForm() {
        this.addAmenityForm = new FormGroup({
            Type: this.Type,
        });
        this.editAmenityForm = new FormGroup({
            EditType: this.EditType,
        });
    }

    editAmenity(amenity: amenityData,amenityPosition:number) {
        this.selectedAmenity = amenity;
        this.amenityPosition=amenityPosition;
        this.editFormVisible=true;
    }
    deleteamenity(amenity: amenityData) {
        this.AmenityServices.DeleteAmenity(amenity.Id).pipe(finalize(
            () => {
                this.btmNavMessageService.changeMessage(false);
            }))
            .subscribe(
                data => {
                    this.toasterService.Success("Amenity deleted", 'Success');
                    this.GetAllAmenities();
                },
                error => {
                    this.toasterService.Error(error.error.Message, 'Error');
                })
    }

    onSubmit(amenity: amenityData, form: NgForm) {
        this.addAmenityProgress=true;
        this.AmenityServices.AddAmenity(amenity).pipe(finalize(
            () => {
                this.addAmenityProgress=false;
            }))
            .subscribe(
                data => {
                    if (this.amenities.length < this.pageSize) {
                        this.amenities.push(data.body as amenityData);
                    }

                    this.toasterService.Success("Amenity added", 'Success');

                },
                error => {
                    this.toasterService.Error(error.error.Message, 'Error');
                })
    }
    onEditSubmit(amenity: any, form: NgForm) {
        this.editAmenityProgress=true;
        let editedAmenity:amenityData= new amenityData(this.selectedAmenity.Id,amenity.EditType,null);
        this.AmenityServices.EditAmenity(editedAmenity).pipe(finalize(
            () => {
                this.editAmenityProgress=false;
            }))
            .subscribe(
                data => {
                    form.reset();
                    this.amenities[this.amenityPosition]=data.body as amenityData;
                    this.toasterService.Success("Amenity edited", 'Success');

                },
                error => {
                    this.toasterService.Error(error.error.Message, 'Error');
                })
    }
    set page(val: number) {
        if (val !== this.pageIndex) {
            this.pageIndex = val;
            this.GetAllAmenities();
        }
    }

}