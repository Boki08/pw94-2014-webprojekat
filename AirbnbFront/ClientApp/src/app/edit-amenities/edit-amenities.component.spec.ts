﻿/// <reference path="../../../../node_modules/@types/jasmine/index.d.ts" />
import { TestBed, async, ComponentFixture, ComponentFixtureAutoDetect } from '@angular/core/testing';
import { BrowserModule, By } from "@angular/platform-browser";
import { EditAmenitiesComponent } from './edit-amenities.component';

let component: EditAmenitiesComponent;
let fixture: ComponentFixture<EditAmenitiesComponent>;

describe('edit-amenities component', () => {
    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [ EditAmenitiesComponent ],
            imports: [ BrowserModule ],
            providers: [
                { provide: ComponentFixtureAutoDetect, useValue: true }
            ]
        });
        fixture = TestBed.createComponent(EditAmenitiesComponent);
        component = fixture.componentInstance;
    }));

    it('should do something', async(() => {
        expect(true).toEqual(true);
    }));
});