﻿import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {

  constructor() { }
  pageName:string;
  link1: string;
  link2: string;
  link3: string;
  link4: string;
  route1: string;
  route2: string;
  route3: string;
  route4: string;
  userNav: boolean;

  link1Visible: boolean=false;
  link2Visible: boolean=false;
  link3Visible: boolean=false;
  link4Visible: boolean=false;
  ngOnInit() {
    this.loggedOut();
  }
  loggedOut() {
    if (localStorage.jwt && localStorage.role == "Admin") {
      
      
      this.pageName="Admin Services";

      this.link1 = "Reservations";
      this.route1 = "/adminReservations";
      this.link2 = "Users";
      this.route2 = "/adminUsers";
      this.link3 = "All Apartments";
      this.route3 = "/adminApartment";
      this.link4 = "Edit Amenities";
      this.route4 = "/editAmenities";
      

      this.link1Visible=true;
      this.link2Visible=true;
      this.link3Visible=true;
      this.link4Visible=true;
    }
    else  if (localStorage.jwt && localStorage.role == "Host") {
      this.pageName="Host Services";

      this.link1 = "Active Apartments";
      this.route1 = "/apartments";
      this.link2 = "All Apartments";
      this.route2 = "/hostApartments";
      this.link3 = "Add Apartment";
      this.route3 = "/addApartment";
      this.link4 = "Reservations";
      this.route4 = "/hostReservations";
      

      this.link1Visible=true;
      this.link2Visible=true;
      this.link3Visible=true;
      this.link4Visible=true;
    }
    else  if (localStorage.jwt && localStorage.role == "Guest") {

      this.pageName="Guest Services";

      this.link1 = "Apartments";
      this.route1 = "/apartments";
      this.link2 = "Reservations";
      this.route2 = "/guestReservations";
      this.link1Visible=true;
      this.link2Visible=true;
      this.link3Visible=false;
      this.link4Visible=false;
      
    }
    else {

      this.link1 = "Apartments";
      this.route1 = "/apartments";

      this.link1Visible=true;
      this.link2Visible=false;
      this.link3Visible=false;
      this.link4Visible=false;

      this.pageName="Visitor Services";
    }
  }


}
