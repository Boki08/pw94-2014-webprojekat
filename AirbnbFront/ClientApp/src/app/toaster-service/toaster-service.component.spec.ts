﻿/// <reference path="../../../node_modules/@types/jasmine/index.d.ts" />
import { TestBed, async, ComponentFixture, ComponentFixtureAutoDetect } from '@angular/core/testing';
import { BrowserModule, By } from "@angular/platform-browser";
import { ToasterServiceComponent } from './toaster-service.component';

let component: ToasterServiceComponent;
let fixture: ComponentFixture<ToasterServiceComponent>;

describe('toaster-service component', () => {
    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [ ToasterServiceComponent ],
            imports: [ BrowserModule ],
            providers: [
                { provide: ComponentFixtureAutoDetect, useValue: true }
            ]
        });
        fixture = TestBed.createComponent(ToasterServiceComponent);
        component = fixture.componentInstance;
    }));

    it('should do something', async(() => {
        expect(true).toEqual(true);
    }));
});