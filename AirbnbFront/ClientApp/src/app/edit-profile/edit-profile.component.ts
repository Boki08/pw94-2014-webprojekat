﻿import { Component } from '@angular/core';
import { appUserData } from '../models/appUserData';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { UserServices } from '../services/userServices';
import { btmNavDataService } from '../bottom-navbar/btmNavDataService';
import { ToasterServiceComponent } from '../toaster-service/toaster-service.component';
import { finalize } from 'rxjs/operators';

@Component({
    selector: 'app-edit-profile',
    templateUrl: './edit-profile.component.html',
    styleUrls: ['./edit-profile.component.css']
})
/** edit-profile component*/
export class EditProfileComponent {

    /** edit-profile ctor */
    constructor(private userServices: UserServices, private toasterService: ToasterServiceComponent, private btmNavMessageService: btmNavDataService) {

    }

    userData: appUserData = new appUserData(0, 'UserName', 'Name', 'Surname', 'Gender', null, null);

    showProgress: boolean = false;
    btnDisabled: boolean = true;

    editForm: FormGroup;
    UserName: FormControl;
    Name: FormControl;
    Surname: FormControl;
    Gender: FormControl;

    ngOnInit() {
        this.CreateFormControls();
        this.CreateForm();

        this.btmNavMessageService.currentMessage.subscribe(message => this.showProgress = message)
        this.btmNavMessageService.changeMessage(true);

        this.getUser();
    }
    getUser() {
        this.userServices.getProfile().pipe(finalize(
            () => {
                this.btmNavMessageService.changeMessage(false);
            }))
            .subscribe(
                data => {
                    this.userData = data.body;
                    this.btnDisabled = false;

                },
                error => {
                    this.toasterService.Error(error.error.Message, 'Error');
                }
            )
    }
    CreateFormControls() {
        this.UserName = new FormControl('', [
            Validators.maxLength(50),
            Validators.minLength(5),
            Validators.required
        ]);
        this.Name = new FormControl('', [
            Validators.maxLength(50),
            Validators.minLength(1),
            Validators.required
        ]);
        this.Surname = new FormControl('', [
            Validators.maxLength(50),
            Validators.minLength(1),
            Validators.required
        ]);
        this.Gender = new FormControl('', [
            Validators.maxLength(10),
            Validators.minLength(1),
            Validators.required
        ]);

    }
    CreateForm() {
        this.editForm = new FormGroup({
            UserName: this.UserName,
            Name: this.Name,
            Surname: this.Surname,
            Gender: this.Gender,
        });
    }


    onSubmit(appUser: any) {
        console.log(appUser);
        this.btmNavMessageService.changeMessage(true);


        appUser.Id = this.userData.Id;
        this.userServices.EditUser(appUser).pipe(finalize(
            () => {
                this.btmNavMessageService.changeMessage(false);

            }))
            .subscribe(
                data => {

                    this.userData = data.body;
                    this.toasterService.Info("Your changes updated successfully", 'Info');

                },
                error => {

                        this.toasterService.Error(error.error.Message, 'Error');
                    

                }
            );
    }
}