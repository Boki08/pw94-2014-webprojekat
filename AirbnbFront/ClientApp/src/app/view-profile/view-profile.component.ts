﻿import { Component, Input, OnInit } from '@angular/core';
import { btmNavDataService } from '../bottom-navbar/btmNavDataService';
import { UserServices } from '../services/userServices';
import { appUserData } from '../models/appUserData';
import { finalize } from 'rxjs/operators';
import { ToasterServiceComponent } from '../toaster-service/toaster-service.component';

@Component({
    selector: 'app-view-profile',
    templateUrl: './view-profile.component.html',
    styleUrls: ['./view-profile.component.css']
})
/** view-profile component*/
export class ViewProfileComponent implements OnInit {

    constructor(private toasterService:ToasterServiceComponent,private btmNavMessageService: btmNavDataService, private userServices: UserServices) {
      
    }
    @Input()
    userData: appUserData;
    showProgress: boolean = true;
  
  
    ngOnInit() {
  
      this.btmNavMessageService.currentMessage.subscribe(message => this.showProgress = message)
  
      this.btmNavMessageService.changeMessage(true);
  
      this.userServices.getProfile().pipe(finalize(
        () => {
          this.btmNavMessageService.changeMessage(false);
        }))
        .subscribe(
          data => {
           
              this.userData = data.body;
            
          },
          error => {
           this.toasterService.Error(error.error.Message,'Error');
          }
        )
    }
    ngOnDestroy() {
      this.btmNavMessageService.changeMessage(false);
    }
    
  }
  