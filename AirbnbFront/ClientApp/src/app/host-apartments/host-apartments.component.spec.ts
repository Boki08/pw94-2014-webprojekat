﻿/// <reference path="../../../../node_modules/@types/jasmine/index.d.ts" />
import { TestBed, async, ComponentFixture, ComponentFixtureAutoDetect } from '@angular/core/testing';
import { BrowserModule, By } from "@angular/platform-browser";
import { HostApartmentsComponent } from './host-apartments.component';

let component: HostApartmentsComponent;
let fixture: ComponentFixture<HostApartmentsComponent>;

describe('host-apartments component', () => {
    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [ HostApartmentsComponent ],
            imports: [ BrowserModule ],
            providers: [
                { provide: ComponentFixtureAutoDetect, useValue: true }
            ]
        });
        fixture = TestBed.createComponent(HostApartmentsComponent);
        component = fixture.componentInstance;
    }));

    it('should do something', async(() => {
        expect(true).toEqual(true);
    }));
});