﻿import { Component } from '@angular/core';
import { ToasterServiceComponent } from '../toaster-service/toaster-service.component';
import { btmNavDataService } from '../bottom-navbar/btmNavDataService';
import { apartmentData } from '../models/apartmentData';
import { apartmentPictureData } from '../models/apartmentPictureData';
import { ApartmentServices } from '../services/apartmentServices';
import { finalize } from 'rxjs/operators';
import { SafeUrl, DomSanitizer } from '@angular/platform-browser';
import { Options, ChangeContext } from 'ng5-slider';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { NgbDate, NgbCalendar } from '@ng-bootstrap/ng-bootstrap';
import { dropdownData } from '../models/dropdownData';
import { amenityData } from '../models/amenityData';
import { apartmentTypeData } from '../models/apartmentTypeData';
import { ApartmentTypeServices } from '../services/apartmentTypeServices';
import { AmenityServices } from '../services/amenityServices';

@Component({
    selector: 'app-host-apartments',
    templateUrl: './host-apartments.component.html',
    styleUrls: ['./host-apartments.component.css']
})
/** host-apartments component*/
export class HostApartmentsComponent {
    /** host-apartments ctor */
    showProgress: boolean;
    showApartmentProgress: boolean = false;
    showApartmentProgressPic: boolean = false;
    showApartmentsWarning: boolean = false;
    showApartments: boolean = false;
    dropdownVisible: boolean = false;
    searchChecked: boolean = false;

    pageSize: number = 5;
    pageIndex: number = 1;
    totalPagesNumber: number = 1;

    apartments: apartmentData[];
    apartment: apartmentData;
    apartmentPictures: apartmentPictureData[];

    img: Array<SafeUrl> = [];





    minPriceValue: number = 0;
    maxPriceValue: number = 400;
    PriceSlideOptions: Options = {
        floor: 0,
        ceil: 500,
        translate: (value: number): string => {
            return '€' + value;
        }
    };
    numOfGuests: number = 1;
    GuestSlideOptions: Options = {
        floor: 1,
        ceil: 10,
    };

    numOfRooms: number = 1;
    RoomSlideOptions: Options = {
        floor: 1,
        ceil: 20,
    };

    addSearchForm: FormGroup;
    City: FormControl;


    hoveredDate: NgbDate;

    fromDate: NgbDate;
    todayDate: NgbDate;
    fromDateString: string;
    toDate: NgbDate;
    toDateString: string;
    dateString: string;



    amenityFilterF: number[] = [];
    typeFilterF: number[] = [];
    statusFilterF: string;

    dropdownApartmentStatuses = [];
    selectedApartmentStatus: dropdownData[] = [];
    dropdownApartmentStatusesSettings = {};

    dropdownListAmenities = [];
    selectedAmenities: dropdownData[] = [];
    dropdownAmenitiesSettings = {};
    amenitiesTemp: amenityData[];

    dropdownPricesSort = [];
    selectedPriceSort: dropdownData[] = [];
    selectedPriceSortTemp: dropdownData;
    dropdownPricesSortSettings = {};

    dropdownApartmentTypes = [];
    selectedApartmentType: dropdownData[] = [];
    apartmentTypesTemp: apartmentTypeData[];
    dropdownApartmentTypesSettings = {};

    stopNav: number = 0;

    constructor(private amenityServices: AmenityServices, private apartmentTypeServices: ApartmentTypeServices, private calendar: NgbCalendar, private sanitizer: DomSanitizer, private apartmentService: ApartmentServices, private toasterService: ToasterServiceComponent, private btmNavMessageService: btmNavDataService) {
        this.fromDate = this.calendar.getToday();
        this.todayDate = this.calendar.getToday();

        this.toDate = this.calendar.getNext(calendar.getToday(), 'd', 10);
    }
    ngOnInit() {
        this.btmNavMessageService.currentMessage.subscribe(message => this.showProgress = message)

        this.CreateFormControls();
        this.CreateForm();

        this.apartmentTypeServices.GetApartmentTypes().pipe(finalize(
            () => {
                this.StopNav();
            }))
            .subscribe(
                data => {
                    this.apartmentTypesTemp = data.body as apartmentTypeData[];
                    //this.apartmentTypesTemp.push(new apartmentTypeData(-1, "All"))
                    for (let a of this.apartmentTypesTemp) {
                        this.dropdownApartmentTypes.push({ item_id: a.Id, item_text: a.Type })
                    }

                },
                error => {
                    if (error.error.Message == undefined) {
                        this.toasterService.Error("Undefined Error", 'Error');
                    }
                    else {
                        this.toasterService.Error(error.error.Message, 'Error');
                    }
                }
            );

        this.amenityServices.GetAllAmenities().pipe(finalize(
            () => {
                this.StopNav();
            }))
            .subscribe(
                data => {
                    this.amenitiesTemp = data.body as amenityData[];
                    //this.apartmentTypesTemp.push(new apartmentTypeData(-1, "All"))
                    for (let a of this.amenitiesTemp) {
                        this.dropdownListAmenities.push({ item_id: a.Id, item_text: a.Type })
                    }

                },
                error => {
                    if (error.error.Message == undefined) {
                        this.toasterService.Error("Undefined Error", 'Error');
                    }
                    else {
                        this.toasterService.Error(error.error.Message, 'Error');
                    }
                }
            );

        this.selectedPriceSortTemp = new dropdownData('Mixed', 'Price - Mixed');
        this.GetAllApartmentsSearch();





        this.toDateString = this.toDate.day + "/" + this.toDate.month + "/" + this.toDate.year;
        this.fromDateString = this.fromDate.day + "/" + this.fromDate.month + "/" + this.fromDate.year;
        this.dateString = this.fromDateString + " - " + this.toDateString;



        this.dropdownApartmentStatusesSettings = {
            singleSelection: true,
            idField: 'item_id',
            textField: 'item_text',

            allowSearchFilter: false
        };

        this.dropdownApartmentStatuses = [
            new dropdownData('Enabled', 'Enabled '),
            new dropdownData('Disabled', 'Disabled '),
            
        ];

        this.dropdownAmenitiesSettings = {
            singleSelection: false,
            idField: 'item_id',
            textField: 'item_text',
            selectAllText: 'Select All',
            unSelectAllText: 'UnSelect All',
            itemsShowLimit: 1,
            allowSearchFilter: true
        };

        this.dropdownPricesSort = [
            new dropdownData('Low', 'Low to High'),
            new dropdownData('High', 'High to Low'),
            new dropdownData('Mixed', 'Mixed'),
        ];


        this.dropdownPricesSortSettings = {
            singleSelection: true,
            idField: 'item_id',
            textField: 'item_text',

            allowSearchFilter: false
        };


        this.dropdownApartmentTypesSettings = {
            singleSelection: true,
            idField: 'item_id',
            textField: 'item_text',

            allowSearchFilter: false
        };
    }
    ngOnDestroy() {
        this.btmNavMessageService.changeMessage(false);
    }
    onSelectApartmentStatus(item: dropdownData) {
        this.statusFilterF=item.item_id;
        this.GetAllApartmentsSearch();

    }

    onDeSelectApartmentStatus(item: dropdownData) {
        this.statusFilterF="NoFilter";
        this.GetAllApartmentsSearch();

    }
    onSelectApartmentType(item: dropdownData) {
        this.GetAllApartmentsSearch();

    }

    onDeSelectApartmentType(item: dropdownData) {
        this.GetAllApartmentsSearch();

    }
    onSelectPriceSort(item: dropdownData) {
        this.selectedPriceSortTemp = item;
        this.GetAllApartmentsSearch();
    }
    onDeSelectPriceSort(item: dropdownData) {
        this.selectedPriceSortTemp = new dropdownData('Mixed', 'Price - Mixed');
        this.GetAllApartmentsSearch();
    }

    onSelectAllAmenities(item: dropdownData[]) {//  
        this.selectedAmenities = item;
        this.GetAllApartmentsSearch();

    }
    onSelectAmenity(item: dropdownData) {// 
        this.GetAllApartmentsSearch();
    }

    onDeSelectAmenity(item: dropdownData) {
        this.GetAllApartmentsSearch();
    }
    onDeSelectAllAmenities(item: dropdownData) {//
        this.selectedAmenities = [];
        this.GetAllApartmentsSearch();
    }

    GetNumOfRooms(changeContext: ChangeContext): void {
        this.numOfRooms;
    }
    GetNumOfGuests(changeContext: ChangeContext): void {
        this.numOfGuests;
    }
    GetNewPriceRange(changeContext: ChangeContext): void {
        this.minPriceValue;
        this.maxPriceValue;
    }

    onDateSelection(date: NgbDate) {
        if (!this.fromDate && !this.toDate) {
            this.fromDate = date;
            this.fromDateString = date.day + "/" + date.month + "/" + date.year;
            this.dateString = this.fromDateString + " - " + this.toDateString;
        } else if (this.fromDate && !this.toDate && date.after(this.fromDate)) {
            this.toDate = date;
            this.toDateString = date.day + "/" + date.month + "/" + date.year;
            this.dateString = this.fromDateString + " - " + this.toDateString;
        } else {
            this.toDate = null;
            this.fromDate = date;
            this.fromDateString = date.day + "/" + date.month + "/" + date.year;
            this.dateString = this.fromDateString + " - " + this.toDateString;
        }
    }

    isHovered(date: NgbDate) {
        return this.fromDate && !this.toDate && this.hoveredDate && date.after(this.fromDate) && date.before(this.hoveredDate);
    }

    isInside(date: NgbDate) {
        return date.after(this.fromDate) && date.before(this.toDate);
    }

    isRange(date: NgbDate) {
        return date.equals(this.fromDate) || date.equals(this.toDate) || this.isInside(date) || this.isHovered(date);
    }

    CreateFormControls() {
        this.City = new FormControl('', [
            Validators.maxLength(30),
        ]);


    }
    CreateForm() {
        this.addSearchForm = new FormGroup({
            City: this.City,
        });
    }
    public GetAllApartmentsSearch() {
        this.btmNavMessageService.changeMessage(true);
        let toDate = new Date(this.toDate.year, this.toDate.month - 1, this.toDate.day).toISOString();
        let fromDate = new Date(this.fromDate.year, this.fromDate.month - 1, this.fromDate.day).toISOString();
        let city: string;
        if (this.addSearchForm.value.City == "") {
            city = "noCity"
        }
        else {
            city = this.addSearchForm.value.City;
        }

        this.amenityFilterF = [];
        if (this.selectedAmenities.length > 0) {

            this.selectedAmenities.forEach(element => {
                this.amenityFilterF.push(element.item_id);
            });
        }
        this.typeFilterF = [];
        if (this.selectedApartmentType.length > 0) {

            this.selectedApartmentType.forEach(element => {
                this.typeFilterF.push(element.item_id);
            });
        }
        

        this.apartmentService.GetAllApartmentsHostSearch(this.pageIndex, this.pageSize,this.searchChecked, this.statusFilterF, this.amenityFilterF, this.typeFilterF, city, this.selectedPriceSortTemp.item_id, fromDate, toDate, this.minPriceValue, this.maxPriceValue, this.numOfGuests, this.numOfRooms).pipe(finalize(

            () => {

                this.StopNav();
            }))
            .subscribe(
                data => {
                    this.apartments = data.body as apartmentData[];

                    let jsonData = JSON.parse(data.headers.get('Paging-Headers'));

                    this.pageIndex = jsonData.currentPage;
                    this.pageSize = jsonData.pageSize;
                    this.totalPagesNumber = jsonData.totalPages;
                    this.showApartments = true;
                    this.showApartmentsWarning = false;

                },
                error => {

                    if (error.error.Message === 'There are no Apartments') {

                        this.showApartmentsWarning = true;
                        this.toasterService.Warning(error.error.Message, 'Warning');
                    }
                    else {
                        this.showApartmentsWarning = false;
                        this.toasterService.Error(error.error.Message, 'Error');
                    }
                    this.showApartments = false;
                })
    }
    deleteApartment(apartment:apartmentData){
        this.btmNavMessageService.changeMessage(true);
        this.apartmentService.deleteApartment(apartment.Id,apartment.AppUserId).pipe(finalize(

            () => {

                this.StopNav();
            }))
            .subscribe(
                data => {
                    this.GetAllApartmentsSearch();
                    this.toasterService.Success("Apartment was deleted", 'Success');
                },
                error => {

                        this.toasterService.Error(error.error.Message, 'Error');

                })
    }
    apartmentDetails(apartment: apartmentData, counter: number) {
        this.apartment = apartment;
       

        this.showApartmentProgressPic = true;
        this.showApartmentProgress = true;
        this.apartmentService.getApartmentInfoHost(this.apartment.Id).pipe(finalize(
            () => {

            }))
            .subscribe(
                data => {

                    this.apartment = data.body;
                    this.apartmentPictures = this.apartment.Pictures;
                   
                    this.showApartmentProgress = false;

                    let count = 0;
                    for (let i = 0; i < this.apartmentPictures.length; i++) {
                        this.apartmentService.getImage(this.apartmentPictures[i].Data).pipe(finalize(
                            () => {
                                count += 1;
                                if (count == this.apartmentPictures.length) {
                                    this.showApartmentProgressPic = false;
                                }
                            }))
                            .subscribe(
                                data => {
                                    this.createImageFromBlob(data, i);
                                },
                                error => {
                                    this.toasterService.Error(error.error.Message, 'Error');
                                })
                    }

                },
                error => {

                    this.toasterService.Error(error.error.Message, 'Error');

                })
    }
    createImageFromBlob(image: Blob, counter: number) {
        let reader = new FileReader();
        reader.addEventListener("load", () => {
            this.img.push(this.sanitizer.bypassSecurityTrustResourceUrl(reader.result as string));
        }, false);

        if (image) {
            reader.readAsDataURL(image);
        }
    }
    StopNav() {
        if (this.stopNav == 2) {
            this.btmNavMessageService.changeMessage(false);
            this.dropdownVisible = true;

        }
        else if (this.stopNav < 2) {
            this.stopNav += 1;
        }
    }
    set page(val: number) {
        if (val !== this.pageIndex) {

            this.pageIndex = val;
            this.GetAllApartmentsSearch();
        }
    }
    setCheckBoxHost(event) {

        if (event.target.checked) {
          this.searchChecked = true;
        }
        else {
          this.searchChecked = false;
          this.GetAllApartmentsSearch();
        }
      }
    
}