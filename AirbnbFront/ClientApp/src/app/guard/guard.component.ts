﻿import { Injectable } from '@angular/core';
import { CanActivate } from '@angular/router';

@Injectable()
export class AdminHostGuard implements CanActivate {

  constructor() {}

  canActivate() {
    return (localStorage.role == "Admin" || localStorage.role == "Host");
  }
  
}