﻿import { Component, OnInit } from '@angular/core';
import { ToasterServiceComponent } from '../toaster-service/toaster-service.component';
import { btmNavDataService } from '../bottom-navbar/btmNavDataService';
import { UserServices } from '../services/userServices';
import { FormGroup, FormControl, Validators, NgForm } from '@angular/forms';
import { PasswordValidator } from './password-validator';
import { finalize } from 'rxjs/operators';
import { dropdownData } from '../models/dropdownData';
import { registerData } from '../models/registerData';

@Component({
    selector: 'app-register',
    templateUrl: './register.component.html',
    styleUrls: ['./register.component.css']
})
/** register component*/
export class RegisterComponent implements OnInit {
 


    constructor(private toasterService:ToasterServiceComponent,private btmNavMessageService: btmNavDataService, private UserService: UserServices) 
      {}
  
    regForm: FormGroup;
    Matching_passwords_group: FormGroup;
    Username: FormControl;
    Name: FormControl;
    Surname: FormControl;
    Password: FormControl;
    RepeatedPassword: FormControl;
    Gender: FormControl;

    dropdownListGender = [];
    selectedGender: dropdownData[] = [];
    dropdownGenderSettings = {};

   
    showProgress: boolean = false;
    
  
    ngOnInit() {
      this.btmNavMessageService.currentMessage.subscribe(message => this.showProgress = message)
      this.CreateFormControls();
      this.CreateForm();
      this.dropdownListGender = [
        new dropdownData('female', 'Female'),
        new dropdownData('male', 'Male'),
    ];
    //this.selectedGender = this.dropdownListGender[1];

    this.dropdownGenderSettings = {
        singleSelection: true,
        idField: 'item_id',
        textField: 'item_text',
        allowSearchFilter: false
    };

      
      this.btmNavMessageService.changeMessage(true);
      this.btmNavMessageService.changeMessage(false);
    }
    CreateFormControls() {
      this.Username = new FormControl('', [
        Validators.maxLength(30),
        Validators.minLength(5),
        Validators.required
      ]);
      this.Password = new FormControl('', [
        Validators.maxLength(30),
        Validators.minLength(6),
        Validators.required
      ]);
      this.RepeatedPassword = new FormControl('', Validators.required);
  
      this.Matching_passwords_group = new FormGroup({
        Password: this.Password,
        RepeatedPassword: this.RepeatedPassword,
      }, {
          validators: PasswordValidator.MatchPassword
        });
     
      this.Name = new FormControl('', [
        Validators.maxLength(30),
        Validators.minLength(1),
        Validators.required
    ]);
    this.Surname = new FormControl('', [
        Validators.maxLength(30),
        Validators.minLength(1),
        Validators.required
    ]);
    this.Gender = new FormControl('', [
        Validators.required
    ]);
    }
    CreateForm() {
      this.regForm = new FormGroup({
        Username: this.Username,
  
        Matching_passwords_group: this.Matching_passwords_group,
        Name: this.Name,
        Surname: this.Surname,
        Gender:this.Gender,
      });
    }
    onSubmit(user: any, form: NgForm) {
      this.btmNavMessageService.changeMessage(true);

      let newUser: registerData = new registerData(user.Name, user.Surname, user.Gender[0].item_id, user.Username, user.Matching_passwords_group.Password, user.Matching_passwords_group.RepeatedPassword);
     
      this.UserService.register(newUser) .pipe(finalize(
        () => {
          this.btmNavMessageService.changeMessage(false);
        }))
      .subscribe(
        data => {
          this.toasterService.Info("Registration was successfull",'Info');
        },
        error => {
          this.toasterService.Error(error.error.Message,'Error');
        })
      form.reset();
    }
  
  }
  