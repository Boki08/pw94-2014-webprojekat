import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';


import { AppComponent } from './app.component';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { RouterModule, Routes } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';
import { HttpClientXsrfModule } from '@angular/common/http';
import { Ng5SliderModule } from 'ng5-slider';
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ApartmentPageComponent } from './apartment-page/apartment-page.component';
import {FileSelectDirective,FileDropDirective } from "ng2-file-upload";

import { TokenInterceptor } from './interceptors/interceptors.component';
import { AdminHostGuard } from './guard/guard.component';
import { BottomNavbarComponent } from './bottom-navbar/bottom-navbar.component';
import { btmNavDataService } from './bottom-navbar/btmNavDataService';
import { ToasterServiceComponent } from './toaster-service/toaster-service.component';
import { ApartmentsComponent } from './apartments/apartments.component';
import { NavbarComponent } from './navbar/navbar.component';
import { PagerComponent } from './pager/pager.component';
import { LogInComponent } from './log-in/log-in.component';
import { ViewProfileComponent } from './view-profile/view-profile.component';
import { EditProfileComponent } from './edit-profile/edit-profile.component';
import { ChangePasswordComponent } from './change-password/change-password.component';
import { AdminUsersComponent } from './admin-users/admin-users.component';
import { AddApartmentComponent } from './add-apartment/add-apartment.component';
import { EditApartmentComponent } from './edit-apartment/edit-apartment.component';
import { HostApartmentsComponent } from './host-apartments/host-apartments.component';
import { RentApartmentComponent } from './rent-apartment/rent-apartment.component';
import { GuestReservationsComponent } from './guest-reservations/guest-reservations.component';
import { HostReservationsComponent } from './host-reservations/host-reservations.component';
import { AdminApartmentComponent } from './admin-apartment/admin-apartment.component';
import { EditAmenitiesComponent } from './edit-amenities/edit-amenities.component';
import { AdminReservationsComponent } from './admin-reservations/admin-reservations.component';
import { RegisterComponent } from './register/register.component';




const Routes = [
  {
    path: "",
    redirectTo: "apartments",
    pathMatch: "full"
  },
  {
    path: "apartments",
    component: ApartmentsComponent
  },
  {
    path: "apartmentPage/:apartmentId",
    component: ApartmentPageComponent
  },
  {
    path: "viewProfile",
    component: ViewProfileComponent,
    canActivate: ['IsLoggedInGuard']
  },{
    path: "editUser",
    component: EditProfileComponent,
    canActivate: ['IsLoggedInGuard']
  },
  {
    path: "changePassword",
    component: ChangePasswordComponent,
    canActivate: ['IsLoggedInGuard']
  },
  {
    path: "adminUsers",
    component: AdminUsersComponent,
    canActivate: ['IsAdminGuard']
  },
  {
    path: "addApartment",
    component: AddApartmentComponent,
    canActivate: ['IsHostGuard']
  },
  {
    path: "hostApartments",
    component: HostApartmentsComponent,
    canActivate: ['IsHostGuard']
  },
  {
    path: "guestReservations",
    component: GuestReservationsComponent,
    canActivate: ['IsGuestGuard']
  },
  {
    path: "hostReservations",
    component: HostReservationsComponent,
    canActivate: ['IsHostGuard']
  },
  {
    path: "adminApartment",
    component: AdminApartmentComponent,
    canActivate: ['IsAdminGuard']
  },
  {
    path: "editAmenities",
    component: EditAmenitiesComponent,
    canActivate: ['IsAdminGuard']
  },
  
  {
    path: "adminReservations",
    component: AdminReservationsComponent,
    canActivate: ['IsAdminGuard']
  },
  {
    path: "register",
    component: RegisterComponent
  },
  {
    path: "editApartment/:apartmentId",
    component: EditApartmentComponent,
    canActivate: ['AdminHostGuard']
    
  },
  
]

@NgModule({
  declarations: [
    AppComponent,
    BottomNavbarComponent,
    ApartmentsComponent,
    NavbarComponent,
    PagerComponent,
    ApartmentPageComponent,
    LogInComponent,
    ViewProfileComponent,
    EditProfileComponent,
    ChangePasswordComponent,
    AdminUsersComponent,
    AddApartmentComponent,
    EditApartmentComponent,
    HostApartmentsComponent,
    FileSelectDirective,
    FileDropDirective ,
    RentApartmentComponent,
    GuestReservationsComponent,
    HostReservationsComponent,
    AdminApartmentComponent,
    EditAmenitiesComponent,
    AdminReservationsComponent,
    RegisterComponent,
    
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(Routes),
    HttpClientModule,
    HttpClientXsrfModule,
    NgbModule,
    FormsModule,
    ReactiveFormsModule,
    NgMultiSelectDropDownModule,
    BrowserAnimationsModule,
    Ng5SliderModule,
  ],
  providers: [
    AdminHostGuard,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: TokenInterceptor,
      multi: true
    },
    {
      provide: 'AdminHostGuard',
      useValue: () => {
        return true;
      }
    },
    {
      provide: 'IsLoggedInGuard',
      useValue: () => {
        if (localStorage.role != undefined)
          return true;
      }
    },
    {
      provide: 'IsAdminGuard',
      useValue: () => {
        if (localStorage.role == "Admin")
          return true;
      }
    },
    {
      provide: 'IsGuestGuard',
      useValue: () => {
        if (localStorage.role == "Guest")
          return true;
      }
    }
    ,
    {
      provide: 'IsHostGuard',
      useValue: () => {
        if (localStorage.role == "Host")
          return true;
      }
    },
    btmNavDataService,
    ToasterServiceComponent
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
