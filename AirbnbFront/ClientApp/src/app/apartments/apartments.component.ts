﻿import { Component, OnInit, ViewChild, ComponentFactoryResolver, Input } from '@angular/core';
import { btmNavDataService } from '../bottom-navbar/btmNavDataService';
import { finalize } from 'rxjs/operators'
import { ToasterServiceComponent } from '../toaster-service/toaster-service.component';
import { ApartmentServices } from '../services/apartmentServices';
import { ApartmentTypeServices } from '../services/apartmentTypeServices';
import { NgbDate, NgbCalendar } from '@ng-bootstrap/ng-bootstrap';
import { Validators, FormGroup, FormControl, NgForm } from '@angular/forms';
import { Options, ChangeContext } from 'ng5-slider';
import { apartmentTypeData } from '../models/apartmentTypeData';
import { dropdownData } from '../models/dropdownData';
import { AmenityServices } from '../services/amenityServices';
import { amenityData } from '../models/amenityData';
import { apartmentData } from '../models/apartmentData';

@Component({
  selector: 'app-apartments',
  templateUrl: './apartments.component.html',
  styleUrls: ['./apartments.component.css'],

})
/** apartments component*/
export class ApartmentsComponent implements OnInit {

  apartments: apartmentData[] = [];
  apartmentsTemp: apartmentData[] = [];
  apartmentsTypeFilter: apartmentData[] = [];
  apartmentsAmenityFilter: apartmentData[] = [];
  pageIndex: number = 1;
  pageSize: number = 9;
  totalPagesNumber: number = 0;
  cardsVisible: boolean = false;
  dropdownVisible: boolean = false;
  showProgress: boolean = true;
  showApartmentWarning: boolean = false;

  disableSearchButton: boolean = true;
  searchChecked:boolean=false;

  @Input() logedIn: boolean = false;
  appUserLog: boolean = false;
  hostLog: boolean = false;
  adminLog: boolean = false;

  /*   selectedTypePrice: string = "Price - Mixed";
    sortTypeToServer: string = "Mixed"; */

  minPriceValue: number = 0;
  maxPriceValue: number = 400;
  PriceSlideOptions: Options = {
    floor: 0,
    ceil: 500,
    translate: (value: number): string => {
      return '€' + value;
    }
  };
  numOfGuests: number = 1;
  GuestSlideOptions: Options = {
    floor: 1,
    ceil: 10,
  };

  numOfRooms: number = 1;
  RoomSlideOptions: Options = {
    floor: 1,
    ceil: 20,
  };

  addSearchForm: FormGroup;
  City: FormControl;


  hoveredDate: NgbDate;

  fromDate: NgbDate;
  todayDate: NgbDate;
  fromDateString: string;
  toDate: NgbDate;
  toDateString: string;
  dateString: string;



  amenityFilterF: number[] = [];
  typeFilterF: number[] = [];
  statusFilterF: string;

  dropdownApartmentStatuses = [];
  selectedApartmentStatus: dropdownData[] = [];
  dropdownApartmentStatusesSettings = {};

  dropdownListAmenities = [];
  selectedAmenities: dropdownData[] = [];
  dropdownAmenitiesSettings = {};
  amenitiesTemp: amenityData[];//vrv ne treba

  dropdownPricesSort = [];
  selectedPriceSort: dropdownData[] = [];
  selectedPriceSortTemp: dropdownData;
  dropdownPricesSortSettings = {};

  dropdownApartmentTypes = [];
  selectedApartmentType: dropdownData[] = [];
  apartmentTypesTemp: apartmentTypeData[];//vrv ne treba
  dropdownApartmentTypesSettings = {};

  stopNav: number = 0;

  constructor(private amenityServices: AmenityServices, private apartmentTypeServices: ApartmentTypeServices, private calendar: NgbCalendar, private toasterService: ToasterServiceComponent, private btmNavMessageService: btmNavDataService, private resolver: ComponentFactoryResolver, private apartmentService: ApartmentServices) {
    this.fromDate = this.calendar.getToday();
    this.todayDate = this.calendar.getToday();

    this.toDate = this.calendar.getNext(calendar.getToday(), 'd', 10);
    if (localStorage.role != null && localStorage.role == "Guest") {
      this.appUserLog = true;
    }
    if (localStorage.role != null && localStorage.role == "Host") {
      this.hostLog = true;
    }
    if (localStorage.role != null && localStorage.role == "Admin") {
      this.adminLog = true;
    }
    else {
      this.logedIn = false;
    }


    this.apartmentTypeServices.GetApartmentTypes().pipe(finalize(
      () => {
        this.StopNav();
      }))
      .subscribe(
        data => {
          this.apartmentTypesTemp = data.body as apartmentTypeData[];
          //this.apartmentTypesTemp.push(new apartmentTypeData(-1, "All"))
          for (let a of this.apartmentTypesTemp) {
            this.dropdownApartmentTypes.push({ item_id: a.Id, item_text: a.Type })
          }

        },
        error => {
          if (error.error.Message == undefined) {
            this.toasterService.Error("Undefined Error", 'Error');
          }
          else {
            this.toasterService.Error(error.error.Message, 'Error');
          }
        }
      );

    this.amenityServices.GetAllAmenities().pipe(finalize(
      () => {
        this.StopNav();
      }))
      .subscribe(
        data => {
          this.amenitiesTemp = data.body as amenityData[];
          //this.apartmentTypesTemp.push(new apartmentTypeData(-1, "All"))
          for (let a of this.amenitiesTemp) {
            this.dropdownListAmenities.push({ item_id: a.Id, item_text: a.Type })
          }

        },
        error => {
          if (error.error.Message == undefined) {
            this.toasterService.Error("Undefined Error", 'Error');
          }
          else {
            this.toasterService.Error(error.error.Message, 'Error');
          }
        }
      );
  }

  set page(val: number) {
    if (val !== this.pageIndex) {
      this.pageIndex = val;
      this.GetAllApartmentsSearch();
    }
  }

  ngOnInit() {
    this.btmNavMessageService.currentMessage.subscribe(message => this.showProgress = message)

    this.selectedPriceSortTemp = new dropdownData('Mixed', 'Price - Mixed');

    this.CreateFormControls();
    this.CreateForm();

    this.GetAllApartmentsSearch()

    this.toDateString = this.toDate.day + "/" + this.toDate.month + "/" + this.toDate.year;
    this.fromDateString = this.fromDate.day + "/" + this.fromDate.month + "/" + this.fromDate.year;
    this.dateString = this.fromDateString + " - " + this.toDateString;


    this.dropdownApartmentStatusesSettings = {
      singleSelection: true,
      idField: 'item_id',
      textField: 'item_text',

      allowSearchFilter: false
    };

    this.dropdownApartmentStatuses = [
      new dropdownData('Enabled', 'Enabled - first'),
      new dropdownData('Disabled', 'Disabled -  first'),
      // new dropdownData('Mixed', 'Price - Mixed'),
    ];

    this.dropdownAmenitiesSettings = {
      singleSelection: false,
      idField: 'item_id',
      textField: 'item_text',
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      itemsShowLimit: 2,
      allowSearchFilter: true
    };

    this.dropdownPricesSort = [
      new dropdownData('Low', 'Price - Low to High'),
      new dropdownData('High', 'Price - High to Low'),
      new dropdownData('Mixed', 'Price - Mixed'),
    ];


    this.dropdownPricesSortSettings = {
      singleSelection: true,
      idField: 'item_id',
      textField: 'item_text',

      allowSearchFilter: false
    };


    this.dropdownApartmentTypesSettings = {
      singleSelection: true,
      idField: 'item_id',
      textField: 'item_text',

      allowSearchFilter: false
    };
  }
  ngOnDestroy() {
    this.btmNavMessageService.changeMessage(false);
  }

  onSelectApartmentStatus(item: dropdownData) {
    this.statusFilterF = item.item_id;
    this.GetAllApartmentsSearch();
    /* this.apartmentTypeFilter();/////////
    this.mixFilterArrays(); */
  }

  onDeSelectApartmentStatus(item: dropdownData) {
    this.statusFilterF = "NoFilter";
    this.GetAllApartmentsSearch();
    /* this.mixFilterArrays(); *////////
  }
  onSelectApartmentType(item: dropdownData) {
    this.GetAllApartmentsSearch();
    /* this.apartmentTypeFilter();/////////
    this.mixFilterArrays(); */
  }

  onDeSelectApartmentType(item: dropdownData) {
    this.GetAllApartmentsSearch();
    /* this.mixFilterArrays(); *////////
  }
  /* apartmentTypeFilter() {/////////////////
    this.apartmentsTypeFilter = [];

    for (let apartment of this.apartments) {
      if (apartment.ApartmentTypeId == this.selectedApartmentType[0].item_id) {
        this.apartmentsTypeFilter.push(apartment);
      }
    }
  } */

  /* mixFilterArrays() {////////
    this.apartmentsTemp = [];
    if (this.selectedAmenities.length == 0 && this.selectedApartmentType.length == 0) {
      this.apartmentsTemp = this.apartments;
    }
    else if (this.selectedAmenities.length == 0) {
      this.apartmentsTemp = this.apartmentsTypeFilter;
    }
    else if (this.selectedApartmentType.length == 0) {
      this.apartmentsTemp = this.apartmentsAmenityFilter;
    }
    else {
      for (let apartmentAmenity of this.apartmentsAmenityFilter) {
        for (let apartmentFilter of this.apartmentsTypeFilter) {
          if (apartmentAmenity.Id == apartmentFilter.Id) {
            this.apartmentsTemp.push(apartmentAmenity);
          }
        }
      }
    }
  } */


  onSelectPriceSort(item: dropdownData) {
    this.selectedPriceSortTemp = item;
    this.GetAllApartmentsSearch();
  }
  onDeSelectPriceSort(item: dropdownData) {
    this.selectedPriceSortTemp = new dropdownData('Mixed', 'Price - Mixed');
    this.GetAllApartmentsSearch();
  }


  onSelectAllAmenities(item: dropdownData[]) {//  

    this.selectedAmenities = item;
    this.GetAllApartmentsSearch();
    /* this.amenityFilter();//////
    this.mixFilterArrays(); */

  }
  onSelectAmenity(item: dropdownData) {// 
    this.GetAllApartmentsSearch();
    /* this.amenityFilter();///////////////
    this.mixFilterArrays(); */

  }

  /* amenityFilter() {//////////////////////////////////
    this.apartmentsAmenityFilter = [];
    let add: boolean = true;

    for (let apartment of this.apartments) {
      for (let selectedAmenity of this.selectedAmenities) {
        if (!apartment.Amenities.some(function (amenity) { return amenity.Id == selectedAmenity.item_id })) {
          add = false;
          break;
        }
      }
      if (add) {
        this.apartmentsAmenityFilter.push(apartment);
      }
      add = true;
    }
  } */

  onDeSelectAmenity(item: dropdownData) {

    /*  if (this.selectedAmenities.length != 0) {/////////////////////
       this.amenityFilter();
     }
     else {
       this.apartmentsAmenityFilter = [];
     }
     this.mixFilterArrays(); */
    this.GetAllApartmentsSearch();
  }
  onDeSelectAllAmenities(item: dropdownData) {//
    /*  this.apartmentsAmenityFilter = []; ///////////////////
    */
    this.selectedAmenities = [];
    this.GetAllApartmentsSearch();
    /* this.mixFilterArrays(); //////////*/
  }




  CreateFormControls() {
    this.City = new FormControl('', [
      Validators.maxLength(30),
    ]);


  }
  CreateForm() {
    this.addSearchForm = new FormGroup({
      City: this.City,
    });
  }


  public GetAllApartmentsSearch() {
    this.btmNavMessageService.changeMessage(true);
    let toDate = new Date(this.toDate.year, this.toDate.month - 1, this.toDate.day).toISOString();
    let fromDate = new Date(this.fromDate.year, this.fromDate.month - 1, this.fromDate.day).toISOString();
    /*  for (let a of this.selectedAmenities) {
       this.selectedAmenitiesData.push(new amenityData(a.item_id, 0, a.item_text, "temp"))
     } */
    //let dates: any[]=[fromDate,toDate];
    let city: string;
    if (this.addSearchForm.value.City == "") {
      city = "noCity"
    }
    else {
      city = this.addSearchForm.value.City;
    }

    this.amenityFilterF = [];
    if (this.selectedAmenities.length > 0) {

      this.selectedAmenities.forEach(element => {
        this.amenityFilterF.push(element.item_id);
      });
    }
    this.typeFilterF = [];
    if (this.selectedApartmentType.length > 0) {

      this.selectedApartmentType.forEach(element => {
        this.typeFilterF.push(element.item_id);
      });
    }

    if (this.hostLog) {

      this.apartmentService.GetAllApartmentsHostSearch(this.pageIndex, this.pageSize,this.searchChecked, this.statusFilterF, this.amenityFilterF, this.typeFilterF, city, this.selectedPriceSortTemp.item_id, fromDate, toDate, this.minPriceValue, this.maxPriceValue, this.numOfGuests, this.numOfRooms).pipe(finalize(

        () => {

          this.StopNav();
        }))
        .subscribe(
          data => {

            this.apartments = data.body as apartmentData[];
            this.apartmentsTemp = this.apartments;
            /*  if (this.selectedAmenities.length == 0 && this.selectedApartmentType.length == 0) {///////////////////
               this.apartmentsTemp = this.apartments;
             }
             else {
               if (this.selectedAmenities.length != 0) {
                 this.amenityFilter()
 
               }
               if (this.selectedApartmentType.length != 0) {
                 this.apartmentTypeFilter();
 
               }
               this.mixFilterArrays();
             } */


            this.cardsVisible = true;
            let jsonData = JSON.parse(data.headers.get('Paging-Headers'));

            this.pageIndex = jsonData.currentPage;
            this.pageSize = jsonData.pageSize;
            this.totalPagesNumber = jsonData.totalPages;

            this.showApartmentWarning = false;
          },
          error => {
            if (error.error.Message === 'There are no Apartments') {
              this.showApartmentWarning = true;
              this.toasterService.Warning(error.error.Message, 'Warning');
            }
            else {
              this.showApartmentWarning = false;
              if (error.error.Message == undefined) {
                this.toasterService.Error("Undefined Error", 'Error');
              }
              else {
                this.toasterService.Error(error.error.Message, 'Error');
              }
            }
            this.cardsVisible = false;
            console.log(error);
          })
    }
    else if (this.adminLog) {


      this.apartmentService.GetAllApartmentsAdminSearch(this.pageIndex, this.pageSize,this.searchChecked, this.statusFilterF, this.amenityFilterF, this.typeFilterF, city, this.selectedPriceSortTemp.item_id, fromDate, toDate, this.minPriceValue, this.maxPriceValue, this.numOfGuests, this.numOfRooms).pipe(finalize(

        () => {

          this.StopNav();
        }))
        .subscribe(
          data => {

            this.apartments = data.body as apartmentData[];
            this.apartmentsTemp = this.apartments;
            /*  if (this.selectedAmenities.length == 0 && this.selectedApartmentType.length == 0) {///////////////////
               this.apartmentsTemp = this.apartments;
             }
             else {
               if (this.selectedAmenities.length != 0) {
                 this.amenityFilter()
   
               }
               if (this.selectedApartmentType.length != 0) {
                 this.apartmentTypeFilter();
   
               }
               this.mixFilterArrays();
             } */


            this.cardsVisible = true;
            let jsonData = JSON.parse(data.headers.get('Paging-Headers'));

            this.pageIndex = jsonData.currentPage;
            this.pageSize = jsonData.pageSize;
            this.totalPagesNumber = jsonData.totalPages;

            this.showApartmentWarning = false;
          },
          error => {
            if (error.error.Message === 'There are no Apartments') {
              this.showApartmentWarning = true;
              this.toasterService.Warning(error.error.Message, 'Warning');
            }
            else {
              this.showApartmentWarning = false;
              if (error.error.Message == undefined) {
                this.toasterService.Error("Undefined Error", 'Error');
              }
              else {
                this.toasterService.Error(error.error.Message, 'Error');
              }
            }
            this.cardsVisible = false;
            console.log(error);
          })
    }
    else {
      this.apartmentService.GetAllApartmentsSearch(this.pageIndex, this.pageSize,this.searchChecked, this.amenityFilterF, this.typeFilterF, city, this.selectedPriceSortTemp.item_id, fromDate, toDate, this.minPriceValue, this.maxPriceValue, this.numOfGuests, this.numOfRooms).pipe(finalize(

        () => {

          this.StopNav();
        }))
        .subscribe(
          data => {

            this.apartments = data.body as apartmentData[];
            this.apartmentsTemp = this.apartments;
            /*   if (this.selectedAmenities.length == 0 && this.selectedApartmentType.length == 0) {////////////////////
                this.apartmentsTemp = this.apartments;
              }
              else {
                if (this.selectedAmenities.length != 0) {
                  this.amenityFilter()
  
                }
                if (this.selectedApartmentType.length != 0) {
                  this.apartmentTypeFilter();
  
                }
                this.mixFilterArrays();
              } */


            this.cardsVisible = true;
            let jsonData = JSON.parse(data.headers.get('Paging-Headers'));

            this.pageIndex = jsonData.currentPage;
            this.pageSize = jsonData.pageSize;
            this.totalPagesNumber = jsonData.totalPages;

            this.showApartmentWarning = false;
          },
          error => {
            if (error.error.Message === 'There are no Apartments') {
              this.showApartmentWarning = true;
              this.toasterService.Warning(error.error.Message, 'Warning');
            }
            else {
              this.showApartmentWarning = false;
              if (error.error.Message == undefined) {
                this.toasterService.Error("Undefined Error", 'Error');
              }
              else {
                this.toasterService.Error(error.error.Message, 'Error');
              }
            }
            this.cardsVisible = false;
            console.log(error);
          })
    }



  }
  setCheckBoxHost(event) {

    if (event.target.checked) {
      this.searchChecked = true;
    }
    else {
      this.searchChecked = false;
      this.GetAllApartmentsSearch();
    }
  }


  /* GetSelectedType(type: string) {
    if (type == "Low") {
      this.selectedTypePrice = "Price - Low to High";
      this.sortTypeToServer = 'Low';
    }
    else if (type == "High") {
      this.selectedTypePrice = "Price - High to Low";
      this.sortTypeToServer = 'High';
    }
    else {
      this.selectedTypePrice = "Price - Mixed";
      this.sortTypeToServer = 'Mixed';
    }
    this.GetAllApartmentsSearch();

  } */



  GetNumOfRooms(changeContext: ChangeContext): void {
    this.numOfRooms;
    //this.getApartments();
  }
  GetNumOfGuests(changeContext: ChangeContext): void {
    this.numOfGuests;
    //this.getApartments();
  }
  GetNewPriceRange(changeContext: ChangeContext): void {
    this.minPriceValue;
    this.maxPriceValue;
    //this.getApartments();
  }


  /* GetSelectedApartmentType(type: apartmentTypeData) {
     this.selectedApartmentType = type.Type;
    this.selectedApartmentTypeIdToServer = type.Id; 

    this.GetAllApartmentsSearch();
  } */


  onDateSelection(date: NgbDate) {
    if (!this.fromDate && !this.toDate) {
      this.fromDate = date;
      this.fromDateString = date.day + "/" + date.month + "/" + date.year;
      this.dateString = this.fromDateString + " - " + this.toDateString;
    } else if (this.fromDate && !this.toDate && date.after(this.fromDate)) {
      this.toDate = date;
      this.toDateString = date.day + "/" + date.month + "/" + date.year;
      this.dateString = this.fromDateString + " - " + this.toDateString;
    } else {
      this.toDate = null;
      this.fromDate = date;
      this.fromDateString = date.day + "/" + date.month + "/" + date.year;
      this.dateString = this.fromDateString + " - " + this.toDateString;
    }
  }

  isHovered(date: NgbDate) {
    return this.fromDate && !this.toDate && this.hoveredDate && date.after(this.fromDate) && date.before(this.hoveredDate);
  }

  isInside(date: NgbDate) {
    return date.after(this.fromDate) && date.before(this.toDate);
  }

  isRange(date: NgbDate) {
    return date.equals(this.fromDate) || date.equals(this.toDate) || this.isInside(date) || this.isHovered(date);
  }

  StopNav() {
    if (this.stopNav == 2) {
      this.btmNavMessageService.changeMessage(false);
      this.dropdownVisible = true;

    }
    else if (this.stopNav < 2) {
      this.stopNav += 1;
    }
  }
}