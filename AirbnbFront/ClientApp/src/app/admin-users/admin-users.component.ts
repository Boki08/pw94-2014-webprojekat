﻿import { Component, OnInit, Input, Pipe, Sanitizer } from '@angular/core';
import { UserServices } from '../services/userServices';
import { appUserData } from '../models/appUserData';
import { btmNavDataService } from '../bottom-navbar/btmNavDataService';
import { finalize } from 'rxjs/operators'
import { ToasterServiceComponent } from '../toaster-service/toaster-service.component';
import { HTTP_INTERCEPTORS, HttpClient } from '@angular/common/http';
import { TokenInterceptor } from '../interceptors/interceptors.component';
import { BehaviorSubject, Observable } from 'rxjs';
import { Http, RequestOptions, ResponseContentType } from '@angular/http';
import { SafeUrl, DomSanitizer } from '@angular/platform-browser';
import { dropdownData } from '../models/dropdownData';
import { FormGroup, FormControl, Validators, NgForm } from '@angular/forms';
import { registerData } from '../models/registerData';

@Component({
    selector: 'app-admin-users',
    templateUrl: './admin-users.component.html',
    styleUrls: ['./admin-users.component.css']
})
/** admin-users component*/
export class AdminUsersComponent {
    /** admin-users ctor */
    constructor(private toasterService: ToasterServiceComponent, private btmNavMessageService: btmNavDataService, private UserServices: UserServices) { }

    pageSize: number = 5;



    users: appUserData[];
    userData: appUserData;


    disableCheckButton: boolean = false;
    showUsers: boolean = false;
    showHosts: boolean = false;
    showUsersWarning: boolean = false;
    showHostsWarning: boolean = false;
    showProgress: boolean;
    userProgress: boolean;
    hostProgress: boolean = false;
    editedFirst: boolean = false;
    approvedFirst: boolean = false;
    isUser: boolean = true;
    enabledFirst: boolean = false;
    disabledFirst: boolean = false;
    showPicture: boolean = false;
    isAdmin: boolean = false;

    selectedGenderSearch: string = "notSet";
    selectedUsername: string = "notSet";

    selectedRole: string = "notSet";

    pageIndex: number = 1;
    totalPagesNumber: number = 0;
    /* pageIndexM: number = 1;
    totalPagesNumberM: number = 0; */

    userCounter: number;

    dropdownListRoles = [];
    selectedRoles: dropdownData[] = [];
    dropdownRolesSettings = {};

    dropdownListGenders = [];
    selectedGenders: dropdownData[] = [];
    dropdownGendersSettings = {};

    addSearchForm: FormGroup;
    UsernameControl: FormControl;

    addHostForm: FormGroup;
    Username: FormControl;
    Password: FormControl;
    Name: FormControl;
    Surname: FormControl;
    Gender: FormControl;

    dropdownListGender = [];
    selectedGender: dropdownData;
    dropdownGenderSettings = {};

    ngOnInit() {
        this.btmNavMessageService.currentMessage.subscribe(message => this.showProgress = message)


        this.getAllUsers();
        this.CreateFormControls();
        this.CreateForm();

        this.dropdownListRoles = [
            new dropdownData('notSet', 'Not set'),
            new dropdownData('Guest', 'Guest '),
            new dropdownData('Host', 'Host '),
            new dropdownData('Admin', 'Admin '),
        ];

        this.dropdownRolesSettings = {
            singleSelection: true,
            idField: 'item_id',
            textField: 'item_text',
            allowSearchFilter: false
        };
        this.dropdownListGenders = [
            new dropdownData('notSet', 'Not set'),
            new dropdownData('female', 'Female'),
            new dropdownData('male', 'Male'),
        ];

        this.dropdownGendersSettings = {
            singleSelection: true,
            idField: 'item_id',
            textField: 'item_text',
            allowSearchFilter: false
        };
        this.dropdownListGender = [
            new dropdownData('female', 'Female'),
            new dropdownData('male', 'Male'),
        ];
        //this.selectedGender = this.dropdownListGender[1];

        this.dropdownGenderSettings = {
            singleSelection: true,
            idField: 'item_id',
            textField: 'item_text',
            allowSearchFilter: false
        };
    }
    ngOnDestroy() {
        this.btmNavMessageService.changeMessage(false);
    }

    CreateFormControls() {
        this.UsernameControl = new FormControl('', [
            Validators.maxLength(30),
        ]);
        this.Username = new FormControl('', [
            Validators.maxLength(30),
            Validators.minLength(5), Validators.required,
        ]);
        this.Password = new FormControl('', [
            Validators.maxLength(30),
            Validators.minLength(6), Validators.required,
        ]);
        this.Name = new FormControl('', [
            Validators.maxLength(30),
            Validators.minLength(1), Validators.required,
        ]);
        this.Surname = new FormControl('', [
            Validators.maxLength(30),
            Validators.minLength(1), Validators.required,
        ]);
        this.Gender = new FormControl('', [
            Validators.required,
        ]);


    }
    CreateForm() {
        this.addSearchForm = new FormGroup({
            UsernameControl: this.UsernameControl,
        });
        this.addHostForm = new FormGroup({
            Username: this.Username,
            Password: this.Password,
            Name: this.Name,
            Surname: this.Surname,
            Gender: this.Gender,
        });
    }

    public getAllUsers() {
        this.btmNavMessageService.changeMessage(true);
        /*  if (type == "Guest") { */
        this.UserServices.GetAllUsers(this.pageIndex, this.pageSize, this.selectedRole, this.selectedGenderSearch, this.selectedUsername).pipe(finalize(
            () => {
                this.btmNavMessageService.changeMessage(false);
            }))
            .subscribe(
                data => {

                    this.users = data.body as appUserData[];


                    let jsonData = JSON.parse(data.headers.get('Paging-Headers'));

                    this.pageIndex = jsonData.currentPage;
                    this.pageSize = jsonData.pageSize;
                    this.totalPagesNumber = jsonData.totalPages;
                    this.showUsers = true;
                    this.showUsersWarning = false;

                },
                error => {
                    if (error.error.Message === "There are no Users") {
                        this.showUsers = false;
                        this.showUsersWarning = true;
                    }
                    else {
                        this.showUsersWarning = false;
                        this.toasterService.Error(error.error.Message, 'Error');
                    }

                })
        //}
        /* else {//manager
          this.UserServices.GetAllUsers(type, this.pageIndexM, this.pageSize, this.enabledFirst, this.disabledFirst).pipe(finalize(
            () => {
              this.btmNavMessageService.changeMessage(false);
            }))
            .subscribe(
              data => {
    
                this.hosts = data.body as appUserData[];
    
                let jsonData = JSON.parse(data.headers.get('Paging-Headers'));
    
                this.pageIndexM = jsonData.currentPage;
                this.pageSize = jsonData.pageSize;
                this.totalPagesNumberM = jsonData.totalPages;
    
                this.showHosts = true;
                this.showHostsWarning = false;
    
              },
              error => {
                if (error.error.Message === "There are no Hosts") {
                  this.toasterService.Warning(error.error.Message, 'Warning');
                  this.showHosts = false;
                  this.showHostsWarning = true;
                }
                else {
                  this.showHostsWarning = false;
                  this.toasterService.Error(error.error.Message, 'Error');
                }
              })
        } */
    }

    public GetAllUsersSearch() {

        if (this.addSearchForm.value.UsernameControl == "") {
            this.selectedUsername = "notSet"
        }
        else {
            this.selectedUsername = this.addSearchForm.value.UsernameControl;
        }
        this.getAllUsers();
    }
    onSelectRole(item: dropdownData) {
        this.selectedRole = item.item_id;

    }
    onSelectGender(item: dropdownData) {
        this.selectedGenderSearch = item.item_id;

    }
    onDeSelectRole(item: dropdownData) {
        this.selectedRole = 'notSet';
    }
    onDeSelectGender(item: dropdownData) {
        this.selectedGenderSearch = 'notSet';
    }



    set pageU(val: number) {
        if (val !== this.pageIndex) {

            this.pageIndex = val;
            this.getAllUsers();
        }
    }
   /*  set pageM(val: number) {
        if (val !== this.pageIndexM) {

            this.pageIndexM = val;
            this.getAllUsers();
        }
    }
 */



    /* setTab(value: string): void {
        this.getAllUsers();
        this.selectedRole = value;
        if (value == "Guest") {
            this.isUser = true;
        }
        else {
            this.isUser = false;
        }

    } */
    /* isSelected(name: string): boolean {

        if (this.selectedRole == name) {
            return true;
        }
        return false
    } */

    userDetails(user: appUserData, counter: number) {

        this.userData = user;
        this.userCounter = counter;

        this.userProgress = true;
        this.disableCheckButton=true;
        this.UserServices.getProfileById(this.userData.Id).pipe(finalize(
            () => {
                this.userProgress = false;
            }))
            .subscribe(
                data => {

                    this.userData = data.body;

                    let jsonData = JSON.parse(data.headers.get('Paging-Headers'));

                    if ('Admin' == jsonData.userRole as string) {
                        this.isAdmin = true;
                    }
                    else {
                        this.isAdmin = false;
                        this.disableCheckButton=false;
                    }
                    //this.users[counter] = this.userData;
                },
                error => {
                    this.toasterService.Error(error.error.Message, 'Error');
                })


    }

    /* hostDetails(host: appUserData, counter: number) {
        this.hostData = host;
        this.hostCounter = counter;

        this.disableCheckButton = true;

        this.hostProgress = true;

        this.UserServices.getProfileById(this.hostData.Id).pipe(finalize(
            () => {
                this.hostProgress = false;
            }))
            .subscribe(
                data => {

                    this.hostData = data.body;
                    this.disableCheckButton = false;

                    this.hosts[counter] = this.hostData;
                },
                error => {

                    this.toasterService.Error(error.error.Message, 'Error');
                })
    }
 */

    /* acceptUser(userID: number, activate: boolean) {
        this.userProgress = true;

       

        this.UserServices.ActivateUser(userID, activate).pipe(finalize(
            () => {
                this.userProgress = false;
               
            }))
            .subscribe(
                data => {
                    this.userData = data.body;
                    this.users[this.usercounter] = data.body;

                    if (activate == true) {
                        this.toasterService.Info("User was activated", 'Info');
                    }
                    else {
                        this.toasterService.Info("User was deactivated", 'Info');
                    }

                },
                error => {

                    if (error.statusText == "Precondition Failed") {
                        this.toasterService.Error("Data was already changed, please reload", 'Error');
                    }
                    else {
                        this.toasterService.Error(error.error.Message, 'Error');
                    }
                    console.log(error);
                }
            )
    } */
    onSubmitHost(host: any, form: NgForm) {
        let newHost: registerData = new registerData(host.Name, host.Surname, host.Gender[0].item_id, host.Username, host.Password, host.Password);

        this.UserServices.registerHost(newHost).pipe(finalize(
            () => {
                this.btmNavMessageService.changeMessage(false);
            }))
            .subscribe(
                data => {
                    this.getAllUsers();
                    this.toasterService.Info("Host added successfully", 'Info');
                },
                error => {
                    this.toasterService.Error(error.error.Message, 'Error');
                })
        form.reset();
    }

    setCheckBoxHost(event) {
        this.hostProgress = true;
        this.disableCheckButton = true;
        let hostEnabled: boolean;
        if (event.target.checked) {
            hostEnabled = true;
        }
        else {
            hostEnabled = false;
        }
        this.UserServices.ActivateUser(this.userData.Id, hostEnabled).pipe(finalize(
            () => {
                this.disableCheckButton = false;
                this.hostProgress = false;
                this.btmNavMessageService.changeMessage(false);
            }))
            .subscribe(
                data => {
                    this.userData = data.body;
                    this.users[this.userCounter] = data.body;

                    if (hostEnabled == true) {
                        this.toasterService.Info("User was blocked", 'Info');
                    }
                    else {
                        this.toasterService.Info("User was unblocked", 'Info');
                    }

                },
                error => {

                    this.toasterService.Error(error.error.Message, 'Error');

                })
    }
    /* DeleteUser() {
      this.btmNavMessageService.changeMessage(true);
      this.UserServices.DeleteUser(parseInt(this.userData.Id)).pipe(finalize(
        () => {
          this.btmNavMessageService.changeMessage(false);
        }))
        .subscribe(
          data => {
            this.users.splice[this.usercounter]
          },
          error => {
            if(error.statusText== "Precondition Failed")
            {
              this.toasterService.Error("Data was already changed, please reload",'Error');
            }
            else
            {
            this.toasterService.Error(error.error.Message,'Error');
            }
            console.log(error.Message);
          }
        )
    } */

    setRadioUser(value: string): void {
        if (value == 'No') {
            this.editedFirst = false;
            this.approvedFirst = false;


        }
        else if (value == 'Approved') {
            this.editedFirst = false;
            this.approvedFirst = true;
        }
        else {
            this.editedFirst = true;
            this.approvedFirst = false;
        }
        this.getAllUsers();

    }

    setRadioHost(value: string): void {
        if (value == 'No') {
            this.enabledFirst = false;
            this.disabledFirst = false;


        }
        else if (value == 'Enabled') {
            this.enabledFirst = false;
            this.disabledFirst = true;
        }
        else {
            this.enabledFirst = true;
            this.disabledFirst = false;
        }
        this.getAllUsers();

    }
}
