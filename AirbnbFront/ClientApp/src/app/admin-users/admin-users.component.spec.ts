﻿/// <reference path="../../../../node_modules/@types/jasmine/index.d.ts" />
import { TestBed, async, ComponentFixture, ComponentFixtureAutoDetect } from '@angular/core/testing';
import { BrowserModule, By } from "@angular/platform-browser";
import { AdminUsersComponent } from './admin-users.component';

let component: AdminUsersComponent;
let fixture: ComponentFixture<AdminUsersComponent>;

describe('admin-users component', () => {
    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [ AdminUsersComponent ],
            imports: [ BrowserModule ],
            providers: [
                { provide: ComponentFixtureAutoDetect, useValue: true }
            ]
        });
        fixture = TestBed.createComponent(AdminUsersComponent);
        component = fixture.componentInstance;
    }));

    it('should do something', async(() => {
        expect(true).toEqual(true);
    }));
});