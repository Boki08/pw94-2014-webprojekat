﻿/// <reference path="../../../../node_modules/@types/jasmine/index.d.ts" />
import { TestBed, async, ComponentFixture, ComponentFixtureAutoDetect } from '@angular/core/testing';
import { BrowserModule, By } from "@angular/platform-browser";
import { RentApartmentComponent } from './rent-apartment.component';

let component: RentApartmentComponent;
let fixture: ComponentFixture<RentApartmentComponent>;

describe('rent-apartment component', () => {
    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [ RentApartmentComponent ],
            imports: [ BrowserModule ],
            providers: [
                { provide: ComponentFixtureAutoDetect, useValue: true }
            ]
        });
        fixture = TestBed.createComponent(RentApartmentComponent);
        component = fixture.componentInstance;
    }));

    it('should do something', async(() => {
        expect(true).toEqual(true);
    }));
});