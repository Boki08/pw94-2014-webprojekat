﻿import { Component } from '@angular/core';
import { btmNavDataService } from '../bottom-navbar/btmNavDataService';
import { ActivatedRoute } from '@angular/router';
import { ApartmentServices } from '../services/apartmentServices';
import { ToasterServiceComponent } from '../toaster-service/toaster-service.component';
import { finalize } from 'rxjs/operators';
import { apartmentData } from '../models/apartmentData';
import { NgbDate, NgbCalendar, NgbDateStruct } from '@ng-bootstrap/ng-bootstrap';

@Component({
    selector: 'app-rent-apartment',
    templateUrl: './rent-apartment.component.html',
    styleUrls: ['./rent-apartment.component.css']
})
/** rent-apartment component*/
export class RentApartmentComponent {
    /** rent-apartment ctor */
    showProgress: boolean = true;
    enabled: boolean = false;

    apartmentId: number;
    stopNav: number = 0;

    isDisabled = (date: NgbDateStruct, current: {month: number, year: number})=> {
        return this.disabledNgbDates.find(x => NgbDate.from(x).equals(date))? true: false;
      }

    hoveredDate: NgbDate;
    fromDate: NgbDate;
    todayDate: NgbDate;
    startDate: NgbDate;
    endDate: NgbDate;
    fromDateString: string;
    toDate: NgbDate;
    toDateString: string;
    dateString: string;

    apartment: apartmentData;
    disabledNgbDates: NgbDate[] = [];
    disabledDates: Date[] = [];

    constructor(private calendar: NgbCalendar, private toasterService: ToasterServiceComponent, private apartmentServices: ApartmentServices, private activatedRoute: ActivatedRoute, private btmNavMessageService: btmNavDataService) {
        activatedRoute.params.subscribe(params => { this.apartmentId = params["apartmentId"] });

    }
    ngOnInit(): void {
        this.btmNavMessageService.currentMessage.subscribe(message => this.showProgress = message)

        this.startDate = this.calendar.getToday();
        this.todayDate = this.calendar.getToday();



        this.btmNavMessageService.changeMessage(true);
        this.apartmentServices.GetApartment(this.apartmentId).pipe(finalize(
            () => {
                this.StopNav();
            }))
            .subscribe(
                data => {
                    this.apartment = data.body as apartmentData;
                    let date: Date = new Date(this.apartment.RentingDates[0].EndDate);
                    this.endDate = new NgbDate(date.getFullYear(), date.getMonth() + 1, date.getDate());
                   

                    date = new Date(this.apartment.RentingDates[0].StartDate);
                    let startDateNgb: NgbDate = new NgbDate(date.getFullYear(), date.getMonth() + 1, date.getDate());
                    if (this.startDate.before(startDateNgb)) {
                        this.startDate = startDateNgb;
                    }
                },
                error => {
                    // this.showApartmentWarning = false;
                    if (error.error.Message == undefined) {
                        this.toasterService.Error("Undefined Error", 'Error');
                    }
                    else {
                        this.toasterService.Error(error.error.Message, 'Error');
                    }
                }
            );

        this.apartmentServices.GetDisabledDates(this.apartmentId).pipe(finalize(
            () => {
                this.StopNav();
            }))
            .subscribe(
                data => {
                    //this.disabledDates = data.body as NgbDate[];
                    //this.mainVisible = true;
                    data.body.forEach(element => {
                        let date: Date = new Date(element);
                        this.disabledDates.push(new Date(element));
                        this.disabledNgbDates.push(new NgbDate(date.getFullYear(), date.getMonth() + 1, date.getDate()));
                    });
                },
                error => {
                    // this.showApartmentWarning = false;
                    if (error.error.Message == undefined) {
                        this.toasterService.Error("Undefined Error", 'Error');
                    }
                    else {
                        this.toasterService.Error(error.error.Message, 'Error');
                    }
                }
            );
    }
    ngOnDestroy() {
        this.btmNavMessageService.changeMessage(false);
    }

    StopNav() {
        if (this.stopNav == 1) {
            this.btmNavMessageService.changeMessage(false);
            this.enabled = true;
        }
        else if (this.stopNav < 2) {
            this.stopNav += 1;
        }
    }
    onDateSelection(date: NgbDate) {
        let toDateTemp: Date = new Date(date.year, date.month - 1, date.day);
        /* this.disabledDates.forEach(element => {
            if (element.toISOString() == toDateTemp.toISOString()) {
                allow = false;
                return;
                
            }
        }); */
        /* for(let i=0;i<this.disabledDates.length;i++){
            if (this.disabledDates[i].toISOString() == toDateTemp.toISOString()) {
                return;
                
            }
        } */
   
            if (!this.fromDate && !this.toDate) {
                this.fromDate = date;
                this.fromDateString = date.day + "/" + date.month + "/" + date.year;
                this.dateString = this.fromDateString + " - " + this.toDateString;
            } else if (this.fromDate && !this.toDate && date.after(this.fromDate)) {

                let testDate: Date = new Date(this.fromDate.year, this.fromDate.month - 1, this.fromDate.day);
                for (testDate; testDate.toISOString() != toDateTemp.toISOString(); testDate.setDate(testDate.getDate() + 1)) {
                    for(let i=0;i<this.disabledDates.length;i++){
                        if (this.disabledDates[i].toISOString() == testDate.toISOString()) {
                            return;
                            
                        }
                    }
                }
                this.toDate = date;
                this.toDateString = date.day + "/" + date.month + "/" + date.year;
                this.dateString = this.fromDateString + " - " + this.toDateString;
            } else {
                this.toDate = null;
                this.fromDate = date;
                this.fromDateString = date.day + "/" + date.month + "/" + date.year;
                this.dateString = this.fromDateString + " - " + this.toDateString;
            }
        
    }

    isHovered(date: NgbDate) {
        return this.fromDate && !this.toDate && this.hoveredDate && date.after(this.fromDate) && date.before(this.hoveredDate);
    }

    isInside(date: NgbDate) {
        return date.after(this.fromDate) && date.before(this.toDate);
    }

    isRange(date: NgbDate) {
        return date.equals(this.fromDate) || date.equals(this.toDate) || this.isInside(date) || this.isHovered(date);
    }
}