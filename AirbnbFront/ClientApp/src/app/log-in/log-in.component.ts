﻿import { Component, ViewChild, ElementRef, EventEmitter, Output } from '@angular/core';
import { Router } from '@angular/router';
import { ToasterServiceComponent } from '../toaster-service/toaster-service.component';
import { btmNavDataService } from '../bottom-navbar/btmNavDataService';
import { LogInService } from '../services/logInServices';
import { NgForm } from '@angular/forms';
import { finalize } from 'rxjs/operators';
import { logInData } from '../models/logInData';
import { UserServices } from '../services/userServices';

@Component({
    selector: 'app-log-in',
    templateUrl: './log-in.component.html',
    styleUrls: ['./log-in.component.css'],
    providers: [LogInService]
})
/** log-in component*/
export class LogInComponent {
    isVisibleNotLogged: boolean = false;
    isVisibleLogged: boolean = false;
    name: string = "";
    @ViewChild('loggedButton', {static:false}) loggedButton: ElementRef;
    showProgress: boolean = false;
    @Output()
  private loggedOutEvent: EventEmitter<number> = new EventEmitter<number>();
    /** log-in ctor */
    constructor(private router: Router, private toasterService: ToasterServiceComponent, private btmNavMessageService: btmNavDataService, private LogInService: LogInService, private UserService: UserServices) {

    }
    ngOnInit() {
        this.btmNavMessageService.currentMessage.subscribe(message => this.showProgress = message)
        this.btmNavMessageService.changeMessage(false);
    }
    ngOnDestroy() {
        this.btmNavMessageService.changeMessage(false);
    }
    ngAfterContentInit() {

        this.setUp();

    }

    setUp() {
        if (!localStorage.jwt) {
            this.isVisibleNotLogged = true;
            this.isVisibleLogged = false;
        }
        else {
            this.isVisibleNotLogged = false;


            let jwtData = localStorage.jwt.split('.')[1]
            let decodedJwtJsonData = window.atob(jwtData)
            let decodedJwtData = JSON.parse(decodedJwtJsonData)

            if (decodedJwtData.UserFullName != null && decodedJwtData.UserFullName != "") {
                this.name = decodedJwtData.UserFullName
            }
            else {
                this.name = decodedJwtData.role;
            }
            this.isVisibleLogged = true;

        }
    }
    onSubmit(user: logInData, form: NgForm) {
        console.log(user);
        this.LogInService.getTheToken(user);
        //form.reset();
        this.setUp();
    }

    logOut() {
        this.btmNavMessageService.changeMessage(true);
        this.UserService.LogOut().pipe(finalize(
            () => {
                if (localStorage.jwt) { localStorage.removeItem("jwt"); }
                if (localStorage.role) { localStorage.removeItem("role"); }
                this.btmNavMessageService.changeMessage(false);
                this.loggedOutEvent.emit();
                this.setUp();
                this.router.navigate(['/apartments']);
            }))
            .subscribe(
                data => {




                    this.toasterService.Info("Logged Out", 'Info');

                },
                error => {
                    //if (localStorage.jwt) { localStorage.removeItem("jwt"); }
                    //if (localStorage.role) { localStorage.removeItem("role"); }
                    this.toasterService.Error(error.error.Message, 'Error');
                    //this.setUp();
                    //this.loggedOutEvent.emit();

                }
            );
    }
}

